
Changelog
=========

All notable changes to this project will be documented in this file.

The format is based on 
[Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to 
[Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.1.0] - 2021-02-12
 *  The first playable prototype.
 *  The player can control the shuttle by mouse and shoot the aliens by three
    different kind of weapons: gun, shotgun and laser.
 *  Floating bonuses are implemented; they provide additional ammunition for
    the shotgun, additional energy for the laser, or an additional shield.
 *  Shields are implemented; when the shuttle is hit by an alien, or by an
    alien bullet, it either loses one shield or dies, if there are no shields
    left.
 *  Three types of aliens implemented: GREEN which only follow the shuttle, RED
    which in addition shoots bullets, BLUE which can clone itself.
 *  The game increases its difficulty with the number of aliens killed.
    The difficulty is increased by spawning the aliens more often, by
    increasing the probability of spawning tougher aliens (red and blue ones),
    and by increasing the speed and the manoeuvrability of the aliens.

## [0.0.0] - 2008
 *  Back in 2008, this program has been written.
 *  The source code of this particular version has been lost, however, the
    binaries remained and are added to the project for a comparison.
 *  You can find this version as a JAR file in the executables/ directory.
 *  On the other hand, another version of the source code remained and served
    as the base of the reincarnated program.

[0.1.0]: https://gitlab.com/petrikm/movetheshuttle/-/tags/0.1.0
[0.0.0]: https://gitlab.com/petrikm/movetheshuttle/-/blob/master/executables/movetheshuttle-0.0.0.jar
