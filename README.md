
Move the Shuttle 
================

This is a simple arcade game written in Java where the player controls a
    shuttle which flies in space and shoots aliens by several types of weapons.

![A screenshot from the game](screenshot.png "A screenshot from the game")


Getting and compiling the source code
-------------------------------------

In order to compile the source code you need
[Java compiler](https://openjdk.java.net)
and [Apache Ant](https://ant.apache.org/).

Further, it is beneficial to have [git](https://git-scm.com/) installed and
to be signed up to [gitlab](https://gitlab.com).

Note that this installation guide has been written from the perspective of a
GNU/Linux user.

Get the source code by cloning it from the gitlab repository:

    git clone https://gitlab.com/petrikm/movetheshuttle.git

Or, download it from [gitlab](https://gitlab.com/petrikm/movetheshuttle) and
unpack the downloaded file.

Navigate to the main directory  of the project:

    cd movetheshuttle

While in the main directory, compile the source code by typing:

    ant

While in the main directory, run the program:

    java -jar dist/movetheshuttle-x.y.z.jar

(here, x.y.z stands for the current version).


Getting and running the binaries
--------------------------------

In order to run the binaries you need a Java Runtime Environment which is
provided, for example, at [OpenJDK](https://openjdk.java.net) or at
[java.com](https://www.java.com/en/download).

Download the binaries using the following links:

 * [movetheshuttle-0.1.0.jar](https://gitlab.com/petrikm/movetheshuttle/-/blob/master/executables/movetheshuttle-0.1.0.jar)
 * [movetheshuttle-0.0.0.jar](https://gitlab.com/petrikm/movetheshuttle/-/blob/master/executables/movetheshuttle-0.0.0.jar)

Run the program:

    java -jar movetheshuttle-x.y.z.jar

(here, x.y.z stands for the version).


Playing the game
----------------

### Controlling the shuttle

The very first motivation to write this game was to implement an
unusual way to control a game object.
The shuttle is controlled by the mouse; it follows the mouse cursor, however,
not directly as there is an inertia implemented.

The attributes of the shuttle are its *position vector* and its *velocity
vector*.
 *  In each time period, the position of the shuttle is modified according to
    the velocity vector.
    (If the velocity vector remained constant, the value of the position vector
    would be given, after one second, as the sum of the previous value of the
    position vector and the value of the velocity vector.)
 *  The velocity vector is modified, in each time period, by the 
    *acceleration vector* which is defined as the difference between the
    position of the mouse cursor and the position of the shuttle.
    (Again, if the acceleration vector remained constant, the value of the velocity
    vector would be given, after one second, as the sum of the previous value of
    the velocity vector and the value of the acceleration vector.)

The result is a smooth trajectory of the movement of the shuttle.

### Mouse control

 *  **Left click** ... fire,
 *  **Mouse wheel** ... change weapon.

### Keyboard control

 *  **P** ... pause the game,
 *  **R** ... restart the game after the shuttle has exploded,
 *  **ESC** ... exit the program.

### Shuttle shields

The player starts with the *number of the shields* equal to zero.
When the shuttle catches a bonus item with a shield, the number of the shields
is increased by one.
When something (an alien or a bullet shot by an alien) hits the shuttle the
number of the shields is decreased by one, however, if the number of the
shields is already equal to zero then the shuttle explodes and the game ends.

### Shuttle weapon types

The player can fire from a selected weapon by clicking the left mouse button.
Further, the weapon can be changed by using the mouse wheel.
However, the player can never switch to a weapon that has zero ammunition (or
energy, in the case of the laser).

There are three distinct weapons implemented:

 *  **GUN** ... This is the basic type of weapon. 
    It has infinite ammunition and one bullet is shot per one mouse click.
 *  **SHOTGUN** ... The behaviour is very similar to GUN, however, multiple
    bullets are shot per one mouse click, the bullets are a bit slower and they
    are represented by a different image.
    Before the player can use this weapon, the shuttle needs to catch a bonus
    with the shotgun ammunition.
 *  **LASER** ... This weapon fires an infinite ray which kills every alien
    that collides with it.
    The ammunition of the laser is not measured by a number of bullets but by
    an amount of energy; the amount of energy is actually equal to the number
    of seconds for which the weapon can be used.

### Bonus items

Occasionally, a bonus item will appear flying from one side of the screen to
the opposite one.
It will always appear as a circle with a rotating image inside.
If the shuttle catches a bonus item (that is, if it enters into a collision
with it) then the bonus item disappears and the player receives an
advatage.
A bonus item can be one of the following types:

 *  **Shotgun ammunition** ... Adds an amount of the ammunition for the
    shotgun.
 *  **Laser battery pack** ... Adds an amount of the energy for the laser.
 *  **Additional shield** ... Increases the number of the shields by one.

### Enemy types

Enemies are represented by floating faces of different colors and abilities:

 *  **Green Face** ... Floats slowly in space being attracted by the shuttle. When
    it enters into a collision with the shuttle, it dies and the shuttle
    loses one shield, or explodes if the number of the shields is zero.
 *  **Red Face** ... Behaves exactly as the Green Face, however, in addition
    occasionally shoots a bullet in the direction of the shuttle.
 *  **Blue Face** ... Behaves exactly as the Green Face, however, in addition
    occasionally makes its own clone.
    If left alone, the screen will eventually be flooded by Blue Faces.


References
----------

[Project page on gitlab.com](https://gitlab.com/petrikm/movetheshuttle)

