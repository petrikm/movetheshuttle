import java.awt.*;
import java.util.*;

/**
 *  Represents the "red" alien that can shoot bullets.
 *  @see    BulletAlienShooter
 */
public class AlienShooter extends Alien {
    /** Externally created list of alien bullets.
     *  When the alien decides to make a shot, it simply adds a bullet to this
     *      list. */
	private ListOfMovingObjects listOfAlienBullets;

    /** How many seconds to the next shot.
     *  Decreases over time.
     *  When it reaches zero then a bullet is shot and the value of this
     *      attribute is set to some positive value indicating the number of
     *      seconds to the next shot. */
    private float timeToNextShot;

    /** 
     *  Constructor. 
     *
     *  @param  positionX           x-coordinate of the position
     *  @param  positionY           y-coordinate of the position
     *  @param  listOfParticles     externally created list of particles
     *  @param  listOfAlienBullets  externally created list of alien bullets
     *  @param  score               current player's score; determines how tough the newly created alien shall be
     *  @param  rand                reference to a random number generator shared by all classes of the program
     */
    AlienShooter(float positionX, float positionY,
            ListOfMovingObjects listOfParticles,
            ListOfMovingObjects listOfAlienBullets,
            int score,
            Random rand) {
        super(positionX, positionY, listOfParticles, score, rand);
        this.listOfAlienBullets = listOfAlienBullets;
        this.timeToNextShot = 2.0f + 3.0f * rand.nextFloat();
        this.contourColor = new Color(120, 0, 0);
        this.fillColor = new Color(200, 0, 0);
	}

    /** Creates a bullet shot by the alien and adds it to the list of alien bullets. */
	public Bullet shootBullet() {
        float bulletSpeed = 100.0f + getAbsoluteVelocity();

        float bulletPositionX = positionX;
        float bulletPositionY = positionY;

        float differenceX = attractorX - positionX;
        float differenceY = attractorY - positionY;
        float distance = (float) Math.sqrt(differenceX * differenceX + differenceY * differenceY);

        float bulletVelocityX = bulletSpeed * differenceX / distance;
        float bulletVelocityY = bulletSpeed * differenceY / distance;

        return new BulletAlienShooter(bulletPositionX, bulletPositionY, bulletVelocityX, bulletVelocityY, rand);
	}

    /** Moves the alien.
     *  @param  deltaTime   time interval according to which the new position of the moving object is computed */
	public void move(float deltaTime) {
		super.move(deltaTime);

        timeToNextShot -= deltaTime;
        if (timeToNextShot < 0.0f) {
            listOfAlienBullets.add(shootBullet());
            this.timeToNextShot = 1.0f + 3.0f * rand.nextFloat();
        }
	}
}
