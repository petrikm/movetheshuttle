import java.util.*;
import java.awt.*;

// needed for unit test
import java.awt.event.*;
import java.awt.image.*;

/**
 *  Represents a floating set of teeth that will appear after an explosion of an alien.
 */
public class AlienMouth extends FadingParticle {
    /** Collision radius shared by all the alien mouths */
    public static final float WIDTH = 12.0f;
    public static final float HEIGHT = 4.0f;
    public static final int NUMBER_OF_TEETH = 4;

    /** Colors of the parts of the vector image of the mouth. */
    protected static Color contourColor = new Color(0, 120, 0);
    protected static Color fillColor    = Color.white;

    /**
     *  Creates a new alien mouth.
     *
     *  @param  positionX           x-coordinate of the position
     *  @param  positionY           y-coordinate of the position
     *  @param  velocityX           x-coordinate of the velocity vector
     *  @param  velocityY           y-coordinate of the velocity vector
     *  @param  lifeTime            how long the particle shall exist (in nanoseconds)
     *  @param  rand                reference to a random number generator shared by all classes of the program
     */
    public AlienMouth(
            float positionX,
            float positionY,
            float velocityX,
            float velocityY,
            long lifeTime,
            Random rand) {
        super(positionX, positionY, velocityX, velocityY,
            WIDTH / 2.0f, new Color(255, 0, 255), lifeTime,
            rand);
    }

    /** Draws an alien mouth.
     *  @param  mouthPositionX    x-coordinate of the position of the center of the mouthball
     *  @param  mouthPositionY    y-coordinate of the position of the center of the mouthball
     *  @param  opacity         inverse transparency of the image
     *  @param  g               graphic context */
    public static void drawMouth(float mouthPositionX, float mouthPositionY, float opacity, Graphics g) {
        int drawX = Math.round(mouthPositionX - WIDTH / 2.0f);
        int drawY = Math.round(mouthPositionY - HEIGHT / 2.0f);
        int drawWidth = Math.round(WIDTH);
        int drawHeight = Math.round(HEIGHT);

        int fillRed =   Math.round(opacity * (float) fillColor.getRed());
        int fillGreen = Math.round(opacity * (float) fillColor.getGreen());
        int fillBlue =  Math.round(opacity * (float) fillColor.getBlue());
        Color fillColor = new Color(fillRed, fillGreen, fillBlue);
        g.setColor(fillColor);
        g.fillRect(drawX, drawY, drawWidth, drawHeight);

        int contourRed =   Math.round(opacity * (float) contourColor.getRed());
        int contourGreen = Math.round(opacity * (float) contourColor.getGreen());
        int contourBlue =  Math.round(opacity * (float) contourColor.getBlue());
        Color contourColor = new Color(contourRed, contourGreen, contourBlue);
        g.setColor(contourColor);
        g.drawRect(drawX, drawY, drawWidth, drawHeight);
        for (int i = 0; i < NUMBER_OF_TEETH - 1; ++i) {
            int x = Math.round((mouthPositionX - WIDTH / 2.0f) + (WIDTH / NUMBER_OF_TEETH) * (i + 1));
            g.drawLine(x, drawY, x, drawY+drawHeight);
        }
    }

    /** Draws the alien mouth. */
    public void draw(Graphics g) {
        drawMouth(positionX, positionY, opacity, g);
    }

    /** Performs unit testing. */
    public static void main(String[] args) {
        UnitTestAlienMouth unitTest = new UnitTestAlienMouth();
        unitTest.createBuffer();
        unitTest.run();
    }
}

// ====================
//     UNIT TESTING
// ====================

/**
 *  Implemets the unit testing.
 */
class UnitTestAlienMouth {
    /** Geometry of the window */
    private Frame testFrame;
    private int testFrameWidth = 800;
    private int testFrameHeight = 600;

    /** Geometry of the canvas */
    private TestCanvas testCanvas;
    private int testCanvasWidth = 500;
    private int testCanvasHeight = 500;

    /** Instance of random number generator which all the classes share */
    private Random rand;    

    /** Instances of the tested class. */
    private AlienMouth alienMouthMoving;
    private AlienMouth alienMouthStatic;

    /** Initializes a simple frame with a canvas and creates an instance of {@code Background}. */
    UnitTestAlienMouth() {
        rand = new Random();

        testFrame = new Frame();
        testFrame.setSize(testFrameWidth, testFrameHeight);
        testFrame.setLocation(100, 100);
        testFrame.addWindowListener(new FrameWA());
        testFrame.setVisible(true);
        testFrame.setResizable(true);

        Panel panel = new Panel();
        panel.setLayout(new FlowLayout());
        panel.setBackground(new Color(50, 0, 50));
        testFrame.add(panel);

        testCanvas = new TestCanvas();
        testCanvas.setSize(testCanvasWidth, testCanvasHeight);
        panel.add(testCanvas);

        alienMouthMoving = new AlienMouth(0.1f * testCanvasWidth, 0.1f * testCanvasHeight,
                0.1f * testCanvasWidth, 0.1f * testCanvasHeight,
                5000000000L, rand);
        alienMouthStatic = new AlienMouth(0.1f * testCanvasWidth, 0.3f * testCanvasHeight,
                0.0f, 0.0f,
                15000000000L, rand);
    }

    /** The main loop. */
    public void run() {
        float deltaTime = 0.0f;
        long nanoDeltaTime = 0;
        long previousTime = 0;
        long currentTime = System.nanoTime();

        while(testCanvas.isVisible()) {
            previousTime = currentTime;
            currentTime = System.nanoTime();
            nanoDeltaTime = currentTime - previousTime;

            // keep the "frames per second" rate under 50
            if (nanoDeltaTime < 20000000L) {
                try {
                    Thread.sleep((20000000L - nanoDeltaTime) / 1000000L);
                } catch(InterruptedException e) {}
                currentTime = System.nanoTime();
                nanoDeltaTime = currentTime - previousTime;
            }

            deltaTime = (float) ((double) nanoDeltaTime / 1000000000.0);

            alienMouthMoving.move(deltaTime);
            alienMouthStatic.move(deltaTime);

            testCanvas.draw();
        }
    }

    /** Creates a buffer for the double-buffered graphics. */
    public void createBuffer() {
        testCanvas.createBufferStrategy(2);
        testCanvas.strategy = testCanvas.getBufferStrategy();
    }

    /** Allows the frame to be closed. */
    private class FrameWA extends WindowAdapter {
        public void windowClosing(WindowEvent e) {
            System.exit(1);
        }
    }

    /** Displays the graphics. */
    class TestCanvas extends Canvas {
        public BufferStrategy strategy;
        public void draw() {
            Graphics g = strategy.getDrawGraphics();

            g.setColor(new Color(0, 0, 50));
            g.fillRect(0, 0, testCanvasWidth, testCanvasHeight);

            alienMouthMoving.draw(g);
            alienMouthStatic.draw(g);

            AlienMouth.drawMouth(0.2f * testCanvasWidth, 0.4f * testCanvasHeight, 1.0f, g);

            g.dispose();
            strategy.show();
        }
    }
}
