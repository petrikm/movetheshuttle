import java.util.*;
import java.awt.*;

// needed for unit test
import java.awt.event.*;
import java.awt.image.*;

/**
 *  Represents a floating, rotating eye that will appear after an explosion of an alien.
 */
public class AlienEye extends FadingParticle {
    /** Collision radius shared by all the alien eyes */
    public static final float RADIUS = 4.0f;

    /** Colors of the parts of the vector image of the eye. */
    protected static Color contourColor = new Color(0, 120, 0);
    protected static Color fillColor    = Color.white;
    protected static Color pupilColor   = new Color(50, 0, 0);

    /** Current rotation angle of the alien eye. */
    private float rotationAngle;

    /** Rotation speed of the eye in radians per second. */
    private float rotationSpeed;

    /** Ratio of pupil radius and eyeball radius. */
    private final static float PUPIL_SIZE_RATIO = 0.6f;

    /**
     *  Creates a new alien eye.
     *
     *  @param  positionX           x-coordinate of the position
     *  @param  positionY           y-coordinate of the position
     *  @param  velocityX           x-coordinate of the velocity vector
     *  @param  velocityY           y-coordinate of the velocity vector
     *  @param  lifeTime            how long the particle shall exist (in nanoseconds)
     *  @param  rand                reference to a random number generator shared by all classes of the program
     */
    public AlienEye(
            float positionX,
            float positionY,
            float velocityX,
            float velocityY,
            long lifeTime,
            Random rand) {
        super(positionX, positionY, velocityX, velocityY,
            RADIUS, new Color(255, 0, 255), lifeTime,
            rand);
        rotationAngle = 0.0f;
        rotationSpeed = 4.0f * (float) Math.PI * (rand.nextFloat() - 0.5f);
    }

    /** Moves the alien eye reflecting its velocity vector, rotation speed, and the given time interval.
     *  @param  deltaTime   time interval according to which the new position and angle of the moving object is computed */
    public void move(float deltaTime) {
        super.move(deltaTime);
        rotationAngle += deltaTime * rotationSpeed;
    }

    /** Draws an alien eyeball, without pupil.
     *  @param  x           x-coordinate of the position of the center of the eyeball
     *  @param  y           y-coordinate of the position of the center of the eyeball
     *  @param  diameter    diameter of the eyeball circle
     *  @param  opacity     inverse transparency of the image
     *  @param  g           graphic context */
    public static void drawEyeBall(int x, int y, int diameter, float opacity, Graphics g) {
        int fillRed =   Math.round(opacity * (float) fillColor.getRed());
        int fillGreen = Math.round(opacity * (float) fillColor.getGreen());
        int fillBlue =  Math.round(opacity * (float) fillColor.getBlue());
        Color fillColor = new Color(fillRed, fillGreen, fillBlue);
        g.setColor(fillColor);
        VectorImage.fillCenteredOval(x, y, diameter, diameter, g);

        int contourRed =   Math.round(opacity * (float) contourColor.getRed());
        int contourGreen = Math.round(opacity * (float) contourColor.getGreen());
        int contourBlue =  Math.round(opacity * (float) contourColor.getBlue());
        Color contourColor = new Color(contourRed, contourGreen, contourBlue);
        g.setColor(contourColor);
        VectorImage.drawCenteredOval(x, y, diameter, diameter, g);
    }

    /** Draws the pupil of an alien eyeball.
     *  @param  x           x-coordinate of the position of the center of the eye pupil
     *  @param  y           y-coordinate of the position of the center of the eye pupil
     *  @param  diameter    diameter of the eye pupil circle
     *  @param  opacity     inverse transparency of the image
     *  @param  g           graphic context */
    public static void drawEyePupil(int x, int y, int diameter, float opacity, Graphics g) {
        int pupilRed =   Math.round(opacity * (float) pupilColor.getRed());
        int pupilGreen = Math.round(opacity * (float) pupilColor.getGreen());
        int pupilBlue =  Math.round(opacity * (float) pupilColor.getBlue());
        Color pupilColor = new Color(pupilRed, pupilGreen, pupilBlue);
        g.setColor(pupilColor);
        VectorImage.fillCenteredOval(x, y, diameter, diameter, g);
    }

    /** Draws an alien eye.
     *  @param  eyePositionX    x-coordinate of the position of the center of the eyeball
     *  @param  eyePositionY    y-coordinate of the position of the center of the eyeball
     *  @param  opacity         inverse transparency of the image
     *  @param  g               graphic context */
    public static void drawEye(float eyePositionX, float eyePositionY, float opacity, Graphics g) {
        int x = Math.round(eyePositionX);
        int y = Math.round(eyePositionY);
        int diameter = Math.round(2.0f * RADIUS);
        float pupilRadius = PUPIL_SIZE_RATIO * RADIUS;
        int pupilDiameter = Math.round(2.0f * pupilRadius);
        drawEyeBall(x, y, diameter, opacity, g);
        drawEyePupil(x, y, pupilDiameter, opacity, g);
    }

    /** Draws an alien eye.
     *  @param  eyePositionX        x-coordinate of the position of the center of the eyeball
     *  @param  eyePositionY        y-coordinate of the position of the center of the eyeball
     *  @param  opacity             inverse transparency of the image
     *  @param  observedPositionX   x-coordinate of the observed object; determines where the eye pupil will be directed
     *  @param  observedPositionY   x-coordinate of the observed object; determines where the eye pupil will be directed
     *  @param  g                   graphic context */
    public static void drawEye(float eyePositionX, float eyePositionY, float opacity, 
            float observedPositionX, float observedPositionY, Graphics g) {
        int x = Math.round(eyePositionX);
        int y = Math.round(eyePositionY);
        int diameter = Math.round(2.0f * RADIUS);
        drawEyeBall(x, y, diameter, opacity, g);

        float pupilRadius = PUPIL_SIZE_RATIO * RADIUS;
        int pupilDiameter = Math.round(2.0f * pupilRadius);

        float observedDirectionX = observedPositionX - eyePositionX;
        float observedDirectionY = observedPositionY - eyePositionY;
        float observedDistance = (float) Math.sqrt((observedDirectionX * observedDirectionX) + (observedDirectionY * observedDirectionY));

        float pupilExcentricity = RADIUS - pupilRadius;

        float pupilPositionX = eyePositionX + (pupilExcentricity * observedDirectionX) / observedDistance;
        float pupilPositionY = eyePositionY + (pupilExcentricity * observedDirectionY) / observedDistance;

        int pupDrawX = Math.round(pupilPositionX);
        int pupDrawY = Math.round(pupilPositionY);

        drawEyePupil(pupDrawX, pupDrawY, pupilDiameter, opacity, g);
    }

    /** Draws an alien eye.
     *  @param  eyePositionX    x-coordinate of the position of the center of the eyeball
     *  @param  eyePositionY    y-coordinate of the position of the center of the eyeball
     *  @param  opacity         inverse transparency of the image
     *  @param  angle           determines where the eye pupil will be rotated
     *  @param  g               graphic context */
    public static void drawEye(float eyePositionX, float eyePositionY, float opacity, 
            float angle, Graphics g) {
        int x = Math.round(eyePositionX);
        int y = Math.round(eyePositionY);
        int diameter = Math.round(2.0f * RADIUS);
        drawEyeBall(x, y, diameter, opacity, g);

        float pupilRadius = PUPIL_SIZE_RATIO * RADIUS;
        int pupilDiameter = Math.round(2.0f * pupilRadius);

        float pupilExcentricity = RADIUS - pupilRadius;

        float pupilPositionX = eyePositionX + (pupilExcentricity * (float) Math.cos(angle));
        float pupilPositionY = eyePositionY + (pupilExcentricity * (float) Math.sin(angle));

        int pupDrawX = Math.round(pupilPositionX);
        int pupDrawY = Math.round(pupilPositionY);

        drawEyePupil(pupDrawX, pupDrawY, pupilDiameter, opacity, g);
    }

    /** Draws the alien eye. */
    public void draw(Graphics g) {
        drawEye(positionX, positionY, opacity, rotationAngle, g);
    }

    /** Performs unit testing. */
    public static void main(String[] args) {
        UnitTestAlienEye unitTest = new UnitTestAlienEye();
        unitTest.createBuffer();
        unitTest.run();
    }
}

// ====================
//     UNIT TESTING
// ====================

/**
 *  Implemets the unit testing.
 */
class UnitTestAlienEye {
    /** Geometry of the window */
    private Frame testFrame;
    private int testFrameWidth = 800;
    private int testFrameHeight = 600;

    /** Geometry of the canvas */
    private TestCanvas testCanvas;
    private int testCanvasWidth = 500;
    private int testCanvasHeight = 500;

    /** Instance of random number generator which all the classes share */
    private Random rand;    

    /** Instances of the tested class. */
    private AlienEye alienEyeMoving;
    private AlienEye alienEyeStatic;

    /** Initializes a simple frame with a canvas and creates an instance of {@code Background}. */
    UnitTestAlienEye() {
        rand = new Random();

        testFrame = new Frame();
        testFrame.setSize(testFrameWidth, testFrameHeight);
        testFrame.setLocation(100, 100);
        testFrame.addWindowListener(new FrameWA());
        testFrame.setVisible(true);
        testFrame.setResizable(true);

        Panel panel = new Panel();
        panel.setLayout(new FlowLayout());
        panel.setBackground(new Color(50, 0, 50));
        testFrame.add(panel);

        testCanvas = new TestCanvas();
        testCanvas.setSize(testCanvasWidth, testCanvasHeight);
        panel.add(testCanvas);

        alienEyeMoving = new AlienEye(0.1f * testCanvasWidth, 0.1f * testCanvasHeight,
                0.1f * testCanvasWidth, 0.1f * testCanvasHeight,
                5000000000L, rand);
        alienEyeStatic = new AlienEye(0.1f * testCanvasWidth, 0.3f * testCanvasHeight,
                0.0f, 0.0f,
                15000000000L, rand);
    }

    /** The main loop. */
    public void run() {
        float deltaTime = 0.0f;
        long nanoDeltaTime = 0;
        long previousTime = 0;
        long currentTime = System.nanoTime();

        while(testCanvas.isVisible()) {
            previousTime = currentTime;
            currentTime = System.nanoTime();
            nanoDeltaTime = currentTime - previousTime;

            // keep the "frames per second" rate under 50
            if (nanoDeltaTime < 20000000L) {
                try {
                    Thread.sleep((20000000L - nanoDeltaTime) / 1000000L);
                } catch(InterruptedException e) {}
                currentTime = System.nanoTime();
                nanoDeltaTime = currentTime - previousTime;
            }

            deltaTime = (float) ((double) nanoDeltaTime / 1000000000.0);

            alienEyeMoving.move(deltaTime);
            alienEyeStatic.move(deltaTime);

            testCanvas.draw();
        }
    }

    /** Creates a buffer for the double-buffered graphics. */
    public void createBuffer() {
        testCanvas.createBufferStrategy(2);
        testCanvas.strategy = testCanvas.getBufferStrategy();
    }

    /** Allows the frame to be closed. */
    private class FrameWA extends WindowAdapter {
        public void windowClosing(WindowEvent e) {
            System.exit(1);
        }
    }

    /** Displays the graphics. */
    class TestCanvas extends Canvas {
        public BufferStrategy strategy;
        public void draw() {
            Graphics g = strategy.getDrawGraphics();

            g.setColor(new Color(0, 0, 50));
            g.fillRect(0, 0, testCanvasWidth, testCanvasHeight);

            alienEyeMoving.draw(g);
            alienEyeStatic.draw(g);

            AlienEye.drawEye(0.2f * testCanvasWidth, 0.4f * testCanvasHeight, 1.0f, 
                    alienEyeMoving.getX(), alienEyeMoving.getY(), g);

            g.dispose();
            strategy.show();
        }
    }
}
