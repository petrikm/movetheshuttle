import java.awt.*;
import java.util.*;

// needed for unit test
import java.awt.event.*;
import java.awt.image.*;

/**
 * Represents the basic bullet fired by an alien of the type AlienShooter.
 */
public class BulletAlienShooter extends Bullet {
    private float tailX[];
    private float tailY[];
    private int lengthOfTail;

    /** 
     *  Constructor. 
     *
     *  @param  positionX           x-coordinate of the position
     *  @param  positionY           y-coordinate of the position
     *  @param  velocityX           x-coordinate of the velocity vector
     *  @param  velocityY           y-coordinate of the velocity vector
     *  @param  rand                reference to a random number generator shared by all classes of the program
     */
    BulletAlienShooter(float positionX, float positionY, float velocityX, float velocityY, Random rand) {
        super(positionX, positionY, velocityX, velocityY, 4.0f, rand);

        lengthOfTail = 5;
        tailX = new float[lengthOfTail];
        tailY = new float[lengthOfTail];
        for (int i = 0; i < lengthOfTail; ++i) {
            tailX[i] = positionX;
            tailY[i] = positionY;
        }
	}

    /** Moves the bullet. */
    public void move(float deltaTime) {
        super.move(deltaTime);
        if (lengthOfTail > 0) {
            float absoluteVelocity = getAbsoluteVelocity();
            float tailElementDistance = 3.0f;
            float diffX = tailElementDistance * velocityX / absoluteVelocity;
            float diffY = tailElementDistance * velocityY / absoluteVelocity;
            tailX[0] = positionX - diffX;
            tailY[0] = positionY - diffY;
            for (int i = 1; i < lengthOfTail; ++i) {
                tailX[i] = tailX[i - 1] - diffX;
                tailY[i] = tailY[i - 1] - diffY;
            }
        }
    }

    /** Draws the bullet. */
	public void draw(Graphics g) {
        int red = 55 + rand.nextInt(200);
        int green = rand.nextInt(red);
        g.setColor(new Color(red, green, 0));
        VectorImage.fillCenteredOval(Math.round(positionX), Math.round(positionY), diameter, diameter, g);

		for (int i = 0; i < lengthOfTail; ++i) {
            int d = Math.round(2.0f * collisionRadius * (float) (lengthOfTail - i + 1) / (float) (lengthOfTail + 2));
            red = 55 + rand.nextInt(200);
            green = rand.nextInt(red);
            g.setColor(new Color(red, green, 0));
            VectorImage.fillCenteredOval(Math.round(tailX[i]), Math.round(tailY[i]), d, d, g);
		}
	}

    /** Performs unit testing. */
    public static void main(String[] args) {
        UnitTestBulletAlienShooter unitTest = new UnitTestBulletAlienShooter();
        unitTest.createBuffer();
        unitTest.run();
    }
}

/**
 *  Implemets a unit test if the class {@code Background}.
 *
 *  Opens a frame with a canvas with an instance of {@code Background}.
 */
class UnitTestBulletAlienShooter {
    /** Geometry of the window */
    private Frame testFrame;
    private int testFrameWidth = 800;
    private int testFrameHeight = 600;

    /** Geometry of the canvas */
    private TestCanvas testCanvas;
    private int testCanvasWidth = testFrameWidth;
    private int testCanvasHeight = testFrameHeight - 100;

    /** instance of random number generator which all the classes share */
    private Random rand;    

    private BulletAlienShooter bullet;

    /** Initializes a simple frame with a canvas and creates an instance of {@code Background}. */
    UnitTestBulletAlienShooter() {
        rand = new Random();

        testFrame = new Frame();
        testFrame.setSize(testFrameWidth, testFrameHeight);
        testFrame.setLocation(100, 100);
        testFrame.addWindowListener(new FrameWA());
        testFrame.setVisible(true);
        testFrame.setResizable(true);

        Panel panel = new Panel();
        panel.setLayout(new FlowLayout());
        panel.setBackground(new Color(50, 0, 50));
        testFrame.add(panel);

        testCanvas = new TestCanvas();
        testCanvas.setSize(testCanvasWidth, testCanvasHeight);
        panel.add(testCanvas);

        bullet = new BulletAlienShooter(1.0f, 1.0f, 0.1f * testCanvasWidth, 0.1f * testCanvasHeight, rand);
    }

    /** The main loop. */
    public void run() {
        float deltaTime = 0.0f;
        long nanoDeltaTime = 0;
        long previousTime = 0;
        long currentTime = System.nanoTime();

        while(testCanvas.isVisible()) {
            previousTime = currentTime;
            currentTime = System.nanoTime();
            nanoDeltaTime = currentTime - previousTime;

            // keep frames per second rate at maximum of 50
            if (nanoDeltaTime < 20000000L) {
                try {
                    Thread.sleep((20000000L - nanoDeltaTime) / 1000000L);
                } catch(InterruptedException e) {}
                currentTime = System.nanoTime();
                nanoDeltaTime = currentTime - previousTime;
            }

            deltaTime = (float) ((double) nanoDeltaTime / 1000000000.0);

            //System.out.println("deltaTime: " + deltaTime + " FPS: " + 1 / deltaTime);

            bullet.move(deltaTime);

            testCanvas.draw();
        }
    }

    /** Creates a buffer for the double-buffered graphics. */
    public void createBuffer() {
        testCanvas.createBufferStrategy(2);
        testCanvas.strategy = testCanvas.getBufferStrategy();
    }

    /** Allows the frame to be closed. */
    private class FrameWA extends WindowAdapter {
        public void windowClosing(WindowEvent e) {
            System.exit(1);
        }
    }

    /** Displays the graphics. */
    class TestCanvas extends Canvas {
        public BufferStrategy strategy;
        public void draw() {
            Graphics g = strategy.getDrawGraphics();

            g.setColor(new Color(0, 0, 50));
            g.fillRect(0, 0, testCanvasWidth, testCanvasHeight);

            bullet.draw(g);

            g.dispose();
            strategy.show();
        }
    }
}
