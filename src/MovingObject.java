import java.awt.*;
import java.util.*;

// needed for unit test
import java.awt.event.*;
import java.awt.image.*;

/**
 *  Represents an abstract moving object on the game canvas.
 *
 *  A moving object is mainly characterised by its position, velocity vector,
 *      and collision radius.
 *
 *  Moving objects can be kept in a one-directional dynamic list; see attribute
 *      {@code next}.
 *
 *  @see ListOfMovingObjects
 */
public abstract class MovingObject {
    /** Position vector of the moving object. 
     *  The coordinates of the visible game screen (game canvas) range from 
     *      (0, 0) to (Constants.GAME_CANVAS_WIDTH, Constants.GAME_CANVAS_HEIGHT). */
    protected float positionX;
    protected float positionY;

    /** Velocity vector of the moving object. */
    protected float velocityX;
    protected float velocityY;

    /** Radius of the circle enclosing the object.
     *  Serves to detect collisions with other moving objects. 
     *  @see isCollision */
    protected float collisionRadius;

    /** For drawing purposes.
     *  Equal to the double value of {@code collisionRadius}. */
    protected int diameter;

    /** Specifies whether the moving object is in the "dying" state.
     *  This serves to safely remove moving objects from the corresponding
     *      list.
     *  In the method {@code ListOfMovingObjects.move}, every object, that is
     *      in the "dying" state, is deleted and, consequently, the attribute
     *      {@code ListOfMovingObjects.numberOfAtoms} is decreased. */
    private boolean dying;

    /** The next moving object in the list.
     *  Moving objects can be kept in a one-directional dynamic list.
     *  For this purpose, each instance of MovingObject can have a reference to
     *      its successor.
     *  Such a list is supposed to be encapsulated in an instance of the class
     *      {@code ListOfMovingObjects}.
     *  The default value is {@code null}.
     *  @see ListOfMovingObjects
     */
    protected MovingObject next;

    /** Reference to a random number generator shared by all classes of the program */
    protected Random rand;

    /** 
     *  Constructor. 
     *
     *  @param  positionX           x-coordinate of the position
     *  @param  positionY           y-coordinate of the position
     *  @param  velocityX           x-coordinate of the velocity vector
     *  @param  velocityY           y-coordinate of the velocity vector
     *  @param  collisionRadius     serves to detect collisions with other moving objects
     *  @param  rand                reference to a random number generator shared by all classes of the program
     */
    MovingObject(
            float positionX,
            float positionY,
            float velocityX,
            float velocityY,
            float collisionRadius,
            Random rand) {
        this.positionX = positionX;
        this.positionY = positionY;
        this.velocityX = velocityX;
        this.velocityY = velocityY;
        this.collisionRadius = collisionRadius;
        this.diameter = Math.round(2.0f * collisionRadius);
        this.dying = false;
        this.next = null;
        this.rand = rand;
    }

    /** Returns the x-coordinate of the position of the moving object. */
    public float getX() {
        return positionX;
    }

    /** Returns the y-coordinate of the position of the moving object. */
    public float getY() {
        return positionY;
    }

    void setPosition(float positionX, float positionY) {
        this.positionX = positionX;
        this.positionY = positionY;
    }
    
    void setVelocity(float velocityX, float velocityY) {
        this.velocityX = velocityX;
        this.velocityY = velocityY;
    }
    
    void addToVelocity(float velocityX, float velocityY) {
        this.velocityX += velocityX;
        this.velocityY += velocityY;
    }
    
    /** Returns absolute velocity of the moving object.
     *  It is a non-negative real number (i.e. not a vector). */
    public float getAbsoluteVelocity() {
        return (float) Math.sqrt((double) (velocityX * velocityX + velocityY * velocityY));
    }

    /** Returns the collision radius of the moving object. */
    public float getCollisionRadius() {
        return collisionRadius;
    }

    /** Returns a reference to the next moving object in the list. */
    public MovingObject getNext() {
        return next;
    }

    /** Determines the successor of this moving object in the dynamic list. */
    public void setNext(MovingObject next) {
        this.next = next;
    }

    /** Sets the moving object state to "dying". */
    public void die() {
        dying = true;
    }

    /** Sets the moving object state to "not dying". */
    public void revive() {
        dying = false;
    }

    /** Checks whether the moving object is "dying". */
    public boolean isDying() {
        return dying;
    }

    /**
     *  Checks whether there is a collision with another moving object. 
     *
     *  This method simply tests, whether the circles around the two objects
     *      (with the radius given by the attribute {@code collisionRadius}) intersect.
     *  If the moving object has an unusual shape then this method should be
     *      overwritten.
     */
    public boolean isCollision(MovingObject object) {
        // (dx, dy) is the (positive) difference vector given by the centers of the two objects
        float dx = positionX - object.getX();
        if (dx < 0.0f) dx = -dx;
        float dy = positionY - object.getY();
        if (dy < 0.0f) dy = -dy;
        // minimal dostance of the centers for no collision
        float threshold = collisionRadius + object.getCollisionRadius();
        // first, perform the square-box collision detection
        if((dx >= threshold) || (dy >= threshold)) return false;
        // second, perform the diamond-box collision detection
        if(dx + dy < threshold) return true;
        // finally, perform the circle collision detection
        float distance = (float) Math.sqrt((double) (dx * dx + dy * dy));
        if(distance >= threshold) {
            return false;
        } else {
            return true;
        }
    }

    /** Checks whether the moving object has gone out of the borders of the game canvas. */
    public boolean isOutOfGameCanvas() {
        if((positionX < -collisionRadius)
                    || (positionX > Constants.GAME_CANVAS_WIDTH + collisionRadius)
                    || (positionY < -collisionRadius)
                    || (positionY > Constants.GAME_CANVAS_HEIGHT + collisionRadius))
            return true;
                else
            return false;
    }

    /** Moves the object reflecting its velocity vector and the given time interval.
     *  @param  deltaTime   time interval according to which the new position of the moving object is computed */
    public void move(float deltaTime) {
        positionX += deltaTime * velocityX;
        positionY += deltaTime * velocityY;
    }

    /** Draws a circle with the collision radius around the moving object.
     *  The radius of the circle is given by the attribute {@code collisionRadius}. */
    public void drawCollisionCircle(Color color, Graphics g) {
        int centerX = Math.round(positionX);
        int centerY = Math.round(positionY);
        int fromX   = Math.round(positionX - collisionRadius);
        int fromY   = Math.round(positionY - collisionRadius);
        g.setColor(color);
        g.drawOval(fromX, fromY, diameter, diameter);
    }

    /** Draws the moving object.
     *  @param  g   graphic context */
    public void draw(Graphics g) {
        if (Constants.DISPLAY_COLLISION_CIRCLES) {
            drawCollisionCircle(Color.MAGENTA, g);
        }
    }

    /** Creates an explosion of the moving object.
     *  This method is to be called when the moving object dies.
     *  If the moving object is supposed to die in some interesting way then
     *      this is the place to define it.
     *  The usual way is to generate a number of particles which simulates
     *      an explosion.
     *  This method does nothing by default. */
    public void explode() {
    }

    /** Performs unit testing. */
    public static void main(String[] args) {
        UnitTestMovingObject unitTest = new UnitTestMovingObject();
        unitTest.createBuffer();
        unitTest.run();
    }
}

// ====================
//     UNIT TESTING
// ====================

class MovingObjectNonAbstract extends MovingObject {
    MovingObjectNonAbstract(
            float positionX,
            float positionY,
            float velocityX,
            float velocityY,
            float collisionRadius,
            Random rand) {
        super(positionX, positionY, velocityX, velocityY, collisionRadius, rand);
    }

    public void draw(Graphics g) {
        int fromX   = Math.round(positionX - collisionRadius);
        int fromY   = Math.round(positionY - collisionRadius);
        int width   = Math.round(2 * collisionRadius);
        int height  = Math.round(2 * collisionRadius);
        g.setColor(Color.green);
        g.drawRect(fromX, fromY, width, height);
        drawCollisionCircle(Color.yellow, g);
    }
}

/**
 *  Implemets a unit test.
 */
class UnitTestMovingObject {
    /** Geometry of the window */
    private Frame testFrame;
    private int testFrameWidth = 800;
    private int testFrameHeight = 600;

    /** Geometry of the canvas */
    private TestCanvas testCanvas;
    private int testCanvasWidth = 500;
    private int testCanvasHeight = 500;

    /** instance of random number generator which all the classes share */
    private Random rand;    

    private MovingObjectNonAbstract movingObject;
    private MovingObjectNonAbstract staticObject;

    /** Initializes a simple frame with a canvas and creates an instance of {@code Background}. */
    UnitTestMovingObject() {
        rand = new Random();

        testFrame = new Frame();
        testFrame.setSize(testFrameWidth, testFrameHeight);
        testFrame.setLocation(100, 100);
        testFrame.addWindowListener(new FrameWA());
        testFrame.setVisible(true);
        testFrame.setResizable(true);

        Panel panel = new Panel();
        panel.setLayout(new FlowLayout());
        panel.setBackground(new Color(50, 0, 50));
        testFrame.add(panel);

        testCanvas = new TestCanvas();
        testCanvas.setSize(testCanvasWidth, testCanvasHeight);
        panel.add(testCanvas);

        movingObject = new MovingObjectNonAbstract(0.0f, 0.0f, 
                0.05f * testCanvasWidth, 0.05f * testCanvasHeight, 
                50.0f, rand);
        staticObject = new MovingObjectNonAbstract(200.0f, 200.0f, 
                0.0f, 0.0f, 
                50.0f, rand);
    }

    /** The main loop. */
    public void run() {
        float deltaTime = 0.0f;
        long nanoDeltaTime = 0;
        long previousTime = 0;
        long currentTime = System.nanoTime();

        while(testCanvas.isVisible()) {
            previousTime = currentTime;
            currentTime = System.nanoTime();
            nanoDeltaTime = currentTime - previousTime;

            // keep frames per second rate at maximum of 50
            if (nanoDeltaTime < 20000000L) {
                try {
                    Thread.sleep((20000000L - nanoDeltaTime) / 1000000L);
                } catch(InterruptedException e) {}
                currentTime = System.nanoTime();
                nanoDeltaTime = currentTime - previousTime;
            }

            deltaTime = (float) ((double) nanoDeltaTime / 1000000000.0);

            System.out.println("isCollision: " + staticObject.isCollision(movingObject) + " " + movingObject.isCollision(staticObject));

            movingObject.move(deltaTime);
            staticObject.move(deltaTime);

            testCanvas.draw();
        }
    }

    /** Creates a buffer for the double-buffered graphics. */
    public void createBuffer() {
        testCanvas.createBufferStrategy(2);
        testCanvas.strategy = testCanvas.getBufferStrategy();
    }

    /** Allows the frame to be closed. */
    private class FrameWA extends WindowAdapter {
        public void windowClosing(WindowEvent e) {
            System.exit(1);
        }
    }

    /** Displays the graphics. */
    class TestCanvas extends Canvas {
        public BufferStrategy strategy;
        public void draw() {
            Graphics g = strategy.getDrawGraphics();

            g.setColor(new Color(0, 0, 50));
            g.fillRect(0, 0, testCanvasWidth, testCanvasHeight);

            movingObject.draw(g);
            staticObject.draw(g);

            g.dispose();
            strategy.show();
        }
    }
}
