import java.awt.*;
import java.util.*;

// needed for unit test
import java.awt.event.*;
import java.awt.image.*;

/**
 *  Represents a moving particle which fades.
 *
 *  It is a filled circle of a defined color moving by a constant velocity. 
 *  It fades until it disappears completely - then it is destroyed.
 */
public class FadingParticle extends Particle {
    /**
     *  Original color of the particle.
     *
     *  This value remains constant while the variable {@code color} changes its
     *      value periodically until it turns zero (black).
     *  Then the particle is destroyed.
     */
    protected Color originalColor;

    /** Inverse transparency (or alpha) of the particle. 
     *  The fading procedure is the following: 
     *      opacity goes gradually from 1.0f to 0.0f and before the particle is
     *      drawn, its original color is multiplied by the opacity.
     *  Hence the particle fades to black, no real transparency. */
    protected float opacity;

    /** The time when the fading particle has been created. */
    private long birthTime;
    /** How long the particle shall exist. 
     *  Hence {@code birthTime + lifeTime} is the time when the fading particle
     *      will be destroyed. */
    private long lifeTime;

    /**
     *  Creates a new fading particle.
     *
     *  @param  positionX           x-coordinate of the position
     *  @param  positionY           y-coordinate of the position
     *  @param  velocityX           x-coordinate of the velocity vector
     *  @param  velocityY           y-coordinate of the velocity vector
     *  @param  collisionRadius     serves to detect collisions with other moving objects
     *  @param  color               color of the particle
     *  @param  lifeTime            how long the particle shall exist (in nanoseconds)
     *  @param  rand                reference to a random number generator shared by all classes of the program
     */
    public FadingParticle(
            float positionX,
            float positionY,
            float velocityX,
            float velocityY,
            float collisionRadius,
            Color color,
            long lifeTime,
            Random rand) {
        super(positionX, positionY, velocityX, velocityY, collisionRadius, color, rand);
        this.originalColor = color;
        this.birthTime = System.nanoTime();
        this.lifeTime = lifeTime;
    }

    /** Moves the fading particle.
     *  Furthermore, new color of the particle is computed and {@code lifeTime} is decreased. */
    public void move(float deltaTime) {
        long currentTime = System.nanoTime();
        if (currentTime > birthTime + lifeTime) {
            die();
        } else {
            opacity = (float) (birthTime + lifeTime - currentTime) / (float) lifeTime;
            super.move(deltaTime);
        }
    }

    /** Draws the fading particle. */
    public void draw(Graphics g) {
        int red =   Math.round(opacity * (float) originalColor.getRed());
        int green = Math.round(opacity * (float) originalColor.getGreen());
        int blue =  Math.round(opacity * (float) originalColor.getBlue());
        color = new Color(red, green, blue);
        super.draw(g);
    }

    /** Performs unit testing. */
    public static void main(String[] args) {
        UnitTestFadingParticle unitTest = new UnitTestFadingParticle();
        unitTest.createBuffer();
        unitTest.run();
    }
}

// ====================
//     UNIT TESTING
// ====================

/**
 *  Implemets a unit test if the class {@code Background}.
 *
 *  Opens a frame with a canvas with an instance of {@code Background}.
 */
class UnitTestFadingParticle {
    /** Geometry of the window */
    private Frame testFrame;
    private int testFrameWidth = 800;
    private int testFrameHeight = 600;

    /** Geometry of the canvas */
    private TestCanvas testCanvas;
    private int testCanvasWidth = testFrameWidth;
    private int testCanvasHeight = testFrameHeight - 100;

    /** instance of random number generator which all the classes share */
    private Random rand;    

    private FadingParticle particle;

    /** Initializes a simple frame with a canvas and creates an instance of {@code Background}. */
    UnitTestFadingParticle() {
        rand = new Random();

        testFrame = new Frame();
        testFrame.setSize(testFrameWidth, testFrameHeight);
        testFrame.setLocation(100, 100);
        testFrame.addWindowListener(new FrameWA());
        testFrame.setVisible(true);
        testFrame.setResizable(true);

        Panel panel = new Panel();
        panel.setLayout(new FlowLayout());
        panel.setBackground(new Color(50, 0, 50));
        testFrame.add(panel);

        testCanvas = new TestCanvas();
        testCanvas.setSize(testCanvasWidth, testCanvasHeight);
        panel.add(testCanvas);

        particle = new FadingParticle(0.1f * testCanvasWidth,
                            0.1f * testCanvasHeight,
                            0.1f * testCanvasWidth,
                            0.1f * testCanvasHeight,
                            5.0f,
                            Color.yellow,
                            2000000000L,
                            rand);
    }

    /** The main loop. */
    public void run() {
        float deltaTime = 0.0f;
        long nanoDeltaTime = 0;
        long previousTime = 0;
        long currentTime = System.nanoTime();

        while(testCanvas.isVisible()) {
            previousTime = currentTime;
            currentTime = System.nanoTime();
            nanoDeltaTime = currentTime - previousTime;

            // keep frames per second rate at maximum of 50
            if (nanoDeltaTime < 20000000L) {
                try {
                    Thread.sleep((20000000L - nanoDeltaTime) / 1000000L);
                } catch(InterruptedException e) {}
                currentTime = System.nanoTime();
                nanoDeltaTime = currentTime - previousTime;
            }

            deltaTime = (float) ((double) nanoDeltaTime / 1000000000.0);

            //System.out.println("deltaTime: " + deltaTime + " FPS: " + 1 / deltaTime);

            particle.move(deltaTime);

            testCanvas.draw();
        }
    }

    /** Creates a buffer for the double-buffered graphics. */
    public void createBuffer() {
        testCanvas.createBufferStrategy(2);
        testCanvas.strategy = testCanvas.getBufferStrategy();
    }

    /** Allows the frame to be closed. */
    private class FrameWA extends WindowAdapter {
        public void windowClosing(WindowEvent e) {
            System.exit(1);
        }
    }

    /** Displays the graphics. */
    class TestCanvas extends Canvas {
        public BufferStrategy strategy;
        public void draw() {
            Graphics g = strategy.getDrawGraphics();

            g.setColor(new Color(0, 0, 50));
            g.fillRect(0, 0, testCanvasWidth, testCanvasHeight);

            particle.draw(g);

            g.dispose();
            strategy.show();
        }
    }
}
