/**
 *  Contains the global constants of the program.
 */
public class Constants {
    /** Path to the '.fig' files with models. */
	public static final String DIRECTORY_WITH_MODELS = "models/";

    /** If {@code true} the circles with collision radius around models will be displayed.
     *  For debugging purposes. */
	public static final boolean DISPLAY_COLLISION_CIRCLES = false;

    /** Default geometry of the game canvas. */
	public static final int GAME_CANVAS_WIDTH = 800;
	public static final int GAME_CANVAS_HEIGHT = 800;
	//public static final int GAME_CANVAS_WIDTH = 670;
	//public static final int GAME_CANVAS_HEIGHT = 670;
	public static final float GAME_CANVAS_DIAGONAL 
        = (float) Math.sqrt(GAME_CANVAS_WIDTH * GAME_CANVAS_WIDTH + GAME_CANVAS_HEIGHT * GAME_CANVAS_HEIGHT);

    /** Models loaded from '.fig' files are too big to be diplayed directly.
     *  Therefore they are scaled by the following constants before drawn. */
	public static final float FIG_IMAGE_SCALE = 0.01f;
}
