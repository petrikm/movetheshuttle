import java.util.*;
import java.awt.*;

// needed for unit test
import java.awt.event.*;
import java.awt.image.*;

/**
 *  Implements the animated star sky at the background of the game canvas.
 *
 *  The class animates a black sky with white stars of different sizes and
 *      brightnesses moving from right to left.
 *  The brightness and the speed of a star is influences by its distance
 *      attribute.
 */
public class Background {
    // geometry of the canvas
    private int gameCanvasWidth;
    private int gameCanvasHeight;

    private Star [] stars;
    private int numStars;

    // reference to a random number generator
    private Random rand;

    /**
     * Speed of the stars.
     *
     * How far a star will move in one animation step. This is of course
     * modified also by the distance of the star.
     */
    private float starSpeed;

    /**
     * Creates a new background with an animated star sky.
     *
     * @param gameCanvasWidth   width of the game canvas
     * @param gameCanvasHeight  height of the game canvas
     * @param numStars          number of stars which will be generated to appear on the animated sky
     * @param starSpeed         speed of the stars (pixels per second, however, affected by {@code Star.distance})
     * @param rand                random number generator shared by all classes
     */
    Background(int gameCanvasWidth, int gameCanvasHeight, Random rand) {
        this.gameCanvasWidth = gameCanvasWidth;
        this.gameCanvasHeight = gameCanvasHeight;
        this.numStars = 100;
        this.starSpeed = 150.0f;
        this.rand = rand;

        stars = new Star [numStars];

        for (int i = 0; i < numStars; ++i) {
            stars[i] = new Star();
        }
    }

    /** Moves all the stars according to their speed and the given delta-time. */
    public void move(float deltaTime) {
        for (int i = 0; i < numStars; ++i) {
            stars[i].move(deltaTime);
        }
    }

    /** Draws the sky with the stars. */
    public void draw(Graphics g) {
        g.setColor(Color.black);
        g.fillRect(0,0,gameCanvasWidth,gameCanvasHeight);
        for (int i = 0; i < numStars; ++i) {
            stars[i].draw(g);
        }
    }

    /**
     *  Represents a moving star.
     *
     *  A star is described by its (x,y) position, distance from the observer,
     *      and by its size.
     *  According to these values the brightness and the speed of the star are
     *      computed.
     */
    private class Star {
        private float x;
        private float y;
        private float distance;
        private int size;

        /** Represents the brightness of the star.
         *  It is a color interpolated between white and black according to the
         *  value of {@code distance}. */
        Color color;

        /**
         *  Randomly generates the position, the distance, and the size of the
         *      star.
         *  The attribute {@code color} is computed according to the value of
         *      {@code distance}.
         */
        Star() {
            x = (float) rand.nextInt(gameCanvasWidth);
            y = (float) rand.nextInt(gameCanvasHeight);
            size = rand.nextInt(3) + 1;
            distance = (float) (rand.nextInt(250) + 5);
            color = new Color(255 - (int) distance, 255 - (int) distance, 255 - (int) distance);
        }

        /** Moves the star by one step according to its speed and distance. */
        public void move(float deltaTime) {
            x -= starSpeed * deltaTime / distance;
        }

        /** Draws the star as a small circle filled with the given color. */
        public void draw(Graphics g) {
            g.setColor(color);
            g.fillOval((int) x, (int) y, size, size);
        }
    }

    /** Performs unit testing. */
    public static void main(String[] args) {
        UnitTestBackground unitTest = new UnitTestBackground();
        unitTest.createBuffer();
        unitTest.run();
    }
}

// ====================
//     UNIT TESTING
// ====================

/**
 *  Implemets a unit test if the class {@code Background}.
 *
 *  Opens a frame with a canvas with an instance of {@code Background}.
 */
class UnitTestBackground {
    /** Geometry of the window */
    private Frame testFrame;
    private int testFrameWidth = 800;
    private int testFrameHeight = 600;

    /** Geometry of the canvas */
    private TestCanvas testCanvas;
    private int testCanvasWidth = testFrameWidth;
    private int testCanvasHeight = testFrameHeight;

    /** instance of random number generator which all the classes share */
    private Random rand;    

    private Background background;

    /** Initializes a simple frame with a canvas and creates an instance of {@code Background}. */
    UnitTestBackground() {
        rand = new Random();

        testFrame = new Frame();
        testFrame.setSize(testFrameWidth, testFrameHeight);
        testFrame.setLocation(100, 100);
        testFrame.addWindowListener(new FrameWA());
        testFrame.setVisible(true);
        testFrame.setResizable(false);

        Panel panel = new Panel();
        panel.setLayout(new FlowLayout());
        panel.setBackground(new Color(50, 0, 50));
        testFrame.add(panel);

        testCanvas = new TestCanvas();
        testCanvas.setSize(testCanvasWidth, testCanvasHeight);
        panel.add(testCanvas);

        background = new Background(testCanvasWidth, testCanvasHeight, rand);
    }

    /** The main loop. */
    public void run() {
        float deltaTime = 0.0f;
        long nanoDeltaTime = 0;
        long previousTime = 0;
        long currentTime = System.nanoTime();

        while(testCanvas.isVisible()) {
            previousTime = currentTime;
            currentTime = System.nanoTime();
            nanoDeltaTime = currentTime - previousTime;

            // keep frames per second rate at maximum of 50
            if (nanoDeltaTime < 20000000L) {
                try {
                    Thread.sleep((20000000L - nanoDeltaTime) / 1000000L);
                } catch(InterruptedException e) {}
                currentTime = System.nanoTime();
                nanoDeltaTime = currentTime - previousTime;
            }
            deltaTime = (float) nanoDeltaTime / 1000000000.0f;

            background.move(deltaTime);

            testCanvas.draw();
        }
    }

    /** Creates a buffer for the double-buffered graphics. */
    public void createBuffer() {
        testCanvas.createBufferStrategy(2);
        testCanvas.strategy = testCanvas.getBufferStrategy();
    }

    /** Allows the frame to be closed. */
    private class FrameWA extends WindowAdapter {
        public void windowClosing(WindowEvent e) {
            System.exit(1);
        }
    }

    /** Displays the graphics. */
    class TestCanvas extends Canvas {
        public BufferStrategy strategy;
        public void draw() {
            Graphics g = strategy.getDrawGraphics();

            background.draw(g);

            g.dispose();
            strategy.show();
        }
    }
}
