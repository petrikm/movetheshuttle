import java.awt.*;
import java.util.*;

/**
 *  Represents the most basic "green" alien.
 *
 *  The alien follows slowly the shuttle being attracted by it.
 *  If the alien gets to a collision with the shuttle, then the alien dies and
 *      the shuttle loses one shield, or dies if it has no shields.
 */
public class Alien extends AttractedMovingObject {
    /** The position at which the alien looks. */
    protected float observedPositionX;
    protected float observedPositionY;

    /** To draw the alien. */
    protected Color contourColor;
    protected Color fillColor;

    /** Collision radius shared by all the aliens. */
    public static final float COLLISION_RADIUS = 14.0f;

    /** Geometry of the alien image. */
    public static final float alienWidth =      1.6f * COLLISION_RADIUS;
    public static final float alienHeight =     2.0f * COLLISION_RADIUS;
    public static final float eyePositionX =    0.4f * COLLISION_RADIUS;
    public static final float eyePositionY =    0.2f * COLLISION_RADIUS;
    public static final float mouthPositionY =  0.5f * COLLISION_RADIUS;

    //public static final float SPEED =           1.0f; // to 1.5f
    //public static final float ACCELERATION =    0.5f; // to 1.0f
    public static final float START_SPEED =           1.0f;
    public static final float START_ACCELERATION =    0.5f;
    public static final float LIMIT_SPEED =           1.5f;
    public static final float LIMIT_ACCELERATION =    1.0f;
    public static final float RESISTANCE =      0.4f;

    public static final float REPULSIVITY =     7.5f;

    /** Externally created list of particles.
     *  Keeps a reference to this lists in order to add there new instances of
     *      particles, etc.
     *  This is useful when the alien explodes and creates red and black
     *      particles that simulate the fire. */
    protected ListOfMovingObjects listOfParticles;

    /** 
     *  Constructor. 
     *
     *  @param  positionX       x-coordinate of the position
     *  @param  positionY       y-coordinate of the position
     *  @param  listOfParticles externally created list of particles
     *  @param  score           current player's score; determines how tough the newly created alien shall be
     *  @param  rand            reference to a random number generator shared by all classes of the program
     */
    Alien(float positionX, float positionY,
            ListOfMovingObjects listOfParticles,
            int score,
            Random rand) {
    super(positionX, positionY, 0.0f, 0.0f, COLLISION_RADIUS,
            Utils.getAsymptoticInterpolation(score, 0.0f, 2.0f * ListOfAliens.MILESTONE_CLONER, START_SPEED, LIMIT_SPEED),
            Utils.getAsymptoticInterpolation(score, 0.0f, 2.0f * ListOfAliens.MILESTONE_CLONER, START_ACCELERATION, LIMIT_ACCELERATION),
            RESISTANCE, rand);
        observedPositionX = 0.0f;
        observedPositionY = 0.0f;
        this.listOfParticles = listOfParticles;
        this.contourColor = new Color(0, 120, 0);
        this.fillColor = Color.green;
    }

    /** Sets the position of the attractor which is the same as the position of the observed object. */
    public void setAttractorPosition(float newAttractorX, float newAttractorY) {
        super.setAttractorPosition(newAttractorX, newAttractorY);
        this.observedPositionX = newAttractorX;
        this.observedPositionY = newAttractorY;
    }

    /** Forces the aliens to keep some distance.
     *  If this method would not be implemented then the aliens would tend to
     *      get closer and closer to each other and eventually they would overlap
     *      which is undersirable.
     *  Calling this method will make two aliens to move from each other
     *      regarding their distance.
     *  @param  otherAlien  instance of the other alien which is supposed to be repulsed from this alien
     *  @param  deltaTime   time interval according to which the new position of the moving object is computed */
    public void repulseOtherAlien(Alien otherAlien, float deltaTime) {
        float differenceX = otherAlien.getX() - positionX;
        float differenceY = otherAlien.getY() - positionY;
        float distance = (float) Math.sqrt(differenceX * differenceX + differenceY * differenceY);
        float dvx = deltaTime * REPULSIVITY * differenceX / distance;
        float dvy = deltaTime * REPULSIVITY * differenceY / distance;
        addToVelocity(-dvx, -dvy);
        otherAlien.addToVelocity(dvx, dvy);
    }

//    // testuje zda byl nejakou strelou zasazen
//    public void handleShooting(Bullets bullets) {
//        Bullet b;
//        double bulX;
//        double bulY;
//
//        b = bullets.getFirst();
//
//        while(b != null) {
//            if(b.isHit(x, y, size)) {
//                die();
//                break;
//            }
//            b = b.getNext();
//        }
//    }

    /** Creates an explosion of the alien.
     *  Calling this method does not make the alien dead.
     *  For this purpose the method {@code die} needs to be called. 
     *  On the other hand, calling the method {@code die} will make the alien
     *      explode, see {@code Alien.move}.
     *  @see ListOfAliens.move */
    public void explode() {
        float eyeVelocityX = 10.0f + 50.0f * rand.nextFloat();
        float eyeVelocityY = 2.0f * eyeVelocityX * (rand.nextFloat() - 0.5f);
        // right eye
        listOfParticles.add(new AlienEye(
                    positionX + eyePositionX, positionY + eyePositionY,
                    eyeVelocityX, eyeVelocityY,
                    1000000L * rand.nextInt(3000) + 2000000000L,
                    rand));
        eyeVelocityX = 10.0f + 50.0f * rand.nextFloat();
        eyeVelocityY = 2.0f * eyeVelocityX * (rand.nextFloat() - 0.5f);
        // left eye
        listOfParticles.add(new AlienEye(
                    positionX - eyePositionX, positionY + eyePositionY,
                    -eyeVelocityX, eyeVelocityY,
                    1000000L * rand.nextInt(3000) + 2000000000L,
                    rand));

        float angle;
        float speed;
        for(int i = 0; i < 150; ++i) {
            angle = 2.0f * (float) Math.PI * rand.nextFloat();
            speed = 1.0f + 150.0f * rand.nextFloat();

            Color color;
            switch(rand.nextInt(10)) {
                case 0:
                    color = contourColor;
                    break;
                case 1:
                    color = fillColor;
                    break;
                default:
                    color = new Color(rand.nextInt(10)+245, rand.nextInt(55), 0);
                    break;
            }
            // make the initial radius of the explosion to correspond with the collision radius
            float norm = collisionRadius * rand.nextFloat(); 
            float particlePositionX = positionX + norm * (float) Math.cos(angle);
            float particlePositionY = positionY + norm * (float) Math.sin(angle);
            listOfParticles.add(new FadingParticle(particlePositionX, particlePositionY,
                        speed * (float) Math.cos(angle), speed * (float) Math.sin(angle),
                        1.0f + 3.0f * rand.nextFloat(),
                        color,
                        1000000L * rand.nextInt(1000) + 500000000L,
                        rand));
        }
    }

    /** Draws the alien. 
     *  Allows to set alien colors. */
    public void drawAlien(Color contourColor, Color fillColor, Graphics g) {
        int drawX = Math.round(positionX);
        int drawY = Math.round(positionY);
        int drawWidth = Math.round(alienWidth);
        int drawHeight = Math.round(alienHeight);

        g.setColor(fillColor);
        VectorImage.fillCenteredOval(drawX, drawY, drawWidth, drawHeight, g);
        g.setColor(contourColor);            
        VectorImage.drawCenteredOval(drawX, drawY, drawWidth, drawHeight, g);

        AlienEye.drawEye(positionX - eyePositionX, positionY - eyePositionY, 1.0f, 
            observedPositionX, observedPositionY, g);
        AlienEye.drawEye(positionX + eyePositionX, positionY - eyePositionY, 1.0f, 
            observedPositionX, observedPositionY, g);

        AlienMouth.drawMouth(positionX, positionY + mouthPositionY, 1.0f, g);
    }

    /** Draws the alien. */
    public void draw(Graphics g) {
        if (! isDying()) {
            super.draw(g);
            drawAlien(contourColor, fillColor, g);
        }
    }
}
