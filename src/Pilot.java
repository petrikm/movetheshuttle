import java.awt.*;
import java.util.*;

// needed for unit test
import java.awt.event.*;
import java.awt.image.*;

/** Represents a pilot flying in a space.
 *
 *  It is a picture of the pilot of the shuttle which rotates and moves by a
 *      constant speed with a constant direction until it reaches the border of the
 *      game canvas.
 *  It is meant to be a decoration when the shuttle explodes.
 */
public class Pilot extends RotatingMovingObject {
   
    /** Image of the pilot. */
	private static VectorImage pilotModel = new VectorImage(Constants.DIRECTORY_WITH_MODELS + "pilot.fig");

    /** A boolean specifying whether the writing "PILOT" will be visible. */
    private boolean withWriting;

    /**
     *  Creates a new flying pilot with given position and velocity and with random rotation speed.
     *
     *  @param  positionX           x-coordinate of the position
     *  @param  positionY           y-coordinate of the position
     *  @param  velocityX           x-coordinate of the velocity vector
     *  @param  velocityY           y-coordinate of the velocity vector
     *  @param  rand                reference to a random number generator shared by all classes of the program
     */
    Pilot(float positionX, float positionY, float velocityX, float velocityY, Random rand) {
        super(positionX, positionY, velocityX, velocityY, 8.0f, rand);

		//pilotModel = new VectorImage();
		//pilotModel.load(Constants.DIRECTORY_WITH_MODELS + "pilot.fig");

        //if(rand.nextInt(3) == 0) {
        //    withWriting = true;
        //} else {
        //    withWriting = false;
        //}
        
        withWriting = false;
    }

    /** Moves the pilot. */
	public void move(float deltaTime) {
		super.move(deltaTime);
		if (isOutOfGameCanvas()) die();
	}

    /** Writes the word "PILOT" at the given position.
     *
     *  @param  x       x-coordinate of the writing
     *  @param  y       y-coordinate of the writing
     *  @param  step    size of the space between individual letters; affects proportionally also the size of the letters
     *  @param  color   color of the writing
     *  @param  g       graphic context
	 */
    private void writePilot(int x, int y, int step, Color color, Graphics g) {
        int cursor = x;
        cursor = cursor + step + (step * VectorImage.drawCharacter('P', cursor, y, step, color, g));
        cursor = cursor + step + (step * VectorImage.drawCharacter('I', cursor, y, step, color, g));
        cursor = cursor + step + (step * VectorImage.drawCharacter('L', cursor, y, step, color, g));
        cursor = cursor + step + (step * VectorImage.drawCharacter('O', cursor, y, step, color, g));
        cursor = cursor + step + (step * VectorImage.drawCharacter('T', cursor, y, step, color, g));
    }

    /** Draws the pilot. */
    public void draw(Graphics g) {
        super.draw(g);
		pilotModel.rotateTranslateAndScale(positionX, positionY,
                    directionX, directionY, Constants.FIG_IMAGE_SCALE);
		pilotModel.draw(g);
        if (withWriting) writePilot(Math.round(positionX) + 20, Math.round(positionY) - 12, 2, Color.gray, g);
    }

    /** Performs unit testing. */
    public static void main(String[] args) {
        UnitTestPilot unitTest = new UnitTestPilot();
        unitTest.createBuffer();
        unitTest.run();
    }
}

// ====================
//     UNIT TESTING
// ====================

/**
 *  Implemets a unit test.
 */
class UnitTestPilot {
    /** Geometry of the window */
    private Frame testFrame;
    private int testFrameWidth = 800;
    private int testFrameHeight = 600;

    /** Geometry of the canvas */
    private TestCanvas testCanvas;
    private int testCanvasWidth = 500;
    private int testCanvasHeight = 500;

    /** instance of random number generator which all the classes share */
    private Random rand;    

    private Pilot movingObject;
    private Pilot staticObject;
    private ListOfMovingObjects listOfPilots;

    /** Initializes a simple frame with a canvas and creates an instance of {@code Background}. */
    UnitTestPilot() {
        rand = new Random();

        testFrame = new Frame();
        testFrame.setSize(testFrameWidth, testFrameHeight);
        testFrame.setLocation(100, 100);
        testFrame.addWindowListener(new FrameWA());
        testFrame.setVisible(true);
        testFrame.setResizable(true);

        Panel panel = new Panel();
        panel.setLayout(new FlowLayout());
        panel.setBackground(new Color(50, 0, 50));
        testFrame.add(panel);

        testCanvas = new TestCanvas();
        testCanvas.setSize(testCanvasWidth, testCanvasHeight);
        panel.add(testCanvas);

        movingObject = new Pilot(0.0f, 0.0f, 
                0.05f * testCanvasWidth, 0.05f * testCanvasHeight, 
                rand);
        staticObject = new Pilot(250.0f, 250.0f, 
                0.0f, 0.0f, 
                rand);
        listOfPilots = new ListOfMovingObjects();
    }

    /** The main loop. */
    public void run() {
        float deltaTime = 0.0f;
        long nanoDeltaTime = 0;
        long previousTime = 0;
        long currentTime = System.nanoTime();

        float timeToNextPilot = 1.0f;

        while(testCanvas.isVisible()) {
            previousTime = currentTime;
            currentTime = System.nanoTime();
            nanoDeltaTime = currentTime - previousTime;

            // keep frames per second rate at maximum of 50
            if (nanoDeltaTime < 20000000L) {
                try {
                    Thread.sleep((20000000L - nanoDeltaTime) / 1000000L);
                } catch(InterruptedException e) {}
                currentTime = System.nanoTime();
                nanoDeltaTime = currentTime - previousTime;
            }

            deltaTime = (float) ((double) nanoDeltaTime / 1000000000.0);

            timeToNextPilot -= deltaTime;
            if (timeToNextPilot <= 0.0f) {
                float angle = 2.0f * (float) Math.PI * rand.nextFloat();
                float speed = 25.0f + 50.0f * rand.nextFloat();
                float velX = speed * (float) Math.cos(angle);
                float velY = speed * (float) Math.sin(angle);
                listOfPilots.add(new Pilot(250.0f, 250.0f, velX, velY, rand));
                timeToNextPilot = 1.0f;
            }

            //System.out.println("deltaTime: " + deltaTime + " FPS: " + 1 / deltaTime);
            
            //System.out.println("isCollision: " + staticObject.isCollision(movingObject) + " " + movingObject.isCollision(staticObject));

            movingObject.move(deltaTime);
            staticObject.move(deltaTime);
            listOfPilots.move(deltaTime);

            testCanvas.draw();
        }
    }

    /** Creates a buffer for the double-buffered graphics. */
    public void createBuffer() {
        testCanvas.createBufferStrategy(2);
        testCanvas.strategy = testCanvas.getBufferStrategy();
    }

    /** Allows the frame to be closed. */
    private class FrameWA extends WindowAdapter {
        public void windowClosing(WindowEvent e) {
            System.exit(1);
        }
    }

    /** Displays the graphics. */
    class TestCanvas extends Canvas {
        public BufferStrategy strategy;
        public void draw() {
            Graphics g = strategy.getDrawGraphics();

            g.setColor(new Color(0, 0, 50));
            g.fillRect(0, 0, testCanvasWidth, testCanvasHeight);

            movingObject.draw(g);
            staticObject.draw(g);
            staticObject.drawCollisionCircle(Color.yellow, g);
            listOfPilots.draw(g);

            g.dispose();
            strategy.show();
        }
    }
}
