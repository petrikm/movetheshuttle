import java.awt.*;

/**
 *  ListOfMovingObjects implements an unsorted one-directional dynamic list of
 *      moving objects.
 *
 *  Every moving object (an istance of {@code MovingObject}) contains a
 *      reference to its successor.
 *
 *  @see    MovingObject
 */
public class ListOfMovingObjects {
    /** Reference to the first object in the list. */
    public MovingObject first;

    /** The length of the list. */
    protected int numberOfAtoms;

    /** Indicates whether this list deletes automatically its atom in order to
     *      keep reasonable "frame per second" rate.
     *  If set to {@code true} then this list will delete its oldest atoms if
     *      {@code deltaTime} exceeds {@code maxDeltaTime}. */
    protected boolean deltaTimeKeeper;
    /** Maximal allowed value of {@code deltaTime}.
     *  Needed only when {@code deltaTimeKeeper == true}.
     *  The value of {@code deltaTime} is obtained as a parameter in the method
     *      {@code move}. */
    protected float maxDeltaTime;

    /** Creates an empty list. */
    ListOfMovingObjects() {
        clear();
    }

    /** Clears the list making it an empty list. */
    public void clear() {
        first = null;
        numberOfAtoms = 0;
    }

    /** Adds a new moving object or a list of moving objects to the beginning of the list.
     *  Hence, the value of the attribute {@code first} will be changed to the
     *      value of the parameter {@code object}. */
    public void add(MovingObject object) {
        if(object != null) {
            MovingObject last = null;
            for (MovingObject iter = object; iter != null; iter = iter.getNext()) {
                last = iter;
                ++ numberOfAtoms;
            }
            if(first == null) {
                first = object;
            } else {
                last.setNext(first);
                first = object;
            }
        }
    }

    /** Returns the number of moving objects is the list. */
    public int getNumber() {
        return numberOfAtoms;
    }

    /** Computes and returns the number of moving objects is the list. */
    public int computeNumber() {
        int result = 0;
        for(MovingObject iter = first; iter != null; iter = iter.getNext()) {
            ++result;
        }
        return result;
    }

    /** Sets this list to the state of keeping reasonable "frame per second" rate.
     *  After calling this method this list will delete automatically its
     *      oldest atoms if {@code deltaTime} exceeds {@code maxDeltaTime}. 
     *  The value of {@code deltaTime} is obtained as a parameter in the method
     *      {@code move}. */
    public void setAsDeltaTimeKeeper(float maxDeltaTime) {
        deltaTimeKeeper = true;
        this.maxDeltaTime = maxDeltaTime;
    }

    /** Checks whether the list is empty. */
    public boolean isEmpty() {
        if(first == null)
            return true;
        else
            return false;
    }

    /** Returns the reference to the first moving object in the list.
     *  If the list is empty, {@code null} is returned. */
    public MovingObject getFirst() {
        return first;
    }

    /** Checks whether there is an object in the list which is in a collision
     *      with the moving object given as the parameter. 
     *  @return {@code true} if there is a collision, {@code false} otherwise */
    public boolean isCollision(MovingObject object) {
        for(MovingObject iter = first; iter != null; iter = iter.getNext()) {
            if(iter.isCollision(object)) {
                return true;
            }
        }
        return false;
    }

    /** Moves all the moving objects in the list. */
    public void move(float deltaTime) {
        MovingObject iter = first;
        MovingObject prev = null;

        int atomsToDelete = 1000;
        boolean cutting = deltaTimeKeeper && deltaTime > maxDeltaTime;

        int index = 0;
        while(iter != null) {
            if(iter.isDying()) {
                if(prev == null) {
                    first = first.getNext();
                    iter = first;
                } else {
                    prev.setNext(iter.getNext());
                    iter = iter.getNext();
                }
                -- numberOfAtoms;
            } else {
                iter.move(deltaTime);
                prev = iter;
                iter = iter.getNext();
                ++ index;
                if (cutting && (numberOfAtoms - index) <= atomsToDelete) {
                    iter.setNext(null);
                    numberOfAtoms = index + 1;
                    break;
                }
            }
        }
    }

    /** Draws all the moving objects in the list. */
    public void draw(Graphics g) {
        for (MovingObject iter = first; iter != null; iter = iter.getNext()) {
            iter.draw(g);
        }
    }
}
