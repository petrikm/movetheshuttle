//import net.jscience.math.*;
import java.util.*;

/**
 * Represents an abstract bullet.
 */
public abstract class Bullet extends MovingObject {
    
    /** 
     *  Constructor. 
     *
     *  @param  positionX           x-coordinate of the position
     *  @param  positionY           y-coordinate of the position
     *  @param  velocityX           x-coordinate of the velocity vector
     *  @param  velocityY           y-coordinate of the velocity vector
     *  @param  collisionRadius     serves to detect collisions with other moving objects
     *  @param  rand                reference to a random number generator shared by all classes of the program
     */
    Bullet(float positionX, float positionY, 
            float velocityX, float velocityY, 
            float collisionRadius, Random rand) {
        // create MovingObject of size=4
        super(positionX, positionY, velocityX, velocityY, collisionRadius, rand);
    }

    /** Moves the bullet. */
    public void move(float deltaTime) {
        super.move(deltaTime);
        if (isOutOfGameCanvas()) die();
    }
}
