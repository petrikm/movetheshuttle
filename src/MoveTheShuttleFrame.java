import java.awt.*;
import java.awt.event.*;

/** 
 *  Represents the main window of the game and it is also the Main-Class of the program.
 */
public class MoveTheShuttleFrame extends Frame {
    GamePanel gamePanel; // the main screen of the game

    MoveTheShuttleFrame() {
        gamePanel = new GamePanel();
        //this.setSize(720,750);
        this.setSize(Constants.GAME_CANVAS_WIDTH + 20, Constants.GAME_CANVAS_HEIGHT + 50);
        this.add(gamePanel);
        this.addWindowListener(new FrameWA());
        this.setVisible(true);
        this.setResizable(false);
        gamePanel.createBuffer();
    }

    /** The main loop of the program. */
    public void run() {
        gamePanel.run();
    }

    /** Runs the program. */
    public static void main(String[] args) {
        MoveTheShuttleFrame frame = new MoveTheShuttleFrame();
        frame.run();
    }

    class FrameWA extends WindowAdapter {
        public void windowClosing(WindowEvent e) {
            System.exit(1);
        }
    }
}
