import java.awt.*;
import java.util.*;

// needed for unit test
import java.awt.event.*;
import java.awt.image.*;

/** 
 *  Represents a bonus item which gives the player an additional shield.
 *  Each time the shuttle is hit, it loses one shield.
 *  When the shuttle is hit and has no shield, it dies.
 */
public class BonusShield extends Bonus {
	
    /** Image of the bonus item. */
	private static VectorImage model = new VectorImage(Constants.DIRECTORY_WITH_MODELS + "bonus_shield.fig");

    /** Creates a new shield bonus item with given position and velocity and
     *      with random rotation speed.
     *
     *  @param  positionX           x-coordinate of the position
     *  @param  positionY           y-coordinate of the position
     *  @param  velocityX           x-coordinate of the velocity vector
     *  @param  velocityY           y-coordinate of the velocity vector
     *  @param  rand                reference to a random number generator shared by all classes of the program
     */
    BonusShield(float positionX, float positionY,
            float velocityX, float velocityY,
            Random rand) {
        super(positionX, positionY, velocityX, velocityY, rand);
        this.img = model;
	}

    /** Creates a new shield bonus item with random position and velocity outside the board.
     *  @param rand reference to a random number generator shared by all classes of the program
     */
    BonusShield(Random rand) {
        super(rand);
        this.img = model;
    }

    /** Manages collision between the bonus item and the shuttle.
     *  If there is such a collision, the bonus item is destroyed and the
     *      player is given an additional shield. */
	public void handleCollisionWithShuttle(Shuttle shuttle) {
        super.handleCollisionWithShuttle(shuttle);
		if(isCollision(shuttle)) {
            shuttle.addShield();
		}
	}

    /** Performs unit testing. */
    public static void main(String[] args) {
        UnitTestBonusShield unitTest = new UnitTestBonusShield();
        unitTest.createBuffer();
        unitTest.run();
    }
}

// ====================
//     UNIT TESTING
// ====================

/**
 *  Implemets a unit test.
 */
class UnitTestBonusShield {
    /** Geometry of the window */
    private Frame testFrame;
    private int testFrameWidth =  Constants.GAME_CANVAS_WIDTH + 20; 
    private int testFrameHeight = Constants.GAME_CANVAS_HEIGHT + 50;

    /** Geometry of the canvas */
    private TestCanvas testCanvas;
    private int testCanvasWidth =  Constants.GAME_CANVAS_WIDTH;
    private int testCanvasHeight = Constants.GAME_CANVAS_HEIGHT;

    /** instance of random number generator which all the classes share */
    private Random rand;    

    private BonusShield movingObject;
    private BonusShield staticObject;
    private ListOfMovingObjects listOfObjects;

    /** Initializes a simple frame with a canvas and creates an instance of {@code Background}. */
    UnitTestBonusShield() {
        rand = new Random();

        testFrame = new Frame();
        testFrame.setSize(testFrameWidth, testFrameHeight);
        testFrame.setLocation(100, 100);
        testFrame.addWindowListener(new FrameWA());
        testFrame.setVisible(true);
        testFrame.setResizable(true);

        Panel panel = new Panel();
        panel.setLayout(new FlowLayout());
        panel.setBackground(new Color(50, 0, 50));
        testFrame.add(panel);

        testCanvas = new TestCanvas();
        testCanvas.setSize(testCanvasWidth, testCanvasHeight);
        panel.add(testCanvas);

        movingObject = new BonusShield(0.0f, 0.0f, 
                0.05f * testCanvasWidth, 0.05f * testCanvasHeight, 
                rand);
        staticObject = new BonusShield(testCanvasWidth / 2.0f, testCanvasHeight / 2.0f,
                0.0f, 0.0f, 
                rand);
        listOfObjects = new ListOfMovingObjects();
    }

    /** The main loop. */
    public void run() {
        float deltaTime = 0.0f;
        long nanoDeltaTime = 0;
        long previousTime = 0;
        long currentTime = System.nanoTime();

        float timeToNextObject = 1.0f;

        while(testCanvas.isVisible()) {
            previousTime = currentTime;
            currentTime = System.nanoTime();
            nanoDeltaTime = currentTime - previousTime;

            // keep frames per second rate at maximum of 50
            if (nanoDeltaTime < 20000000L) {
                try {
                    Thread.sleep((20000000L - nanoDeltaTime) / 1000000L);
                } catch(InterruptedException e) {}
                currentTime = System.nanoTime();
                nanoDeltaTime = currentTime - previousTime;
            }

            deltaTime = (float) ((double) nanoDeltaTime / 1000000000.0);

            timeToNextObject -= deltaTime;
            if (timeToNextObject <= 0.0f) {
                listOfObjects.add(new BonusShield(rand));
                timeToNextObject = 1.0f;
            }

            movingObject.move(deltaTime);
            staticObject.move(deltaTime);
            listOfObjects.move(deltaTime);

            testCanvas.draw();
        }
    }

    /** Creates a buffer for the double-buffered graphics. */
    public void createBuffer() {
        testCanvas.createBufferStrategy(2);
        testCanvas.strategy = testCanvas.getBufferStrategy();
    }

    /** Allows the frame to be closed. */
    private class FrameWA extends WindowAdapter {
        public void windowClosing(WindowEvent e) {
            System.exit(1);
        }
    }

    /** Displays the graphics. */
    class TestCanvas extends Canvas {
        public BufferStrategy strategy;
        public void draw() {
            Graphics g = strategy.getDrawGraphics();

            g.setColor(new Color(0, 0, 50));
            g.fillRect(0, 0, testCanvasWidth, testCanvasHeight);


            g.setColor(new Color(50, 50, 50));
            int xPos = 20;
            int yPos = 100;
            g.drawString("Number of objects: " + listOfObjects.getNumber() + " (" + listOfObjects.computeNumber() + ")", xPos, yPos += 20);

            movingObject.draw(g);
            staticObject.draw(g);
            staticObject.drawCollisionCircle(Color.yellow, g);
            listOfObjects.draw(g);

            g.dispose();
            strategy.show();
        }
    }
}
