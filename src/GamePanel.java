import java.util.*;
import java.io.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;

/**
 * This class implements the main panel of the game.
 *
 * It is supposed to be added to a window or to an applet panel.
 */
public class GamePanel extends Panel {

    /** The canvas where all the game graphics are displayed */
    private int gameCanvasWidth;
    private int gameCanvasHeight;
    private GameCanvas gameCanvas;

    /** Animated star sky in the background of the game */
    private Background background;

    /** Instance of random number generator which all the classes share */
    private Random rand;

    /** Just to display the rate */
    private float framesPerSecond = 0.0f;

    /** The game does not start until the mouse is clicked. */
    private boolean waitingForTheFirstClick;

    private Shuttle shuttle;
    private ListOfAliens listOfAliens;
	private ListOfMovingObjects listOfShuttleBullets;
	private ListOfMovingObjects listOfAlienBullets;
	private ListOfMovingObjects listOfParticles;
	private ListOfMovingObjects listOfBonuses;

    /** If {@code true} then the aliens are (continually) created. */
    private boolean spawning;

    /** If {@code true} then the player cannot die. */
    private boolean invulnerable;

    /** How many aliens have been killed */
    private int score;

    /** Indicates whether additional info is displayed on the game canvas. */
    boolean showDebugInfo;

    /** Indicates whether tha game is paused. */
    boolean paused;

    GamePanel() {
        rand = new Random();

        setLayout(new BorderLayout());

        gameCanvasWidth =  Constants.GAME_CANVAS_WIDTH;
        gameCanvasHeight = Constants.GAME_CANVAS_HEIGHT;

        gameCanvas = new GameCanvas();
        gameCanvas.setSize(gameCanvasWidth,gameCanvasHeight);
        gameCanvas.addMouseMotionListener(new GamePanelMouseMotionAdapter());
        gameCanvas.addMouseListener(new GamePanelMouseAdapter());
        gameCanvas.addMouseWheelListener(new GamePanelMouseAdapter());
        gameCanvas.addKeyListener(new GamePanelKeyAdapter());
        gameCanvas.setFocusable(true);

        Panel gameCanvasPanel = new Panel();
        gameCanvasPanel.setBackground(new Color(0, 0, 50));
        gameCanvasPanel.setLayout(new FlowLayout());
        gameCanvasPanel.add(gameCanvas);

        add(BorderLayout.CENTER, gameCanvasPanel);

        background = new Background(gameCanvasWidth, gameCanvasHeight, rand);

        listOfShuttleBullets = new ListOfMovingObjects();
        listOfAlienBullets = new ListOfMovingObjects();
        listOfParticles = new ListOfMovingObjects();
        listOfParticles.setAsDeltaTimeKeeper(0.05f);
        listOfBonuses = new ListOfMovingObjects();
        listOfAliens = new ListOfAliens(listOfParticles, listOfAlienBullets, listOfShuttleBullets, rand);
        shuttle = new Shuttle(listOfShuttleBullets, listOfParticles, rand);

        waitingForTheFirstClick = true;
        spawning = false;
        invulnerable = false;
        showDebugInfo = false;
        paused = false;

        reset();
    }

    /** Resets the game, i.e., starts a new game from scratch.
     *
     *  The shuttle is placed to its starting position, all aliens, bullets,
     *      and bonuses are deleted. */
    public void reset() {
        shuttle.clear();
        shuttle.setAttracted(false);
        //shuttle.addShotgunAmmo(10000);
        //shuttle.addLaserEnergy(100.0f);
        listOfShuttleBullets.clear();
        listOfAlienBullets.clear();
        listOfParticles.clear();
        listOfBonuses.clear();
        listOfAliens.clear();
        waitingForTheFirstClick = true;
        score = 0;
    }

    /** This is the main loop of the program. */
    public void run() {
        // time difference between two consquent frames
        float deltaTime = 0.0f;
        long nanoDeltaTime = 0;
        long previousTime = 0;
        long currentTime = System.nanoTime();

        long framesPerSecondRefreshTime = 200000000L;

        float timeToNextBonus = 5.0f + 5.0f * rand.nextFloat();

        //score = 256;
        //shuttle.addShotgunAmmo(37);
        //shuttle.addLaserEnergy(5.4f);

        while(gameCanvas.isVisible()) {
            // computation of delta time
            // in order to know how far all the game objects shall move
            previousTime = currentTime;
            currentTime = System.nanoTime();
            nanoDeltaTime = currentTime - previousTime;

            // keep the "frames per second" rate under 50
            if (nanoDeltaTime < 20000000L) {
                try {
                    Thread.sleep((20000000L - nanoDeltaTime) / 1000000L);
                } catch(InterruptedException e) {}
                currentTime = System.nanoTime();
                nanoDeltaTime = currentTime - previousTime;
            }
            // this is the value which all the moving objects need to know
            deltaTime = (float) nanoDeltaTime / 1000000000.0f;

            // this is to display the "frames per second" rate
            framesPerSecondRefreshTime -= nanoDeltaTime;
            if (framesPerSecondRefreshTime <= 0L) {
                framesPerSecond = 1 / deltaTime;
                framesPerSecondRefreshTime = 200000000L;
            }

            if (! paused) {
                // spawn new aliens
                if (spawning) {
                    listOfAliens.spawn(deltaTime, score);
                }

                // create new bonus items
                if (! waitingForTheFirstClick) {
                    timeToNextBonus -= deltaTime;
                    if (timeToNextBonus <= 0.0f) {
                        float ticket = rand.nextFloat();
                        if (ticket < 0.5f) {
                            listOfBonuses.add(new BonusShotgun(rand));
                        } else if (ticket < 0.8f) {
                            listOfBonuses.add(new BonusLaser(rand));
                        } else {
                            listOfBonuses.add(new BonusShield(rand));
                        }
                        float minTime = Utils.getAsymptoticInterpolation(score, 0.0f, 2.0f * ListOfAliens.MILESTONE_CLONER, 15.0f, 5.0f);
                        float maxTime = Utils.getAsymptoticInterpolation(score, 0.0f, 2.0f * ListOfAliens.MILESTONE_CLONER, 30.0f, 10.0f);
                        timeToNextBonus = minTime + (maxTime - minTime) * rand.nextFloat();
                    }
                }

                // move all the game objects respecting the delta time
                background.move(deltaTime);
                listOfShuttleBullets.move(deltaTime);
                listOfAlienBullets.move(deltaTime);
                listOfParticles.move(deltaTime);
                listOfBonuses.move(deltaTime);
                listOfAliens.move(deltaTime);
                if (! shuttle.isDying()) {
                    shuttle.move(deltaTime);
                }

                // manage the collisions between the shuttle and the aliens and their bullets
                if (! invulnerable && ! shuttle.isOutOfGameCanvas()) {
                    shuttle.handleCollisions(listOfAliens, listOfAlienBullets);
                    if (shuttle.isDying()) {
                        listOfAliens.stopFollowingTheShuttle();
                        spawning = false;
                    }
                }

                // manage the aliens that are hit by a shuttle bullet
                score += listOfAliens.handleCollisions(listOfShuttleBullets);

                for (Bonus bonus = (Bonus) listOfBonuses.getFirst(); bonus != null; bonus = (Bonus) bonus.getNext()) {
                    bonus.handleCollisionWithShuttle(shuttle);
                }

                // all the aliens follow the shuttle
                listOfAliens.setAttractorPosition(shuttle.getX(), shuttle.getY());
            }

            // draw everything
            gameCanvas.draw();
        }
    }

    /** Creates a buffer for the double-buffered graphics. */
    public void createBuffer() {
        gameCanvas.createBufferStrategy(2);
        gameCanvas.strategy = gameCanvas.getBufferStrategy();
    }

    /**
     * Implements the canvas to display the graphics of the game.
     */
    private class GameCanvas extends Canvas {
        public BufferStrategy strategy;

        public void draw() {
            Graphics g = strategy.getDrawGraphics();

            background.draw(g);

            Font font = new Font("Serif", Font.PLAIN, 16);
            g.setFont(font);
            g.setColor(new Color(200, 200, 50));

            g.drawString("Kills: " + score, 40, 40);
            g.drawString("Shields: " + shuttle.getShields(), Constants.GAME_CANVAS_WIDTH - 120, 40);

            {
                int xPos = 40;
                int yPos = Constants.GAME_CANVAS_HEIGHT - 100;
                switch (shuttle.getCurrentWeapon()) {
                    case Shuttle.GUN:
                        g.drawString("Current weapon: GUN", xPos, yPos += 20);
                        break;
                    case Shuttle.SHOTGUN:
                        g.drawString("Current weapon: SHOTGUN", xPos, yPos += 20);
                        break;
                    case Shuttle.LASER:
                        g.drawString("Current weapon: LASER", xPos, yPos += 20);
                        break;
                    default:
                        g.drawString("Current weapon: " + shuttle.getCurrentWeapon(), xPos, yPos += 20);
                }
                g.drawString("Shotgun ammo: " + shuttle.getShotgunAmmo(), xPos, yPos += 20);
                g.drawString("Laser energy:   " + String.format("%.2f", shuttle.getLaserEnergy()), xPos, yPos += 20);
            }

            if (showDebugInfo) {
                g.setColor(new Color(100, 100, 100));
                int xPos = 20;
                int yPos = 100;
                g.drawString("FPS: " + Math.round(framesPerSecond), xPos, yPos += 20);
                g.drawString("Spawning: " + spawning, xPos, yPos += 20);
                g.drawString("Invulnerable: " + invulnerable, xPos, yPos += 20);
                g.drawString("Number of aliens: " + listOfAliens.getNumber() + " (" + listOfAliens.computeNumber() + ")", xPos, yPos += 20);
                g.drawString("Number of particles: " + listOfParticles.getNumber() + " (" + listOfParticles.computeNumber() + ")", xPos, yPos += 20);
                g.drawString("Number of bonuses: " + listOfBonuses.getNumber() + " (" + listOfBonuses.computeNumber() + ")", xPos, yPos += 20);
                g.drawString("Number of shuttle bullets: " + listOfShuttleBullets.getNumber() + " (" + listOfShuttleBullets.computeNumber() + ")", xPos, yPos += 20);
                g.drawString("Number of alien bullets: " + listOfAlienBullets.getNumber() + " (" + listOfAlienBullets.computeNumber() + ")", xPos, yPos += 20);
                g.drawString("Press A to add some ammunition", xPos, yPos += 20);
                g.drawString("Press I to toggle invulnerability", xPos, yPos += 20);
                g.drawString("Press S to toggle alien spawning", xPos, yPos += 20);
                g.drawString("Press X for shuttle's explosion", xPos, yPos += 20);
                g.drawString("Press Z to explode an alien", xPos, yPos += 20);
            }

            if (waitingForTheFirstClick) {
                g.setColor(new Color(200, 200, 50));
                int xPos = Constants.GAME_CANVAS_WIDTH / 2;
                int yPos = Constants.GAME_CANVAS_HEIGHT / 10;
                VectorImage.drawCenteredString(xPos, yPos += 20,
                        "The shuttle follows the mouse cursor.",
                        new Font("Arial", font.PLAIN, 16), g);
                VectorImage.drawCenteredString(xPos, yPos += 20,
                        "Click to shoot.",
                        new Font("Arial", font.PLAIN, 16), g);
                VectorImage.drawCenteredString(xPos, yPos += 20,
                        "Use mouse wheel to change weapons.",
                        new Font("Arial", font.PLAIN, 16), g);
                //VectorImage.drawCenteredString(xPos, yPos += 40,
                //        "Press 'R' to restart the game.",
                //        new Font("Arial", font.PLAIN, 16), g);
                VectorImage.drawCenteredString(xPos, yPos += 40,
                        "Press 'P' to pause the game.",
                        new Font("Arial", font.PLAIN, 16), g);
                //VectorImage.drawCenteredString(xPos, yPos += 20,
                //        "Press 'D' to show debug info.",
                //        new Font("Arial", font.PLAIN, 16), g);
                VectorImage.drawCenteredString(xPos, yPos += 20,
                        "Press 'ESC' to exit the program.",
                        new Font("Arial", font.PLAIN, 16), g);
                VectorImage.drawCenteredString(xPos, yPos += 40,
                        "CLICK TO START", new Font("Arial", font.ITALIC, 20), g);
            }

            {
                int xPos = Constants.GAME_CANVAS_WIDTH / 2;
                int yPos = 20;
                if (invulnerable) {
                    g.setColor(new Color(255, 255, 50));
                    VectorImage.drawCenteredString(xPos, yPos += 20,
                            "INVULNERABLE",
                            new Font("Arial", font.PLAIN, 16), g);
                }
                if (paused) {
                    g.setColor(new Color(255, 255, 50));
                    VectorImage.drawCenteredString(xPos, yPos += 20,
                            "PAUSED",
                            new Font("Arial", font.PLAIN, 16), g);
                }
                //if (shuttle.isDying()) {
                //    g.setColor(new Color(255, 255, 50));
                //    VectorImage.drawCenteredString(xPos, yPos += 20,
                //            "Press 'R' to restart the game.",
                //            new Font("Arial", font.PLAIN, 16), g);
                //}
            }

            {
                int xPos = Constants.GAME_CANVAS_WIDTH / 2;
                int yPos = Constants.GAME_CANVAS_HEIGHT / 2;
                if (shuttle.isDying()) {
                    g.setColor(new Color(255, 255, 50));
                    VectorImage.drawCenteredString(xPos, yPos,
                            "You have executed " + score + " of them!", 
                            new Font("Arial", font.PLAIN, 24), g);
                    VectorImage.drawCenteredString(xPos, yPos += 40,
                            "Press 'R' to restart the game.",
                            new Font("Arial", font.PLAIN, 16), g);
                }
            }

            listOfShuttleBullets.draw(g);
            listOfAlienBullets.draw(g);
            listOfParticles.draw(g);
            listOfBonuses.draw(g);
            listOfAliens.draw(g);
            shuttle.draw(g);

            g.dispose();
            strategy.show();
        }
    }

    /**
     *  Handles the movement of the mouse cursor.
     */
    private class GamePanelMouseMotionAdapter extends MouseMotionAdapter {
        public void mouseMoved(MouseEvent e) {
            shuttle.setAttractorPosition((float) e.getX(), (float) e.getY());
        }
        public void mouseDragged(MouseEvent e) {
            shuttle.setAttractorPosition((float) e.getX(), (float) e.getY());
        }
    }

    /**
     *  Handles the mouse clicking.
     */
    private class GamePanelMouseAdapter extends MouseAdapter {
        public void mousePressed(MouseEvent e) {
            if(waitingForTheFirstClick) {
                waitingForTheFirstClick = false;
                spawning = true;
                shuttle.setAttracted(true);
            } else {
                shuttle.startShooting();
            }
        }
        public void mouseReleased(MouseEvent e) {
            if(! waitingForTheFirstClick) {
                shuttle.stopShooting();
            }
        }
        public void mouseWheelMoved(MouseWheelEvent e) {
            int notches = e.getWheelRotation();
            if (notches < 0) {
                //System.out.println("Weapon switched to previous.");
                shuttle.setPreviousWeapon();
            } else {
                //System.out.println("Weapon switched to next.");
                shuttle.setNextWeapon();
            }
            /*
            switch (shuttle.getCurrentWeapon()) {
                case Shuttle.GUN:
                    System.out.println("Weapon switched to GUN");
                    break;
                case Shuttle.SHOTGUN:
                    System.out.println("Weapon switched to SHOTGUN");
                    break;
                case Shuttle.LASER:
                    System.out.println("Weapon switched to LASER");
                    break;
                default:
                    System.out.println("ERROR: Unknown weapon type " + shuttle.getCurrentWeapon());
            }
            */
        }
    }

    /**
     *  Handles the keyboard.
     */
    private class GamePanelKeyAdapter extends KeyAdapter {
        /** Indicates whether the given key is being pressed.
         *  To avoid repeated events when a key is held.
         *  Maybe can be managed in a better way? */
        private boolean keyPressed[];

        GamePanelKeyAdapter() {
            super();
            keyPressed = new boolean[KeyEvent.KEY_LAST + 1];
            for (int i = 0; i < KeyEvent.KEY_LAST + 1; ++i) {
                keyPressed[i] = false;
            }
        }

        /** Invoked when a key has been pressed. */
        public void keyPressed(KeyEvent e) {
            int keyCode = e.getKeyCode();
            if (! keyPressed[keyCode]) {
                keyPressed[keyCode] = true;
                switch (keyCode) {
                    case KeyEvent.VK_ESCAPE:
                        System.exit(1);
                        break;
                    case KeyEvent.VK_R:
                        if (shuttle.isDying()) {
                            reset();
                        }
                        break;
                    case KeyEvent.VK_P:
                        if (paused) {
                            paused = false;
                        } else {
                            paused = true;
                        }
                        break;
                    // debug keys
                    //case KeyEvent.VK_D:
                    //    if (showDebugInfo) {
                    //        showDebugInfo = false;
                    //    } else {
                    //        showDebugInfo = true;
                    //    }
                    //    break;
                    //case KeyEvent.VK_Z:
                    //    listOfAliens.getFirst().die();
                    //    break;
                    //case KeyEvent.VK_A:
                    //    shuttle.addShotgunAmmo(100);
                    //    shuttle.addLaserEnergy(10);
                    //    break;
                    //case KeyEvent.VK_X:
                    //    shuttle.explode();
                    //    break;
                    //case KeyEvent.VK_I:
                    //    if (invulnerable) {
                    //        invulnerable = false;
                    //    } else {
                    //        invulnerable = true;
                    //    }
                    //    break;
                }
            }
        }

        /** Invoked when a key has been released. */
        public void keyReleased(KeyEvent e) {
            int keyCode = e.getKeyCode();
            if (keyPressed[keyCode]) {
                keyPressed[keyCode] = false;
            }
        }

        /** Invoked when a key has been typed. */
        public void keyTyped(KeyEvent e) {
        }
    }
}
