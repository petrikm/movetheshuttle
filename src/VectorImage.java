import java.awt.*;
import java.io.*;

// needed for unit test
import java.util.*;
import java.awt.event.*;
import java.awt.image.*;

/**
 * Represents an image defined by line segments and circles.
 */
public class VectorImage {

    /** Coordinates of points describing the vector image.
     *  These values are fixed for th whole running time of the program. */
    protected float[] x, y;

    /** Coordinates of points after geometrical transformations.
     *  These values are computed from the attributes {@code x[]} and 
     *      {@code y[]} by a preforming geometrical transformations (rotations,
     *      translations, scaling, ...) and by converting the {@code float} values
     *      to {@code int} by {@code Math.round()}. 
     *  @see rotateTranslateAndScale
     *  @see rotateAndTranslate */
    protected int[] drawX, drawY;

    /** Number of the points that define the image.
     *  That is, size of the arrays of coordinates.
     *  Namely: {@code x[]}, {@code y[]}, {@code drawX[]}, and {@code drawY[]} */
    protected int numPoints;

    /** Array of geometrical elements (such as polygons, circles, ...). 
     *  The geometrical elements do not contain their coordinates; they contain
     *      indices to the array of points in (which are 
     *      {@code x[]}, {@code y[]}, {@code drawX[]}, and {@code drawY[]}).
     *  This allows to rotate, scale or place the whole vector image as a
     *      single object. */
    protected Element elements; 

    /** To scale properly the diameters of the circles. */
    protected float diameterScale;

    /** List of (x,y)-coordinates. */
    //TODO rewrite to: Vector<DataType> vector_name = new Vector<>();
    protected AtomCoordinate coordinates;

    /** Scale of the whole image. */
    //private static final int figRatio = 100;
    private static final float figRatio = 100;

    /** Color codes used by XFig. */
    public static int[][] colorList = {
        {0x00, 0x00, 0x00}, //  0 black
        {0x00, 0x00, 0xFF}, //  1 blue    
        {0x00, 0xFF, 0x00}, //  2 green    
        {0x00, 0xFF, 0xFF}, //  3 cyan    
        {0xFF, 0x00, 0x00}, //  4 red    
        {0xFF, 0x00, 0xFF}, //  5 magenta    
        {0xFF, 0xFF, 0x00}, //  6 yellow    
        {0xFF, 0xFF, 0xFF}, //  7 white    
        {0x00, 0x00, 0x90}, //  8 
        {0x00, 0x00, 0xB0}, //  9 
        {0x00, 0x00, 0xD0}, //  1 
        {0x87, 0xCE, 0xFF}, // 11 
        {0x00, 0x90, 0x00}, // 12 
        {0x00, 0xB0, 0x00}, // 13 
        {0x00, 0xD0, 0x00}, // 14 
        {0x00, 0x90, 0x90}, // 15 
        {0x00, 0xB0, 0xB0}, // 16 
        {0x00, 0xD0, 0xD0}, // 17 
        {0x90, 0x00, 0x00}, // 18 
        {0xB0, 0x00, 0x00}, // 19 
        {0xD0, 0x00, 0x00}, // 20 
        {0x90, 0x00, 0x90}, // 21 
        {0xB0, 0x00, 0xB0}, // 22 
        {0xD0, 0x00, 0xD0}, // 23 
        {0x80, 0x30, 0x00}, // 24 
        {0xA0, 0x40, 0x00}, // 25 
        {0xC0, 0x60, 0x00}, // 26 
        {0xFF, 0x80, 0x80}, // 27 
        {0xFF, 0xA0, 0xA0}, // 28 
        {0xFF, 0xC0, 0xC0}, // 29 
        {0xFF, 0xE0, 0xE0}, // 30 
        {0xFF, 0xD7, 0x00}  // 31 
    };

    /**
     *  Creates a new vector image.
     *  The created vector image is empty. 
     *  The method {@code load} has to be executed before doing anything with
     *      the vector image.
     */
    VectorImage() {
        clear();
    }

    /** Creates a new vector image from the given '.fig' file. */
    VectorImage(String path) {
        try {
            loadFromFIG(path);
        } catch (FileNotFoundException e) {
            System.out.println("ERROR: COULD NOT FIND FILE: " + path);
        } catch (IOException e) {
            System.out.println("ERROR: CANNOT OPEN FILE: " + path);
        } catch (TextDataFile.ParseError e) {
            System.out.println(e.getMessage());
        } catch (VectorImage.UnknownGraphicObjectTypeError e) {
            System.out.println(e.getMessage());
        }
    }

    /** Erases the whole image. */
    private void clear() {
        numPoints = 0;
        x = null;
        y = null;
        drawX = null;
        drawY = null;
        diameterScale = 1.0f;
        elements = null;
        coordinates = null;
    }

//    /** 
//     *  Loads a vector image from a '.fig' file.
//     *
//     *  When the file is being loaded the coordinates are first stored to the
//     *      dynamic list {@code coordinates}.
//     *  Once the image is loaded, the coordinates are copied to the arrays
//     *      {@code x}, {@code y}, {@code drawX}, and {@code drawY}.
//     *
//     *  FIG is a file type used by the application XFig (see <a href="https://xfig.org">xfig.org</a>).
//     */
//    void load(String filename) {
//        coordinates = null;
//
//        FileFIG file = new FileFIG(filename);
//        try {
//            file.open();
//            file.parse(this);
//            file.close();
//            System.out.println("Loaded file " + filename + ".");
//        }
//        catch(IOException e) {
//            System.out.println("ERROR in VectorImage.load(): Cannot open file " + filename + ".");
//        }
//
//        copyCoordinates();
//        coordinates = null;
//    }

    /** Reads an ellipse from '.fig' file and creates the corresponding vector image element.
     *  According to the read data, creates an instance of {@code Circle} or
     *      {@code Oval} and stores it to the dynamic list {@code elements}. 
     *
     *  TODO
     *  Only circle reading and creation is implemented, so far.
     *  Hence, no creation of ovals; this is still to be done. */
    public void readEllipseFromFIG(TextDataFile inputFile) 
            throws  FileNotFoundException,
                    IOException,
                    TextDataFile.ParseError,
                    TextDataFile.EndOfFile,
                    UnknownGraphicObjectTypeError {

        // these values are shared with other graphic objects 
        // (see method readPolylineFromFIG)
		int     subType     = inputFile.parseNextInteger();
		int     lineStyle   = inputFile.parseNextInteger();
		int     thickness   = inputFile.parseNextInteger();       // 1/80 inch
		int     drawColor    = inputFile.parseNextInteger();
		int     fillColor   = inputFile.parseNextInteger();
		int     depth       = inputFile.parseNextInteger();
		int     penStyle    = inputFile.parseNextInteger();
		int     areaFill    = inputFile.parseNextInteger();       // -1 = no fill
		float   styleVal    = inputFile.parseNextFloat();

        // these values are specific for an ellipse
        int     direction   = inputFile.parseNextInteger();   // always 1
        float   angle       = inputFile.parseNextFloat();     // radians, the angle of the x-axis
        int     centerX     = inputFile.parseNextInteger();
        int     centerY     = inputFile.parseNextInteger();
        int     radiusX     = inputFile.parseNextInteger();
        int     radiusY     = inputFile.parseNextInteger();
        int     startX      = inputFile.parseNextInteger();   // the first point entered
        int     startY      = inputFile.parseNextInteger();
        int     endX        = inputFile.parseNextInteger();   // the last point entered
        int     endY        = inputFile.parseNextInteger();

        switch(subType) {
            case 1: // ELLIPSE DEFINED BY RADII
                // TODO, NOT IMPLEMENTED
                break;
            case 3: // CIRCLE DEFINED BY RADIUS
                int drx = radiusX - centerX;
                int dry = radiusY - centerY;
                int diameter = (int) Math.round(2.0 * Math.sqrt((drx * drx) + (dry * dry)));
                addCircle(centerX, centerY, diameter, depth, getColor(drawColor), getColor(fillColor));
                break;
            default:
                throw new UnknownGraphicObjectTypeError(inputFile.getLineNumber(), "UNKNOWN ELLIPSE SUB-TYPE: " + subType);
        }
	}

    /** Reads a polyline from '.fig' file and creates the corresponding vector image element.
     *  According to the read data, creates an instance of {@code Polyline} or
     *      {@code Polygon} and stores it to the dynamic list {@code elements}. 
     *  Note that a polygons differs from a polyline in being closed, that is,
     *      its first vertex matches its last vertex.
     *  Hence the last vertex is not even stored. */
	public void readPolylineFromFIG(TextDataFile inputFile) 
            throws  FileNotFoundException,
                    IOException,
                    TextDataFile.ParseError,
                    TextDataFile.EndOfFile,
                    UnknownGraphicObjectTypeError {

        // these values are shared with other graphic objects 
        // (see method readEllipseFromFIG)
		int     subType     = inputFile.parseNextInteger();
		int     lineStyle   = inputFile.parseNextInteger();
		int     thickness   = inputFile.parseNextInteger();       // 1/80 inch
		int     drawColor    = inputFile.parseNextInteger();
		int     fillColor   = inputFile.parseNextInteger();
		int     depth       = inputFile.parseNextInteger();
		int     penStyle    = inputFile.parseNextInteger();
		int     areaFill    = inputFile.parseNextInteger();       // -1 = no fill
		float   styleVal    = inputFile.parseNextFloat();

        // these values are specific for a polyline
        int joinStyle      = inputFile.parseNextInteger();
        int capStyle       = inputFile.parseNextInteger();
        int radius         = inputFile.parseNextInteger();
        int forwardArrow   = inputFile.parseNextInteger();
        int backwardArrow  = inputFile.parseNextInteger();
        int nPoints        = inputFile.parseNextInteger();
        int x;
        int y;

        switch(subType) {
            case 1: // POLYLINE
                Polyline polyline = addPolyline(nPoints, depth, getColor(drawColor));
                for (int i = 0; i < nPoints; ++i) {
                    polyline.setVertex(i, inputFile.parseNextInteger(), inputFile.parseNextInteger());
                }
                break;
            case 2: // BOX	
                Polygon box = addPolygon(nPoints - 1, depth, getColor(drawColor), getColor(fillColor));
                for (int i = 0; i < (nPoints - 1); ++i) {
                    box.setVertex(i, inputFile.parseNextInteger(), inputFile.parseNextInteger());
                }
                // skip the next two values; these are the coordinates of the
                // last point which are actually equal to the coordinates of
                // the first point
                inputFile.parseNextInteger();
                inputFile.parseNextInteger();
                break;
            case 3: // POLYGON	
                Polygon polygon = addPolygon(nPoints - 1, depth, getColor(drawColor), getColor(fillColor));
                for (int i = 0; i < (nPoints - 1); ++i) {
                    polygon.setVertex(i, inputFile.parseNextInteger(), inputFile.parseNextInteger());
                }
                // skip the next two values; these are the coordinates of the
                // last point which are actually equal to the coordinates of
                // the first point
                inputFile.parseNextInteger();
                inputFile.parseNextInteger();
                break;
            default:
                throw new UnknownGraphicObjectTypeError(inputFile.getLineNumber(), "UNKNOWN POLYLINE SUB-TYPE: " + subType);
        }
	}

    /** Reads a graphic object from '.fig' file and creates the corresponding vector image element.
     *  According to the read data, creates an instance of {@code Element} and
     *      stores it to the dynamic list {@code elements}. */
	public void readObjectFromFIG(TextDataFile inputFile) 
            throws  FileNotFoundException,
                    IOException,
                    TextDataFile.ParseError,
                    TextDataFile.EndOfFile,
                    UnknownGraphicObjectTypeError {

		int objectType = inputFile.parseNextInteger();
		switch(objectType) {
			case 1: // ELLIPSE
                readEllipseFromFIG(inputFile);
				break;
			case 2: // POLYLINE	
                readPolylineFromFIG(inputFile);
				break;
			default:
                throw new UnknownGraphicObjectTypeError(inputFile.getLineNumber(), "UNKNOWN GRAPHIC OBJECT TYPE: " + objectType);
		}
	}

    /** Loads a vector image from a '.fig' file.
     *
     *  When the file is being loaded the coordinates are first stored to the
     *      dynamic list {@code coordinates}.
     *  Once the image is loaded, the coordinates are copied to the arrays
     *      {@code x}, {@code y}, {@code drawX}, and {@code drawY}.
     *
     *  FIG is a file type used by the application XFig 
     *      (see <a href="https://xfig.org">xfig.org</a>). 
     *
     *  IMPORTANT NOTE: 
     *  This is a VERY SIMPLE code that reads only such '.fig' files that
     *      consist of ellipses and polylines ONLY (defining polygons as those
     *      polylines that are closed and possibly filled by a color). 
     *  Hence, this method CANNOT read ANY '.fig' file.
     *  This method has been written in order to load such '.fig' files that
     *      represent vector models in this game and since all these models consist
     *      of polygons, polylines, and ellipses only, the code is sufficient for
     *      its purpose. */
    public void loadFromFIG(String path)
            throws FileNotFoundException, IOException, TextDataFile.ParseError, UnknownGraphicObjectTypeError {
        clear();

        TextDataFile inputFile = new TextDataFile(path);

        // skip first 9 lines
        // (here you can see that this code is really not universal)
        while (inputFile.getParsedType() != TextDataFile.ParsedType.EOF && inputFile.getLineNumber() < 9) {
            inputFile.parseNextValue();
        }

        // read the file
        // if there are graphic objects other then polylines or ellipses, the
        // code will throw an exception
        while (inputFile.getParsedType() != TextDataFile.ParsedType.EOF) {
            try {
                readObjectFromFIG(inputFile);
            } catch (TextDataFile.EndOfFile e) {
                break;
            }
        }

        copyCoordinates();
    }

    /** The coordinates in the dynamic list {@code coordinates} are copied to the arrays
     *      {@code x}, {@code y}, {@code drawX}, and {@code drawY}. */
    public void copyCoordinates() {
        numPoints = 0;
        AtomCoordinate iter = coordinates;

        while(iter != null) {
            ++numPoints;
            iter = iter.getNext();
        }

        x = new float[numPoints];
        y = new float[numPoints];
        drawX = new int[numPoints];
        drawY = new int[numPoints];

        iter = coordinates;

        int i = 0;
        while(iter != null) {
            //x[i] = MathFP.div(MathFP.toFP(iter.getX()), MathFP.toFP(figRatio));
            //y[i] = MathFP.div(MathFP.toFP(iter.getY()), MathFP.toFP(figRatio));
            //x[i] = (float) iter.getX() / figRatio;
            //y[i] = (float) iter.getY() / figRatio;
            x[i] = (float) iter.getX();
            y[i] = (float) iter.getY();
            drawX[i] = iter.getX();
            drawY[i] = iter.getY();
            
            iter = iter.getNext();
            ++i;
        }
    }

    /** 
     *  Adds a pair of coordinates to the list {@code coordinates}.
     *  However, not added if it already exists in the list.
     *
     *  @return index of the added pair of coordinates 
     */
    int addCoordinate(int x, int y) {
    
        if(coordinates == null) {
            coordinates = new AtomCoordinate(x, y, null);
            return 0;
        }

        int i = 0;
        AtomCoordinate iter = coordinates;
        AtomCoordinate prev = null;

        while(iter != null) {
            if((iter.getX() == x) && (iter.getY() == y)) {
                return i;
            }

            ++i;
            prev = iter;
            iter = iter.getNext();
        }

        AtomCoordinate temp = new AtomCoordinate(x, y, null);
        prev.setNext(temp);
        
        return i;
    }

    /** Returns the color according to the table of colors {@code colorList}.
     *  @param  index   index to the table {@code colorList} */
    public Color getColor(int index) {
        return new Color(colorList[index][0], colorList[index][1], colorList[index][2]);
    }

    /** Adds a new vector image element (circle, oval, polygon, ...) to the
     * dynamic list {@code elements}. */
    public void addElement(Element elem) {
        Element iter = elements;
        Element prev = null;

        // sort the element to the list according to its depth
        while(iter != null) {
            if(iter.getDepth() < elem.getDepth()) break;

            prev = iter;        
            iter = iter.getNext();
        }

        if(prev == null) {
            elements = elem;
        } else {
            prev.setNext(elem);
        }
        elem.setNext(iter);
    }
                        
    /** Adds a new circle element to the dynamic list {@code elements}.
     *
     *  @param  centerX     x-coordinate of the center of the circle
     *  @param  centerY     y-coordinate of the center of the circle
     *  @param  radiusX     x-coordinate of a point of the border of the circle
     *  @param  radiusY     y-coordinate of a point of the border of the circle
     *  @param  depth       depth of the circle element in the vector image
     *  @param  drawColor   color of the border of the circle
     *  @param  fillColor   filling color of the circle
     */
    public void addCircle(int centerX, int centerY, int diameter, int depth, Color drawColor, Color fillColor) {
        addElement(new Circle(addCoordinate(centerX ,centerY), diameter, depth, drawColor, fillColor, null));
    }

    /** Adds a new oval element to the dynamic list {@code elements}.
     *
     *  @param  centerX     x-coordinate of the center of the circle
     *  @param  centerY     y-coordinate of the center of the circle
     *  @param  width       width of the oval
     *  @param  height      height of the oval
     *  @param  depth       depth of the circle element in the vector image
     *  @param  drawColor   color of the border of the circle
     *  @param  fillColor   filling color of the circle
     */
    public void addOval(int centerX, int centerY, int width, int height, int depth, Color drawColor, Color fillColor) {
        addElement(new Oval(addCoordinate(centerX ,centerY), width, height, depth, drawColor, fillColor, null));
    }

    /** Adds a new polygon element to the dynamic list {@code elements}.
     *
     *  A polygon differs from a polyline in being closed (i.e. its first
     *      vertex matches its last vertex).
     *
     *  @param  numVertices number of vertices of the polygon
     *  @param  depth       depth of the polygon element in the vector image
     *  @param  drawColor   color of the border of the polygon
     *  @param  fillColor   filling color of the polygon
     *
     *  @return reference to the newly created polygon
     */
    public Polygon addPolygon(int numVertices, int depth, Color drawColor, Color fillColor) {
        //System.out.println("creating new polygon: depth=" + depth + ", drawColor=" + drawColor + ", fillColor=" + fillColor);
        Polygon polygon = new Polygon(numVertices, depth, drawColor, fillColor, null);
        addElement(polygon);
        return polygon;
    }

    /** Adds a new polyline element to the dynamic list {@code elements}.
     * 
     *  A polyline differs from a polygon in not being closed (i.e. its first
     *      vertex does not need to match its last vertex).
     *
     *  @param  numVertices number of vertices of the polyline
     *  @param  depth       depth of the polyline element in the vector image
     *  @param  drawColor    index of the color of the polyline (see {@code getColor})
     *
     *  @return reference to the newly created polyline
     */
    public Polyline addPolyline(int numVertices, int depth, Color drawColor) {
        Polyline polyline = new Polyline(numVertices, depth, drawColor, null);
        addElement(polyline);
        return polyline;
    }

    /** Rotates, translates, and scales the coordinates of the vector image.
     * 
     *  The rotation vector states to which direction the image shall be rotated.
     *
     *  This method is supposed to be called before calling the method {@code draw}.
     *
     *  @param  tx      x-coordinate of the translation vector
     *  @param  ty      y-coordinate of the translation vector
     *  @param  rx      x-coordinate of the rotation vector
     *  @param  ry      y-coordinate of the rotation vector
     *  @param  scale   scale coefficient; {@code 1} stands for no scaling
     */
    public void rotateTranslateAndScale(float tx, float ty, float rx, float ry, float scale) {
        float xi;
        float yi;
        float rl = (float) Math.sqrt(rx * rx + ry * ry);
            
        this.diameterScale = scale;

        for(int i = 0; i < numPoints; ++i) {
            if(scale == 1.0f) {
                xi = x[i];
                yi = y[i];
            } else {
                xi = scale * x[i];
                yi = scale * y[i];
            }
            if(rl == 0.0f) {
                drawX[i] = Math.round(xi + tx);
                drawY[i] = Math.round(yi + ty);
            } else {
                drawX[i] = Math.round(((((xi * rx) + (yi * (-ry))) / rl) + tx));
                drawY[i] = Math.round(((((xi * ry) + (yi * rx)) / rl) + ty));
            }
        }
    }

    /** Rotates and translates the coordinates of the vector image (no scaling).
     * 
     *  The rotation vector states to which direction the image shall be rotated.
     *
     *  This method is supposed to be called before calling the method {@code draw}.
     *
     *  @param  tx      x-coordinate of the translation vector
     *  @param  ty      y-coordinate of the translation vector
     *  @param  rx      x-coordinate of the rotation vector
     *  @param  ry      y-coordinate of the rotation vector
     */
    public void rotateAndTranslate(float tx, float ty, float rx, float ry) {
        rotateTranslateAndScale(tx, ty, rx, ry, 1.0f);
    }

    /** Draws the vector image.
     *  @param  g   graphical context */
    public void draw(Graphics g) {
        for(Element iter = elements; iter != null; iter = iter.getNext()) {
            iter.draw(g);
        }
    }

    /**
     * Prints some informations about the vector image to the text console.
     *
     * This function serves mainly for the testing purposes.
     */
    public void consoleInfo() {
        System.out.println("");
        System.out.println("points:");
        System.out.println("-------");
        System.out.println("numPoints: "+numPoints);

        for(int i = 0; i < numPoints; ++i) {
            System.out.print("x[" + i + "]: " + x[i] + " ");
            System.out.print("y[" + i + "]: " + y[i] + " ");
            System.out.print("drawX[" + i + "]: " + drawX[i] + " ");
            System.out.print("drawY[" + i + "]: " + drawY[i] + " ");
            System.out.println();
        }

        for(AtomCoordinate iter = coordinates; iter != null; iter = iter.getNext()) {
            System.out.println(iter.toString());
        }

        System.out.println("");
        System.out.println("elements:");
        System.out.println("---------");
        for(Element iter = elements; iter != null; iter = iter.getNext()) {
            System.out.println(iter.toString());
        }
    }

    /** Exports the vector image to java code.
     *
     *  Creates a file {@code className.java} and saves the vector image as a
     *      class named {@code className}.
     */
    public void exportToJava(String className) throws IOException {
        String fileName = className + ".java";
        FileWriter out = new FileWriter(fileName);

        out.write("import java.awt.*;\n");
        out.write("\n");
        out.write("/**\n");
        out.write(" * " + className + " is a class representing a vector image created automatically \n");
        out.write(" * by the method {@code exportToJava} of class {@code VectorImage}.\n");
        out.write(" *\n");
        out.write(" * The class overrides only the constructor in which the vector image is\n");
        out.write(" * defined.\n");
        //out.write(" *\n");
        //out.write(" * @version     %I%, %G%\n");
        //out.write(" * @author        Milan Petrik\n");
        out.write(" */\n");
        out.write("public class " + className + " extends VectorImage {\n");
        out.write("\n");
        out.write("    /**\n");
        out.write("     * Creates and defines a new vector image.\n");
        out.write("     */\n");
        out.write("  " + className + "() {\n");
        out.write("    super();\n");
        out.write("\n");
        out.write("    numPoints = " + numPoints + ";\n");
        out.write("    x = new long[numPoints];\n");
        out.write("    y = new long[numPoints];\n");
        out.write("    drawX = new int[numPoints];\n");
        out.write("    drawY = new int[numPoints];\n");
        out.write("    elements = null;\n");
        out.write("    coordinates = null;\n");
        out.write("\n");
        out.write("    Element elem = null;\n");
        out.write("    Circle circle = null;\n");
        out.write("    Oval oval = null;\n");
        out.write("    Polygon polygon = null;\n");
        out.write("    Polyline polyline = null;\n");
        for(int i = 0; i < numPoints; ++i) {
            out.write("\n");
            out.write("    x[" + i + "] = " + x[i] + ";\n");
            out.write("    y[" + i + "] = " + y[i] + ";\n");
            out.write("    drawX[" + i + "] = " + drawX[i] + ";\n");
            out.write("    drawY[" + i + "] = " + drawY[i] + ";\n");
        }
        for(Element iter = elements; iter != null; iter = iter.getNext()) {
            out.write("\n");
            out.write(iter.exportToJava("    "));
        }
        out.write("  }\n");
        out.write("}\n");

        out.close();
    }

    // ==================
    //     SUBCLASSES
    // ==================

    /**
     *  Represents one abstract graphical vector element of the vector image.
     *
     *  The element can be one of the following types: polygon, polyline,
     *      circle, oval.
     *
     *  The vector elements are stored in a one-way ordered dynamic list hence
     *      every vector element contains a reference to its successor.
     */
    public abstract class Element {
        protected Color borderColor;
        protected Color fillColor;

        /** Determines in which order the vector elements shall be drawn. 
         *  When a new instance of {@code Element} are created and added to the
         *      list, it is, actually, sorted to the list according to its value of
         *      {@code depth}. */
        protected int depth;

        /** Reference to the successor of this element in the dynamic list. */
        private Element next;

        /**
         *  Constructor.
         *
         *  @param  depth       determines in which order the image elements shall be drawn
         *  @param  borderColor color of the border of the element
         *  @param  fillColor   filling color of the element
         *  @param  next        reference to the successor of this element in the dynamic list
         */
        Element(int depth, Color borderColor, Color fillColor, Element next) {
            this.depth = depth;
            this.borderColor = borderColor;
            this.fillColor = fillColor;
            this.next = next;
        }

        /** Sets the successor of this element. */
        public void setNext(Element next) {
            this.next = next;
        }

        /** Returns the successor of this element. */
        public Element getNext() {
            return next;
        }

        /** Returns the depth of this element. */
        public int getDepth() {
            return depth;
        }

        /** Returns a string representation of the vector element.
         *  Mainly for debugging purposes. */
        public String toString() {
            return "(element depth=" + depth + ")";
        }

        /** Returns java code describing creation of this vector element.
         *  @return string containing a java code describing the creation of this vector element */
        abstract public String exportToJava(String indent);

        /** Returns java code describing the border color of this vector element.
         *  Expected to be called in the method {@code exportToJava}.
         *  @return string with java code specifying the border color */
        public String exportDrawColorToJava() {
            return "new Color(" + borderColor.getRed() + ", " + borderColor.getGreen() + ", " + borderColor.getBlue() + ")";
        }

        /** Returns java code describing the filling color of this vector element.
         *  Expected to be called in the method {@code exportToJava}.
         *  @return string with java code specifying the filling color
         */
        public String exportFillColorToJava() {
            return "new Color(" + fillColor.getRed() + ", " + fillColor.getGreen() + ", " + fillColor.getBlue() + ")";
        }

        /** Draws the vector element.
         *  @param  g   graphical context */
        public abstract void draw(Graphics g);
    }

    /** Represents a polygon.
     *
     *  Polygon is a sequence of connected line segments such that the first
     *      vertex matches the last vertex.
     *
     *  The polygon is represented by an array of vertices.
     *  The last vertex (which corresponds with the first one) is not included
     *      in the array.
     */
    public class Polygon extends Element {

        /** Number of the vertices of the polygon */
        protected int numVertices;

        /** Indices to the array of points in {@code VectorImage} (which are
         *      {@code x[]}, {@code y[]}, {@code drawX[]}, and {@code drawY[]}) */
        protected int[] vertices;

        /** Coordinates of the polygon points.
         *  Serve to draw the polygon.
         *  Before each call of the method {@code draw}, these values are
         *      copied from the arrays {@code drawX[]} and {@code drawY[]} of
         *      {@code VectorImage}. */
        protected int[] polyX, polyY;

        /** Constructor.
         *
         *  @param  numVertices number of vertices of the polygon
         *  @param  depth       determines in which order the image elements shall be drawn
         *  @param  borderColor color of the border of the element
         *  @param  fillColor   filling color of the element
         *  @param  next        reference to the successor of this element in the dynamic list
         */
        Polygon(int numVertices, int depth, Color borderColor, Color fillColor, Element next) {
            super(depth, borderColor, fillColor, next);
            this.numVertices = numVertices;
            vertices = new int [numVertices];
            polyX = new int [numVertices];
            polyY = new int [numVertices];
        }

        /** Specifies the value of a vertex according to an existing point in {@code VectorImage.coordinates}.
         *
         *  @param  index   index determining which vertex shall be overwritten
         *  @param  point   index determining by which point in {@code VectorImage.coordinates} the vertex shall be overwritten
         */
        public void setVertex(int index, int point) {
            vertices[index] = point;
        }

        /** Specifies the value of a vertex directly by its coordinates.
         *
         *  The function occasionally adds a new point to {@code VectorImage.coordinates}.
         *
         *  @param  index   index determining which vertex shall be overwritten
         *  @param  x       x-coordinate
         *  @param  y       y-coordinate
         */
        public void setVertex(int index, int x, int y) {
            vertices[index] = addCoordinate(x, y);
        }

        /** Draws the polygon.
         *  @param  g   graphical context */
        public void draw(Graphics g) {
            for(int i = 0; i < numVertices; ++i) {
                polyX[i] = drawX[vertices[i]];
                polyY[i] = drawY[vertices[i]];
            }
            if(fillColor != null) {
                g.setColor(fillColor);
                g.fillPolygon(polyX, polyY, numVertices);
            }
            if(borderColor != null) {
                g.setColor(borderColor);
                g.drawPolygon(polyX, polyY, numVertices);
            }
        }

        /** Returns a string representation of the polygon.
         *  Mainly for debugging purposes.
         *  @return a string of the format "(polygon[number of vertices]: array of vertices)"
         */
        public String toString() {
            String result = "(polygon[" + numVertices + "]: ";
            for(int i = 0; i < numVertices; ++i) {
                if(i != 0) result += ",";
                result += "(" + vertices[i] + ")";
            }
            result += ")";
            return super.toString() + " " + result;
        }

        /** Returns java code describing creation of the vertices of this polygon.
         *  Expected to be called in the method {@code exportToJava}.
         *  @return string containing a java code describing the creation of the vertices of this polygon */
        public String exportVerticesToJava(String indent, String polygonName) {
            String text = "";

            for(int i = 0; i < numVertices; ++i) {
                text += indent + polygonName + ".setVertex(" + i + ", " + vertices[i] + ");\n";
            }

            return text;
        }

        /** Returns java code describing creation of this polygon.
         *  Expected to be called in the method {@code exportToJava}.
         *  @return string containing a java code describing the creation of this polygon */
        public String exportToJava(String indent) {
            String text = indent + "polygon = new Polygon(" + numVertices + ", " + depth + ", "
                + exportDrawColorToJava() + ", "
                + exportFillColorToJava() + ", "
                + "elem);\n";

            text += indent + "addElement(polygon);\n";
            text += exportVerticesToJava(indent, "polygon");
            text += indent + "elem = polygon;\n";

            return text;
        }
    }

    /** Represents a polyline.
     *
     *  Polyline is a sequence of connected line segments.
     *  It is represented by an array of vertices.
     *  Note that polyline does not use the filling color.
     */
    public class Polyline extends Polygon {
        /**
         * Constructor.
         *
         *  @param  numVertices number of vertices of the polygon
         *  @param  depth       determines in which order the image elements shall be drawn
         *  @param  borderColor color of the border of the element
         *  @param  next        reference to the successor of this element in the dynamic list
         */
        Polyline(int numVertices, int depth, Color borderColor, Element next) {
            super(numVertices, depth, borderColor, null, next);
        }

        /** Draws the polyline.
         * @param g     graphical context */
        public void draw(Graphics g) {
            for(int i = 0; i < numVertices; ++i) {
                polyX[i] = drawX[vertices[i]];
                polyY[i] = drawY[vertices[i]];
            }
            if(borderColor != null) {
                g.setColor(borderColor);
                g.drawPolyline(polyX, polyY, numVertices);
            }
        }

        /** Returns a string representation of the polyline.
         *  Mainly for debugging purposes.
         *  @return a string of the format "(polyline[number of vertices]: array of vertices)"
         */
        public String toString() {
            String result = "(polyline[" + numVertices + "]: ";
            for(int i = 0; i < numVertices; ++i) {
                if(i != 0) result += ",";
                result += "(" + vertices[i] + ")";
            }
            result += ")";
            return super.toString() + " " + result;
        }

        /** Returns java code describing creation of this polyline.
         *  Expected to be called in the method {@code exportToJava}.
         *  @return string containing a java code describing the creation of this polyline */
        public String exportToJava(String indent) {
            String text = indent + "polyline = new Polyline(" + numVertices + ", " + depth + ", "
                + exportDrawColorToJava() + ", "
                + "elem);\n";

            text += indent + "addElement(polyline);\n";
            text += exportVerticesToJava(indent, "polyline");
            text += indent + "elem = polyline;\n";

            return text;
        }
    }

    /** Represents a circle.
     *
     *  The circle is described by the position of its center and by its
     *      diameter.
     */
    public class Circle extends Element {

        /** Position of the center of the circle.
         *      It is, actually, an index to the array of points in 
         *      {@code VectorImage} (which are
         *      {@code x[]}, {@code y[]}, {@code drawX[]}, and {@code drawY[]}). */
        private int position;

        /** Diameter (size) of the circle. */
        private int diameter;

        /**
         * Constructor.
         *
         *  @param  position    index to the array in the vector image giving the center of the circle
         *  @param  diameter    diameter of the circle
         *  @param  depth       determines in which order the image elements shall be drawn
         *  @param  borderColor color of the border of the element
         *  @param  fillColor   filling color of the element
         *  @param  next        reference to the successor of this element in the dynamic list
         */
        Circle(int position, int diameter, int depth, Color borderColor, Color fillColor, Element next) {
            super(depth, borderColor, fillColor, next);
            this.position = position;
            this.diameter = diameter;
        }

        /** Draws the circle.
         * @param g     graphical context */
        public void draw(Graphics g) {
            int d = Math.round(diameterScale * diameter);
            g.setColor(fillColor);
            fillCenteredOval(drawX[position], drawY[position], d, d, g);
            g.setColor(borderColor);
            drawCenteredOval(drawX[position], drawY[position], d, d, g);
        }

        /** Returns a string representation of the circle.
         *  Mainly for debugging purposes.
         *  @return a string of the format "(circ: pos=" + position + ", size=" + size + ")"
         */
        public String toString() {
            return super.toString() + " " + "(circ: pos=" + position + ", d=" + diameter + ")";
        }

        /** Returns java code describing creation of this circle.
         *  Expected to be called in the method {@code exportToJava}.
         *  @return string containing a java code describing the creation of this circle */
        public String exportToJava(String indent) {
            String text = indent + "circle = new Circle(" + position + ", " + diameter + ", " + depth + ", "
                + exportDrawColorToJava() + ", "
                + exportFillColorToJava() + ", "
                + "elem);\n";

            text += indent + "addElement(circle);\n";
            text += indent + "elem = circle;\n";

            return text;
        }
    }

    /** Represents an oval (aligned ellipse).
     *
     *  An oval is an ellipse such that the axes of the ellipse are aligned
     *      with the axes of the global coordinate system.
     *
     *  An oval is described by the position of its center and by its
     *      width and height.
     */
    public class Oval extends Element {
        /** Position of the center of the oval.
         *      It is, actually, an index to the array of points in 
         *      {@code VectorImage} (which are
         *      {@code x[]}, {@code y[]}, {@code drawX[]}, and {@code drawY[]}). */
        private int position;

        /** Diameter in the direction of the x-axis. */
        private int width;

        /** Diameter in the direction of the y-axis. */
        private int height;

        /** Creates an oval.
         *
         * @param position      index to the array in the vector image giving the center of the oval
         * @param width         width of the oval
         * @param height        height of the oval
         * @param depth         depth of the element
         * @param borderColor   color of the border of the element
         * @param fillColor     filling color of the element
         * @param next          reference to the next element
         */
        Oval(int position, int width, int height, int depth, Color borderColor, Color fillColor, Element next) {
            super(depth, borderColor, fillColor, next);
            this.position = position;
            this.width = width;
            this.height = height;
        }

        /** Draws the oval.
         * @param g     graphical context */
        public void draw(Graphics g) {
            int drawnWidth  = Math.round(diameterScale * width);
            int drawnHeight = Math.round(diameterScale * height);
            g.setColor(fillColor);
            fillCenteredOval(drawX[position], drawY[position], drawnWidth, drawnHeight, g);
            g.setColor(borderColor);
            drawCenteredOval(drawX[position], drawY[position], drawnWidth, drawnHeight, g);
        }

        /** Returns a string representation of the oval.
         *  Mainly for debugging purposes.
         *  @return a string of the format "(oval: pos=" + position + ", width=" + width + ", height=" + height + ")"
         */
        public String toString() {
            return super.toString() + " " + "(oval: pos=" + position + ", width=" + width + ", height=" + height + ")";
        }

        /** Returns java code describing creation of this oval.
         *  Expected to be called in the method {@code exportToJava}.
         *  @return string containing a java code describing the creation of this oval */
        public String exportToJava(String indent) {
            String text = indent + "oval = new Oval(" + position + ", " + width + ", " + height + ", " + depth + ", "
                + exportDrawColorToJava() + ", "
                + exportFillColorToJava() + ", "
                + "elem);\n";

            text += indent + "addElement(circle);\n";
            text += indent + "elem = circle;\n";

            return text;
        }
    }

    /** Draws a centered oval.
     *
     *  This does exactly the same as the standard function {@code drawOval},
     *      however, the oval is specified by its center instead of its upper left
     *      corner.
     *
     *  @param  x   x-coordinate of the center of the oval
     *  @param  y   y-coordinate of the center of the oval
     *  @param  w   width of the oval in pixels
     *  @param  h   height of the oval in pixels
     *  @param  g   graphical context
     */
    public static void drawCenteredOval(int x, int y, int w, int h, Graphics g) {
        g.drawOval(x - w/2, y - h/2, w, h);
    }

    /** Draws a filled centered oval.
     *
     *  This does exactly the same as the standard function {@code fillOval},
     *      however, the oval is specified by its center instead of its upper left
     *      corner.
     *
     *  @param  x   x-coordinate of the center of the oval
     *  @param  y   y-coordinate of the center of the oval
     *  @param  w   width of the oval in pixels
     *  @param  h   height of the oval in pixels
     *  @param  g   graphical context
     */
    public static void fillCenteredOval(int x, int y, int w, int h, Graphics g) {
        g.fillOval(x - w/2, y - h/2, w, h);
    }

    /** Draws a centered text. 
     *
     *  @param  x       x-coordinate of the center of the text
     *  @param  y       y-coordinate of the center of the text
     *  @param  text    the string to be drawn (written)
     *  @param  font    the font of the text; example: {@code new Font("Arial", font.PLAIN, 16)}
     *  @param  g       graphical context
     * */
    public static void drawCenteredString(int x, int y, String text, Font font, Graphics g) {
        FontMetrics metrics = g.getFontMetrics(font);
        int drawX = x - (metrics.stringWidth(text) / 2);
        int drawY = y - (metrics.getHeight() / 2) + metrics.getAscent();
        g.setFont(font);
        g.drawString(text, drawX, drawY);
    }

    /** Writes a character at the specified position.
     *
     *  This method draws a character (a letter) by line segments respecting a
     *      grid.
     *  The size of the grid is determined by the parameter {@code step}.
     *  At the moment, only 'I', 'L', 'O', 'P', or 'T' can be drawn,
     *      just enough to write the word "PILOT".
     *
     *  @param  c       character to be written
     *  @param  x       x-coordinate of the character 
     *  @param  y       y-coordinate of the character 
     *  @param  step    size of the grid into which the character is written
     *  @param  color   color of the character
     *  @param  g       graphical context
     *
     *  @return width of the character as a multiple of {@code step} 
     *      (this can be used to determine the position where to write the next character)
     */
    public static int drawCharacter(char c, int x, int y, int step, Color color, Graphics g) {
        int len;
        int[] polyX;
        int[] polyY;

        g.setColor(color);

        switch(c) {
            case 'i':
            case 'I':
                len = 2;
                polyX = new int [len];
                polyY = new int [len];

                polyX[0] = x;
                polyY[0] = y;
                polyX[1] = x;
                polyY[1] = y + (3 * step);

                g.drawPolyline(polyX, polyY, len);

                return 1;

            case 'l':
            case 'L':
                len = 3;
                polyX = new int [len];
                polyY = new int [len];

                polyX[0] = x;
                polyY[0] = y;
                polyX[1] = x;
                polyY[1] = y + (3 * step);
                polyX[2] = x + (2 * step);
                polyY[2] = y + (3 * step);

                g.drawPolyline(polyX, polyY, len);

                return 3;

            case 'o':
            case 'O':
                len = 5;
                polyX = new int [len];
                polyY = new int [len];

                polyX[0] = x;
                polyY[0] = y;
                polyX[1] = x;
                polyY[1] = y + (3 * step);
                polyX[2] = x + (2 * step);
                polyY[2] = y + (3 * step);
                polyX[3] = x + (2 * step);
                polyY[3] = y;
                polyX[4] = x;
                polyY[4] = y;

                g.drawPolyline(polyX, polyY, len);

                return 3;

            case 'p':
            case 'P':
                len = 5;
                polyX = new int [len];
                polyY = new int [len];

                polyX[0] = x;
                polyY[0] = y + (3 * step);
                polyX[1] = x;
                polyY[1] = y;
                polyX[2] = x + (2 * step);
                polyY[2] = y;
                polyX[3] = x + (2 * step);
                polyY[3] = y + (2 * step);
                polyX[4] = x;
                polyY[4] = y + (2 * step);

                g.drawPolyline(polyX, polyY, len);

                return 3;

            case 't':
            case 'T':
                len = 2;
                polyX = new int [len];
                polyY = new int [len];

                polyX[0] = x;
                polyY[0] = y;
                polyX[1] = x + (2 * step);
                polyY[1] = y;

                g.drawPolyline(polyX, polyY, len);

                len = 2;
                polyX = new int [len];
                polyY = new int [len];

                polyX[0] = x + step;
                polyY[0] = y;
                polyX[1] = x + step;
                polyY[1] = y + (3 * step);

                g.drawPolyline(polyX, polyY, len);

                return 3;
        }

        return 0;
    }

    // ==================
    //     EXCEPTIONS
    // ==================

    /**
     *  Implements reporting of the errors that may occur when parsing the text
     *      data file.
     */
    class UnknownGraphicObjectTypeError extends Exception {
        UnknownGraphicObjectTypeError(int lineNumber, String msg) {
            super("SEMANTIC ERROR ON LINE " + lineNumber + ": " + msg);
        }
    }

    // =================
    //     UNIT TEST
    // =================

    /** Performs unit testing. */
    public static void main(String[] args)
            throws FileNotFoundException, IOException, TextDataFile.ParseError, UnknownGraphicObjectTypeError {
        UnitTestVectorImage unitTest = new UnitTestVectorImage();
        unitTest.createBuffer();
        unitTest.run();
    }
}

// ====================
//     UNIT TESTING
// ====================

/**
 *  Implemets a unit test.
 */
class UnitTestVectorImage {
    /** Geometry of the window */
    private Frame testFrame;
    private int testFrameWidth =  Constants.GAME_CANVAS_WIDTH + 20; 
    private int testFrameHeight = Constants.GAME_CANVAS_HEIGHT + 50;

    /** Geometry of the canvas */
    private TestCanvas testCanvas;
    private int testCanvasWidth =  Constants.GAME_CANVAS_WIDTH;
    private int testCanvasHeight = Constants.GAME_CANVAS_HEIGHT;

    /** instance of random number generator which all the classes share */
    private Random rand;    

    private VectorImage vectorImageStatic;
    private VectorImage vectorImageMoving;
    private VectorImage[] images;
    private int numImages;

    /** Initializes a simple frame with a canvas and creates an instance of {@code Background}. */
    UnitTestVectorImage()
            throws  FileNotFoundException,
                    IOException,
                    TextDataFile.ParseError,
                    VectorImage.UnknownGraphicObjectTypeError {
        rand = new Random();

        testFrame = new Frame();
        testFrame.setSize(testFrameWidth, testFrameHeight);
        testFrame.setLocation(100, 100);
        testFrame.addWindowListener(new FrameWA());
        testFrame.setVisible(true);
        testFrame.setResizable(true);

        Panel panel = new Panel();
        panel.setLayout(new FlowLayout());
        panel.setBackground(new Color(50, 0, 50));
        testFrame.add(panel);

        testCanvas = new TestCanvas();
        testCanvas.setSize(testCanvasWidth, testCanvasHeight);
        panel.add(testCanvas);

        VectorImage.Polygon polygon;
        VectorImage.Polyline polyline;

        vectorImageStatic = new VectorImage();
        polygon = vectorImageStatic.addPolygon(4, 10, Color.MAGENTA, Color.YELLOW);
        polygon.setVertex(0, 0, 0);
        polygon.setVertex(1, 50, 0);
        polygon.setVertex(2, 50, 50);
        polygon.setVertex(3, 0, 50);
        vectorImageStatic.addCircle(0, 0, 50, 2, Color.BLUE, Color.GREEN);
        vectorImageStatic.addCircle(50, 50, 50, 2, Color.BLUE, Color.GREEN);
        vectorImageStatic.addOval(50, -50, 20, 40, 2, Color.BLUE, Color.GREEN);
        polyline = vectorImageStatic.addPolyline(4, 5, Color.ORANGE);
        polyline.setVertex(0, 0, 0);
        polyline.setVertex(1, 0, -50);
        polyline.setVertex(2, -50, 0);
        polyline.setVertex(3, -50, -50);
        vectorImageStatic.copyCoordinates();
        //vectorImageStatic.consoleInfo();

        vectorImageMoving = new VectorImage();
        polygon = vectorImageMoving.addPolygon(4, 10, Color.MAGENTA, Color.YELLOW);
        polygon.setVertex(0, 0, 0);
        polygon.setVertex(1, 50, 0);
        polygon.setVertex(2, 50, 50);
        polygon.setVertex(3, 0, 50);
        vectorImageMoving.addCircle(0, 0, 50, 2, Color.BLUE, Color.GREEN);
        vectorImageMoving.addCircle(50, 50, 50, 2, Color.BLUE, Color.GREEN);
        vectorImageMoving.addOval(50, -50, 20, 40, 2, Color.BLUE, Color.GREEN);
        polyline = vectorImageMoving.addPolyline(4, 5, Color.ORANGE);
        polyline.setVertex(0, 0, 0);
        polyline.setVertex(1, 0, -50);
        polyline.setVertex(2, -50, 0);
        polyline.setVertex(3, -50, -50);
        vectorImageMoving.copyCoordinates();
        //vectorImageMoving.consoleInfo();

        numImages = 5;
        images = new VectorImage[numImages];
        images[0] = new VectorImage(Constants.DIRECTORY_WITH_MODELS + "bonus_shotgun.fig");
        images[1] = new VectorImage(Constants.DIRECTORY_WITH_MODELS + "bonus_shield.fig");
        images[2] = new VectorImage(Constants.DIRECTORY_WITH_MODELS + "bonus_laser.fig");
        images[3] = new VectorImage(Constants.DIRECTORY_WITH_MODELS + "bonus_life.fig");
        images[4] = new VectorImage(Constants.DIRECTORY_WITH_MODELS + "shuttle.fig");
    }

    /** The main loop. */
    public void run() {
        float deltaTime = 0.0f;
        long nanoDeltaTime = 0;
        long previousTime = 0;
        long currentTime = System.nanoTime();

        float timeToNextObject = 1.0f;

        float currentTimeInSeconds;

        while(testCanvas.isVisible()) {
            previousTime = currentTime;
            currentTime = System.nanoTime();
            nanoDeltaTime = currentTime - previousTime;

            currentTimeInSeconds = (float) ((double) currentTime / 1000000000.0);

            // keep frames per second rate at maximum of 50
            if (nanoDeltaTime < 20000000L) {
                try {
                    Thread.sleep((20000000L - nanoDeltaTime) / 1000000L);
                } catch(InterruptedException e) {}
                currentTime = System.nanoTime();
                nanoDeltaTime = currentTime - previousTime;
            }

            deltaTime = (float) ((double) nanoDeltaTime / 1000000000.0);

            vectorImageStatic.rotateTranslateAndScale(
                    Constants.GAME_CANVAS_WIDTH / 2, Constants.GAME_CANVAS_HEIGHT / 2,
                    1.0f, 0.0f, // rotation
                    1.0f //scale
                    );

            vectorImageMoving.rotateTranslateAndScale(
                    (float) (Constants.GAME_CANVAS_WIDTH / 2 + 200 * Math.cos(currentTimeInSeconds)), 
                    (float) (Constants.GAME_CANVAS_HEIGHT / 2 + 200 * Math.sin(currentTimeInSeconds)),
                    (float) Math.cos(-currentTimeInSeconds), //rotation
                    (float) Math.sin(-currentTimeInSeconds), //rotation
                    1.1f + (float) Math.cos(-currentTimeInSeconds) //scale
                    );

            for (int i = 0; i < numImages; ++i) {
                images[i].rotateTranslateAndScale(
                        75.0f + 125.0f * i,
                        150.0f,
                        (float) Math.cos(-currentTimeInSeconds), //rotation
                        (float) Math.sin(-currentTimeInSeconds), //rotation
                        0.04f + 0.02f * (float) Math.cos(-currentTimeInSeconds) //scale
                        );
            }

            testCanvas.draw();
        }
    }

    /** Creates a buffer for the double-buffered graphics. */
    public void createBuffer() {
        testCanvas.createBufferStrategy(2);
        testCanvas.strategy = testCanvas.getBufferStrategy();
    }

    /** Allows the frame to be closed. */
    private class FrameWA extends WindowAdapter {
        public void windowClosing(WindowEvent e) {
            System.exit(1);
        }
    }

    /** Displays the graphics. */
    class TestCanvas extends Canvas {
        public BufferStrategy strategy;
        public void draw() {
            Graphics g = strategy.getDrawGraphics();

            g.setColor(new Color(0, 0, 50));
            g.fillRect(0, 0, testCanvasWidth, testCanvasHeight);

            vectorImageStatic.draw(g);
            vectorImageMoving.draw(g);

            for (int i = 0; i < numImages; ++i) {
                images[i].draw(g);
            }

            Color color = Color.YELLOW;
            int step = 5;
            int cursorX = 100;
            int cursorY = Constants.GAME_CANVAS_HEIGHT - 100;
            cursorX = cursorX + step + (step * VectorImage.drawCharacter('P', cursorX, cursorY, step, color, g));
            cursorX = cursorX + step + (step * VectorImage.drawCharacter('I', cursorX, cursorY, step, color, g));
            cursorX = cursorX + step + (step * VectorImage.drawCharacter('L', cursorX, cursorY, step, color, g));
            cursorX = cursorX + step + (step * VectorImage.drawCharacter('O', cursorX, cursorY, step, color, g));
            cursorX = cursorX + step + (step * VectorImage.drawCharacter('T', cursorX, cursorY, step, color, g));

            g.dispose();
            strategy.show();
        }
    }
}
