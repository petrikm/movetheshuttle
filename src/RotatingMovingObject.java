import java.awt.*;
import java.util.*;

/**
 *  Represents an abstract rotating and moving object on the game canvas.
 *
 *  Such an object is mainly characterised by its position, velocity vector,
 *      angle, and collision radius.
 *
 *  @see MovingObject
 *  @see ListOfMovingObjects
 */
public abstract class RotatingMovingObject extends MovingObject {
   
    /** Current rotation angle of the pilot. */
    private float rotationAngle;

    /** Rotation speed of the pilot in radians per second. */
    private float rotationSpeed;

    /** Vector representing the direction of the moving object. 
     *  It is, actually, cosinus and sinus of {@code rotationAngle} need by the
     *      method {@code rotateAndTranslate} of the class {@code VectorImage}.
     *  It has shown to be more beneficial to have the rotation defined by a
     *      vector instead on an angle.
     *  Some objects have their direction defined already (e.g. the shuttle by
     *      its velocity vector) and hence there is no need to compute the angle
     *      and consequently its sinus and cosinus. */
    protected float directionX;
    protected float directionY;

    /**
     *  Creates a new rotating object with given position and velocity and with random rotation speed.
     *
     *  @param  positionX           x-coordinate of the position
     *  @param  positionY           y-coordinate of the position
     *  @param  velocityX           x-coordinate of the velocity vector
     *  @param  velocityY           y-coordinate of the velocity vector
     *  @param  collisionRadius     serves to detect collisions with other moving objects
     *  @param  rand                reference to a random number generator shared by all classes of the program
     */
    RotatingMovingObject(
            float positionX,
            float positionY,
            float velocityX,
            float velocityY,
            float collisionRadius,
            Random rand) {
        super(positionX, positionY, velocityX, velocityY, collisionRadius, rand);
        rotationAngle = 0.0f;
        directionX = 1.0f;
        directionY = 0.0f;
        setRandomRotationSpeed();
    }

    public void setRandomRotationSpeed() {
        rotationSpeed = 4.0f * (float) Math.PI * (rand.nextFloat() - 0.5f);
    }

    /** Moves the object reflecting its velocity vector, rotation speed, and the given time interval.
     *  @param  deltaTime   time interval according to which the new position and angle of the moving object is computed */
	public void move(float deltaTime) {
		super.move(deltaTime);
        rotationAngle += deltaTime * rotationSpeed;
        directionX = (float) Math.cos(rotationAngle);
        directionY = (float) Math.sin(rotationAngle);
	}
}
