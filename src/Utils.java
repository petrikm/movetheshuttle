/**
 *  Various utility functions needed by the program.
 */
public class Utils {

    /** Returns the value given by the affine (or linear) interpolation.
     *
     *  Value {@code x} is variable.
     *  Values {@code a,b,c,d} are parameters.
     *  If {@code x == a} then {@code c} is returned.
     *  If {@code x == b} then {@code d} is returned.
     *  If {@code x} is between {@code a} and {@code b} then the linearly
     *      interpolated valued between {@code c} and {@code d} is returned. 
     *
     *  Important note:
     *      It is expected that {@code a < b}.
     */
    public static float getInterpolation(float x, float a, float b, float c, float d) {
        return (d-c) / (b-a) * x + (c*b - a*d) / (b-a);
    }

    /** Returns the value interpolated asymptotically between the values c and d.
     *
     *  Value {@code x} is variable.
     *  Values {@code a,b,c,d} are parameters.
     *  If {@code x == a} then {@code c} is returned.
     *  If {@code x == b} then {@code (c+d)/2} is returned.
     *  If {@code x} approaches the positive infinity then the return value approaches {@code d}.
     *
     *  Important note:
     *      It is expected that {@code a < b} and {@code c > d}.
     */
    public static float getAsymptoticInterpolation(float x, float a, float b, float c, float d) {
        return (c-d) * (b - a) / (x - 2.0f * a + b) + d;
    }

    /** Checks whether a given point lies inside a given circle.
     *
     *  @param  x               x-coordinate of the point
     *  @param  y               y-coordinate of the point
     *  @param  circleX         x-coordinate of the center of the circle
     *  @param  circleY         y-coordinate of the center of the circle
     *  @param  circleRadius    radius of the circle
     *
     *  @return true of the point lies inside the circle */
    public static boolean isInCircle(float x, float y, float circleX, float circleY, float circleRadius) {
        float dx = x - circleX;
        float dy = y - circleY;
        return (float) Math.sqrt(dx * dx + dy * dy) <= circleRadius;
    }

    /** Computes the distance between a point and a line.
     *  Needed e.g. in method {@code BulletShuttleLaser.isCollision} to compute
     *      the distance of an alien from the laser beam of the shuttle. 
     *  @param  x   x-coordinate of the point
     *  @param  y   y-coordinate of the point
     *  @param  lx  x-coordinate of a point that lies on the line
     *  @param  ly  y-coordinate of a point that lies on the line
     *  @param  vx  x-coordinate of a direction vector of the line
     *  @param  vy  y-coordinate of a direction vector of the line */
    public static float computeDistanceFromLine(float x, float y, float lx, float ly, float vx, float vy) {
        float differenceX = x - lx;
        float differenceY = y - ly;
        float differenceLength = (float) Math.sqrt(differenceX * differenceX + differenceY * differenceY);
        float dotProduct = differenceX * vx + differenceY * vy;
        float directionLength = (float) Math.sqrt(vx * vx + vy * vy);
        float angle = (float) Math.acos(dotProduct / (differenceLength * directionLength));
        return differenceLength * (float) Math.sin(angle);
    }

    /** Returns signum of the determinant of a 2x2 matrix of floats.
     *  The matrix is given by:
     *      a   b
     *      c   d
     * */
    public static int sgnDet(float a, float b, float c, float d) {
        float ad = a * d;
        float bc = b * c;
        if (ad > bc) {
            return 1;
        } else if (ad < bc) {
            return -1;
        } else {
            return 0;
        }
    }

    /** Performs unit testing. */
    public static void main(String[] args) {
        // -----------------------
        // test getInterpolation()
        // -----------------------
        {
            float a = 2.0f;
            float b = 5.0f;
            float c = 1.0f;
            float d = 3.0f;
            float step = 0.2f;
            System.out.println("Interpolating from (" + a + "," + c + ") to (" + b + "," + d + ") with step " + step + ":");
            int counter = 0;
            for (float x = a; x <= b; x += step) {
                float y = getInterpolation(x, a, b, c, d);
                //System.out.println("(" + x + "," + y + ")");
                //System.out.printf("(%.3f,%.3f)\n", x, y);
                System.out.printf("(%.3f,%.3f) ", x, y);
                ++ counter;
                if (counter % 4 == 0) {
                    System.out.printf("\n");
                }
            }
            System.out.printf("\n");
        }
        // -----------------------
        // test getInterpolation()
        // -----------------------
        {
            float a = 2.0f;
            float b = 5.0f;
            float c = 1.0f;
            float d = 0.5f;
            float step = 0.2f;
            System.out.println("Interpolating from (" + a + "," + c + ") to (" + b + "," + d + ") with step " + step + ":");
            int counter = 0;
            for (float x = a; x <= b; x += step) {
                float y = getInterpolation(x, a, b, c, d);
                //System.out.println("(" + x + "," + y + ")");
                //System.out.printf("(%.3f,%.3f)\n", x, y);
                System.out.printf("(%.3f,%.3f) ", x, y);
                ++ counter;
                if (counter % 4 == 0) {
                    System.out.printf("\n");
                }
            }
            System.out.printf("\n");
        }
        // ---------------------------------
        // test getAsymptoticInterpolation()
        // ---------------------------------
        {
            float a = 2.0f;
            float b = 4.0f;
            float c = 1.0f;
            float d = 0.0f;
            float step = 0.5f;
            System.out.println("Interpolating asymptotically from " + c + " to " + d + ", start: " + a + ", middle: " + b + " step: " + step + ":");
            int counter = 0;
            for (float x = a; x <= 5.0f * b; x += step) {
                float y = getAsymptoticInterpolation(x, a, b, c, d);
                //System.out.println("(" + x + "," + y + ")");
                //System.out.printf("(%.3f,%.3f)\n", x, y);
                System.out.printf("(%.3f,%.5f) ", x, y);
                ++ counter;
                if (counter % 4 == 0) {
                    System.out.printf("\n");
                }
            }
            System.out.printf("\n");
        }
        // ---------------------------------
        // test getAsymptoticInterpolation()
        // ---------------------------------
        {
            float a = 2.0f;
            float b = 4.0f;
            float c = 0.0f;
            float d = 1.0f;
            float step = 0.5f;
            System.out.println("Interpolating asymptotically from " + c + " to " + d + ", start: " + a + ", middle: " + b + " step: " + step + ":");
            int counter = 0;
            for (float x = a; x <= 5.0f * b; x += step) {
                float y = getAsymptoticInterpolation(x, a, b, c, d);
                //System.out.println("(" + x + "," + y + ")");
                //System.out.printf("(%.3f,%.3f)\n", x, y);
                System.out.printf("(%.3f,%.5f) ", x, y);
                ++ counter;
                if (counter % 4 == 0) {
                    System.out.printf("\n");
                }
            }
            System.out.printf("\n");
        }
        // ---------------------------------
        // test getAsymptoticInterpolation()
        // ---------------------------------
        {
            float a = 2.0f;
            float b = 4.0f;
            float c = 1.0f;
            float d = 0.5f;
            float step = 0.5f;
            System.out.println("Interpolating asymptotically from " + c + " to " + d + ", start: " + a + ", middle: " + b + " step: " + step + ":");
            int counter = 0;
            for (float x = a; x <= 5.0f * b; x += step) {
                float y = getAsymptoticInterpolation(x, a, b, c, d);
                //System.out.println("(" + x + "," + y + ")");
                //System.out.printf("(%.3f,%.3f)\n", x, y);
                System.out.printf("(%.3f,%.5f) ", x, y);
                ++ counter;
                if (counter % 4 == 0) {
                    System.out.printf("\n");
                }
            }
            System.out.printf("\n");
        }
        // -----------------
        // test isInCircle()
        // -----------------
        {
            float x =  2.0f;
            float y =  3.0f;
            float cx = 1.0f;
            float cy = 2.0f;
            float cr = 1.0f;
            System.out.print("Is (" + x + "," + y + ") ");
            System.out.print("inside circle (" + cx + "," + cy + "), " + cr + "? ... ");
            System.out.println(isInCircle(x, y, cx, cy, cr));
        }
        // -----------------
        // test isInCircle()
        // -----------------
        {
            float x =  1.5f;
            float y =  2.5f;
            float cx = 1.0f;
            float cy = 2.0f;
            float cr = 1.0f;
            System.out.print("Is (" + x + "," + y + ") ");
            System.out.print("inside circle (" + cx + "," + cy + "), " + cr + "? ... ");
            System.out.println(isInCircle(x, y, cx, cy, cr));
        }
    }
}
