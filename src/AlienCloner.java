import java.awt.*;
import java.util.*;

/**
 *  Represents the "blue" alien that can occasionally clone itself.
 */
public class AlienCloner extends Alien {
    /** Externally created list of aliens.
     *  When the alien decides to create its clone, the clone is added to this
     *      list. */
	private ListOfAliens listOfAliens;

    /** How many seconds to the next cloning.
     *  Decreases over time.
     *  When it reaches zero then the alien creates its clone. */
    private float timeToNextClone;

    /** To remember the player's score in the time of the creation of this instance.
     *  Player's score is used to determine the alien's toughness, that is, how
     *      fast it moves.
     *  All the clones of this alien will share the same toughness even tough
     *  the player's score may change in the time of the cloning. */
    private int rememberedScore;

    /** 
     *  Constructor. 
     *
     *  @param  positionX           x-coordinate of the position
     *  @param  positionY           y-coordinate of the position
     *  @param  listOfParticles     externally created list of particles
     *  @param  listOfAliens        externally created list of aliens
     *  @param  score               current player's score; determines how tough the newly created alien shall be
     *  @param  rand                reference to a random number generator shared by all classes of the program
     */
    AlienCloner(float positionX, float positionY,
            ListOfMovingObjects listOfParticles,
            ListOfAliens listOfAliens,
            int score,
            Random rand) {
        super(positionX, positionY, listOfParticles, score, rand);
        this.listOfAliens = listOfAliens;
        this.timeToNextClone = 2.0f + 3.0f * rand.nextFloat();
        this.contourColor = new Color(0, 0, 120);
        this.fillColor = new Color(0, 0, 200);
        this.rememberedScore = score;
	}

    /** Moves the alien.
     *  @param  deltaTime   time interval according to which the new position of the moving object is computed */
	public void move(float deltaTime) {
		super.move(deltaTime);

        timeToNextClone -= deltaTime;
        if (timeToNextClone < 0.0f && listOfAliens.getNumber() < 1000) {
            AlienCloner clone = new AlienCloner(positionX + 1.0f, positionY, listOfParticles, listOfAliens, rememberedScore, rand);
            this.addToVelocity(-100.0f, 0.0f);
            clone.addToVelocity(100.0f, 0.0f);
            listOfAliens.add(clone);
            this.timeToNextClone = 0.5f + 1.5f * rand.nextFloat();
        }
	}
}
