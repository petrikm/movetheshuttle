import java.awt.*;
import java.util.*;

/**
 * Represents the shuttle controlled by the player.
 */
public class Shuttle extends AttractedMovingObject {

    /** Collision radius of the shuttle. */
    public static final float COLLISION_RADIUS = 22.0f;

    /** Externally created lists of bullets and particles.
     *  An instance of {@code Shuttle} keeps references to these lists in order
     *      to add there new instances of bullets, particles, pilots, etc. it
     *      produces. */
    private ListOfMovingObjects listOfShuttleBullets;
    private ListOfMovingObjects listOfParticles;

    /** Turn shuttle's shooting on or off.
     *  By setting this attribute {@code true} the shuttle will shoot every
     *      time period until set to {@code false}. */
    private boolean shooting;

    /** Types of weapons that the shuttle can use. */
    public static final int GUN = 0;
    public static final int SHOTGUN = 1;
    public static final int LASER = 2;
    private static final int NUMBER_OF_WEAPONS = 3;
    private static final float GUN_BULLET_SPEED = 2.0f * Constants.GAME_CANVAS_DIAGONAL;
    private static final float SHOTGUN_BULLET_SPEED = Constants.GAME_CANVAS_DIAGONAL;
    /** How many particles per second are emitted by (each) jet. */
    private static final float JET_FIRE_FREQUENCY = 400.0f;
    /** Time difference between two emitted particles. */
    private static final float JET_FIRE_PERIOD = 1.0f / JET_FIRE_FREQUENCY;

    /** To synchronize jet particles with the time. */
    private float timeFromLastEmittedJetParticle;

    /** Amount of ammunition for SHOTGUN.
     *  Equals the number of shots that can be fired. */
    private int shotgunAmmo;
    //private int shotgunAmmoPerPack;

    /** Amount of energy for LASER.
     *  Equals the number of seconds for which the laser can be used. */
    private float laserEnergy;
    //private long laserEnergyPerPack;

    /** Currently used weapon.
     *  Can be GUN, SHOTGUN, or LASER */
    private int currentWeapon;

    /** Position where the generated bullets will appear.
     *  Relative to the shuttle's position. */
    private float gunMuzzleX;
    private float gunMuzzleY;

    /** Position where the generated particles of the jet fire will appear.
     *  Relative to the shuttle's position. */
    private float jetPositionsX[];
    private float jetPositionsY[];
    private int numberOfJets;

    /** Initial position of the shuttle.
     *  Here the shuttle will appear if a new game is started. */
    private float initialPositionX;
    private float initialPositionY;

    /** 2D vector model of the shuttle. */
	private static VectorImage shuttleModel = new VectorImage(Constants.DIRECTORY_WITH_MODELS + "shuttle.fig");

    /** The current number of shields of the shuttle.
     *  If the shuttle is hit and the number od shields is greater than zero
     *      then the number od shields is decreased by one.
     *  If the shuttle is hit and the number od shields is zero then the
     *      shuttle dies.
     *  The number of shields is increasable by picking the corresponding
     *  in-game bonuses. */
    private int shields = 0;

    /** Shuttle invulnerability time counter. 
     *  After the shuttle is hit by an alien and loses a shield, it becomes
     *      invulnerable for a short time. */
    private float timeCounterOfShortInvulnerability;
    /** The time period for which the shuttle is invulnerable after losing a shield. */
    private final static float SHORT_INVULNERABILITY_TIME = 1.0f; // in seconds

    /**
     * Creates a shuttle.
     */
    Shuttle(ListOfMovingObjects listOfShuttleBullets,
            ListOfMovingObjects listOfParticles,
            Random rand) {
        super((float) Constants.GAME_CANVAS_WIDTH  / 2.0f, (float) Constants.GAME_CANVAS_HEIGHT / 2.0f,
                0.0f, 0.0f, COLLISION_RADIUS,
                5.0f, 5.0f, 0.1f, // ... this seems to be suitable for the shuttle movement
                rand);

        this.listOfShuttleBullets = listOfShuttleBullets;
        this.listOfParticles = listOfParticles;

        initialPositionX = (float) Constants.GAME_CANVAS_WIDTH  / 2.0f;
        initialPositionY = (float) Constants.GAME_CANVAS_HEIGHT / 2.0f;

        clear();

        gunMuzzleX = collisionRadius;
        gunMuzzleY = 0.0f;

        numberOfJets = 1;
        jetPositionsX = new float[numberOfJets];
        jetPositionsY = new float[numberOfJets];
        jetPositionsX[0] = - 0.8f * collisionRadius;
        jetPositionsY[0] = 0.0f;

        timeFromLastEmittedJetParticle = 0.0f;
    }

    /** Resets the shuttle to start a new game.
     *  Cancels the death state, sets the position to the center of the game
     *      board, sets velocity to zero. */
    public void clear() {
        revive();
        setPosition(initialPositionX, initialPositionY);
        setVelocity(0.0f, 0.0f);
        shooting = false;
        currentWeapon = GUN;

        shotgunAmmo = 0;
        laserEnergy = 0.0f;

        shields = 0;

        setAttracted(false);
        // set initial angle of the shuttle to -45 degrees
        setPreviousVelocity(1.0f, -1.0f);

        timeCounterOfShortInvulnerability = 0.0f;
    }

    /** Makes the shuttle to start shooting bullets. */
    public void startShooting() {
        shooting = true;
    }

    /** Makes the shuttle to stop shooting bullets. */
    public void stopShooting() {
        shooting = false;
    }

    /** Fires one shot.
     *  Computes the direction of shooting and shoots a bullet.
     *  According to the current weapon type creates a new bullet (or a list of
     *      bullets) of the corresponding type.
     */
    public Bullet shootBullet(float deltaTime) {
        float absoluteVelocity = getAbsoluteVelocity();
        if (absoluteVelocity > 0.0f) {
            float bulletX = positionX + (gunMuzzleX * velocityX - gunMuzzleY * velocityY) / absoluteVelocity;
            float bulletY = positionY + (gunMuzzleX * velocityY + gunMuzzleY * velocityX) / absoluteVelocity;

            float normedVelocityX = velocityX / absoluteVelocity;
            float normedVelocityY = velocityY / absoluteVelocity;

            Bullet bullet = null;

            float bulletVelocityX;
            float bulletVelocityY;

            switch (currentWeapon) {
                case GUN:
                    bulletVelocityX = (GUN_BULLET_SPEED + absoluteVelocity) * normedVelocityX;
                    bulletVelocityY = (GUN_BULLET_SPEED + absoluteVelocity) * normedVelocityY;
                    bullet = new BulletShuttleGun(bulletX, bulletY, bulletVelocityX, bulletVelocityY, rand);
                    shooting = false;
                    break;
                case SHOTGUN:
                    bulletVelocityX = (SHOTGUN_BULLET_SPEED + absoluteVelocity) * normedVelocityX;
                    bulletVelocityY = (SHOTGUN_BULLET_SPEED + absoluteVelocity) * normedVelocityY;
                    Bullet nextBullet = null;
                    for(int i = 0; i < 24; ++i) {
                        bullet = new BulletShuttleShotgun(bulletX, bulletY,
                                bulletVelocityX + 400.0f * (rand.nextFloat() - 0.5f),
                                bulletVelocityY + 400.0f * (rand.nextFloat() - 0.5f),
                                rand);
                        bullet.setNext(nextBullet);
                        nextBullet = bullet;
                    }
                    decShotgunAmmo();
                    shooting = false;
                    break;
                case LASER:
                    bulletVelocityX = normedVelocityX;
                    bulletVelocityY = normedVelocityY;
                    bullet = new BulletShuttleLaser(bulletX, bulletY, bulletVelocityX, bulletVelocityY, rand);
                    decLaserEnergy(deltaTime);
                    break;
                default:
                    bullet = null;
            }
            return bullet;
        } else {
            return null;
        }
    }

    /** Adds an amount of ammunition for SHOTGUN.
     *  The amount of ammunition equals the number of shots that can be fired
     *      by SHOTGUN. */
    public void addShotgunAmmo(int amountOfAmmunition) {
        shotgunAmmo += amountOfAmmunition;
        if (currentWeapon == GUN) {
            currentWeapon = SHOTGUN;
        }
    }

    /** Adds an amount of energy for LASER.
     *  The amount of energy equals the number of seconds for which the laser
     *      can be used. */
    public void addLaserEnergy(float amountOfEnergy) {
        laserEnergy += amountOfEnergy;
        if (currentWeapon == GUN) {
            currentWeapon = LASER;
        }
    }

    /** Decreases the amount of ammunition for SHOTGUN by one shot. */
    public void decShotgunAmmo() {
        -- shotgunAmmo;
        if (shotgunAmmo <= 0) {
            shotgunAmmo = 0;
            setNextWeapon();
        }
    }

    /** Decreases the amount of ammunition for LASER by {@code deltaTime}. */
    public void decLaserEnergy(float deltaTime) {
        laserEnergy -= deltaTime;
        if (laserEnergy <= 0.0f) {
            laserEnergy = 0.0f;
            setNextWeapon();
        }
    }

    /** Returns the amount of the ammunition of SHOTGUN. */
    public int getShotgunAmmo() {
        return shotgunAmmo;
    }

    /** Returns the amount of the energy of LASER. */
    public float getLaserEnergy() {
        return laserEnergy;
    }

    /** Increases the number of shields by one. */
    public void addShield() {
        if(shields < 0) {
            shields = 0;
        }
        ++ shields;
    }

    /** Decreases the number of shields by one. 
     *  If the number of the shields is zero then the shuttle dies. */
    public void destroyShield() {
        if(shields == 0) {
            die();
            explode();
        } else {
            -- shields;
        }
    }

    /** Returns the current number of shields. */
    public int getShields() {
        return shields;
    }

    /** Selects the next weapon with non-empty ammo.
     *  Needed when changing weapons with mouse scroll. */
    public void setNextWeapon() {
        switch (currentWeapon) {
            case GUN:
                if (getShotgunAmmo() > 0) {
                    currentWeapon = SHOTGUN;
                    break;
                }
                if (getLaserEnergy() > 0.0f) {
                    currentWeapon = LASER;
                    break;
                }
            case SHOTGUN:
                if (getLaserEnergy() > 0.0f) {
                    currentWeapon = LASER;
                    break;
                }
                currentWeapon = GUN;
                break;
            case LASER:
                currentWeapon = GUN;
                break;
        }
    }

    /** Selects the previous weapon with non-empty ammo.
     *  Needed when changing weapons with mouse scroll. */
    public void setPreviousWeapon() {
        switch (currentWeapon) {
            case LASER:
                if (getShotgunAmmo() > 0.0f) {
                    currentWeapon = SHOTGUN;
                    break;
                }
                currentWeapon = GUN;
                break;
            case SHOTGUN:
                currentWeapon = GUN;
                break;
            case GUN:
                if (getLaserEnergy() > 0.0f) {
                    currentWeapon = LASER;
                    break;
                }
                if (getShotgunAmmo() > 0) {
                    currentWeapon = SHOTGUN;
                    break;
                }
        }
    }

    /** Returns the current weapon. */
    public int getCurrentWeapon() {
        return currentWeapon;
    }

    /** Creates explosion.
     *  This method generates a number of instance of {@code Particle} which
     *      simulates an explosion of the shuttle.
     *  @see    Particle */
    public void explode() {
        float angle;
        float speed;
        for(int i = 0; i < 2500; ++i) {
            angle = 2.0f * (float) Math.PI * rand.nextFloat();
            speed = 1.0f + 150.0f * rand.nextFloat();

            Color color;
            switch(rand.nextInt(50)) {
                case 0:
                    color = new Color(0x87, 0xCE, 0xFF); // LtBlue
                    break;
                case 1:
                    color = new Color(0x00, 0x00, 0x90); // Blue4
                    break;
                case 2:
                    color = new Color(0x00, 0x90, 0x90); // Cyan4
                    break;
                case 3:
                    color = new Color(0x00, 0xFF, 0xFF); // Cyan
                    break;
                default:
                    color = new Color(rand.nextInt(10)+245, rand.nextInt(255), 0);
                    break;
            }
            // make the initial radius of the explosion to correspond with the collision radius
            float norm = collisionRadius * rand.nextFloat();
            float particlePositionX = positionX + norm * (float) Math.cos(angle);
            float particlePositionY = positionY + norm * (float) Math.sin(angle);
            listOfParticles.add(new FadingParticle(particlePositionX, particlePositionY,
                        speed * (float) Math.cos(angle), speed * (float) Math.sin(angle),
                        1.0f + 3.0f * rand.nextFloat(),
                        color,
                        1000000L * rand.nextInt(2000) + 1000000000L,
                        rand));
        }
        // from time to time, add a pilot to the explosion (as one more particle)
        if(rand.nextInt(4) == 0) {
            angle = 2.0f * (float) Math.PI * rand.nextFloat();
            speed = 25.0f + 25.0f * rand.nextFloat();
            listOfParticles.add(new Pilot(positionX, positionY,
                        speed * (float) Math.cos(angle), speed * (float) Math.sin(angle),
                        rand));
        }
    }

    /** Creates a set of particles that simulate fire coming out of the shuttle's jets. */
    private void createJetFire(float deltaTime) {
        float absoluteVelocity = getAbsoluteVelocity();
        if(absoluteVelocity > 2.0f) {
            timeFromLastEmittedJetParticle += deltaTime;
            while (timeFromLastEmittedJetParticle >= JET_FIRE_PERIOD) {
                timeFromLastEmittedJetParticle -= JET_FIRE_PERIOD;
                // create a fire particle sper each jet
                for(int j = 0; j < numberOfJets; ++j) {
                    float particleX = positionX + (jetPositionsX[j] * velocityX - jetPositionsY[j] * velocityY) / absoluteVelocity;
                    float particleY = positionY + (jetPositionsX[j] * velocityY + jetPositionsY[j] * velocityX) / absoluteVelocity;
                    float speedModifier = 5.0f * rand.nextFloat();
                    float xModifier = 0.5f * absoluteVelocity * (rand.nextFloat() - 0.5f);
                    float yModifier = 0.5f * absoluteVelocity * (rand.nextFloat() - 0.5f);
                    listOfParticles.add(
                        new FadingParticle(
                            particleX, particleY,
                            - speedModifier * (velocityX + xModifier),
                            - speedModifier * (velocityY + yModifier),
                            1.0f + 3.0f * rand.nextFloat(),
                            new Color(rand.nextInt(10)+245, rand.nextInt(255), 0),
                            200000000L,
                            rand
                        )
                    );
                }
            }
        }
    }

    /** Manages collisions between the shuttle, the aliens and the bullets shot
     *      by the aliens.
     *  If there is such a collision, then the alien or the alien bullet is
     *      killed and the number of the shuttle shields is decreased by one.
     *  If the shields has no shields, then it dies and the game ends. */
    public void handleCollisions(ListOfAliens listOfAliens, ListOfMovingObjects listOfAlienBullets) {
        if (timeCounterOfShortInvulnerability <= 0.0f) {
            for (Alien alien = (Alien) listOfAliens.getFirst(); alien != null; alien = (Alien) alien.getNext()) {
                if (isCollision(alien)) {
                    destroyShield();
                    alien.die();
                    timeCounterOfShortInvulnerability = SHORT_INVULNERABILITY_TIME;
                    return;
                }
            }
            for (Bullet bullet = (Bullet) listOfAlienBullets.getFirst(); bullet != null; bullet = (Bullet) bullet.getNext()) {
                if (isCollision(bullet)) {
                    destroyShield();
                    bullet.die();
                    timeCounterOfShortInvulnerability = SHORT_INVULNERABILITY_TIME;
                    return;
                }
            }
        }
    }

    /** Moves the shuttle.
     *  If shooting is on then also a new bullet is added to the list of
     *      bullets. */
    public void move(float deltaTime) {
        if (!isDying()) {
            //  Moving of the shuttle is completely inherited from
            //      {@code AttractedMovingObject}.
            super.move(deltaTime);

            //  However, we need to add some particles to simulate the flame of
            //      the shuttle's jet.
            createJetFire(deltaTime);

            //  And, if demanded, a bullet.
            if(shooting) {
                listOfShuttleBullets.add(shootBullet(deltaTime));
            }

            //  Let the invulnerability time decrease.
            if (timeCounterOfShortInvulnerability > 0.0f) {
                timeCounterOfShortInvulnerability -= deltaTime;
            }
        }
    }

    /** Draws the shuttle. */
    public void draw(Graphics g) {
        if (!isDying()) {
            super.draw(g);
            if (getShields() > 0) {
                int col = getShields() * 50;
                if (col > 255) col = 255;
                g.setColor(new Color(col, col, col));
                VectorImage.drawCenteredOval(Math.round(positionX), Math.round(positionY), 55, 55, g);
            }
            if (velocityX == 0.0f && velocityY == 0.0f) {
                shuttleModel.rotateTranslateAndScale(positionX, positionY, 
                        previousVelocityX, previousVelocityY, Constants.FIG_IMAGE_SCALE);
            } else {
                shuttleModel.rotateTranslateAndScale(positionX, positionY, 
                        velocityX, velocityY, Constants.FIG_IMAGE_SCALE);
            }
            shuttleModel.draw(g);
        }
    }
};
