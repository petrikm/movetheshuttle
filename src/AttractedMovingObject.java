import java.awt.*;
import java.util.*;

// needed for unit test
import java.awt.event.*;
import java.awt.image.*;

/**
 *  Represents an abstract moving object which is attracted by another object
 *      (the attractor) situated on the game canvas.
 */
public abstract class AttractedMovingObject extends MovingObject {
    /** Multiplicative modifier of the instantaneous velocity vector. 
     *  @see move */
    protected float speedModifier;

    /** Describes, how quickly the attracted object reacts to a change of the
     *      coordinates of the attractor.
     *  @see updateVelocity */
    protected float accelerationModifier;

    /** Describes the effect of the drag ("fluid resistance") of the space.
     *  That is, how much the object will be continually slowed:
     *      the value {@code 1.0f} stands for no slowing; 
     *      if the value is lesser than {@code 1.0f} then the object will be
     *      continually slowed;
     *      if the value is greater than {@code 1.0f} then the object will be
     *      continually accelerated.
     *  It is a multiplicative modifier of the velocity vector describing the
     *      proportional change of the vector in 1 second.
     *  For example, if the velocity vector is (2.0f, 3.0f) and the resistance
     *      modifier is 0.5f then the velocity vector will become (1.0f, 1.5f)
     *      after 1 second if it is free from other influences.
     * @see updateVelocity */
    protected float resistanceModifier;

    /** Position of the attractor */
    protected float attractorX;
    protected float attractorY;

    /** Indicates whether the object is being attracted.
     *  If set to {@code false} then the object behaves as an instance of
     *      {@code MovingObject} (i.e. not attracted, just moving constantly).
     */
    protected boolean attracted;

    /** Last non-zero value of the velocity vector.
     *  Needed to rotate the shuttle if the velocity vector is zero. */
    protected float previousVelocityX;
    protected float previousVelocityY;

    /** 
     *  Constructor. 
     *
     *  @param  positionX               x-coordinate of the position
     *  @param  positionY               y-coordinate of the position
     *  @param  velocityX               x-coordinate of the velocity vector
     *  @param  velocityY               y-coordinate of the velocity vector
     *  @param  collisionRadius         serves to detect collisions with other moving objects
     *  @param  speedModifier           multiplicative modifier of the instantaneous velocity vector
     *  @param  accelerationModifier    how quickly the attracted object reacts to a change of the position of the attractor
     *  @param  resistanceModifier      how much the object will be continually slowed: {@code = 1.0f} for no slowing; {@code < 1.0f} for slowing; {@code > 1.0f} for continuall acceleration
     *  @param  rand                reference to a random number generator shared by all classes of the program
     */
    AttractedMovingObject(
            float positionX,
            float positionY,
            float velocityX,
            float velocityY,
            float collisionRadius,
            float speedModifier,
            float accelerationModifier,
            float resistanceModifier,
            Random rand) {
        super(positionX, positionY, velocityX, velocityY, collisionRadius, rand);
        this.speedModifier = speedModifier;
        this.accelerationModifier = accelerationModifier;
        this.resistanceModifier = resistanceModifier;

        attractorX = 0;
        attractorY = 0;
        attracted = true;

        setPreviousVelocity(1.0f, 0.0f);
    }

    /** Sets the position of the attractor. */
    public void setAttractorPosition(float newAttractorX, float newAttractorY) {
        this.attractorX = newAttractorX;
        this.attractorY = newAttractorY;
    }

    /** Sets whether the object shall be attracted.
     *  @param  attracted   if {@code true} then the attracted moving object
     *                      will be, from now on, attracted; otherwise it will
     *                      behave as a usual {@code MovingObject}
     */
    public void setAttracted(boolean attracted) {
        this.attracted = attracted;
    }

    /** Tests whether the attracted moving object is being attracted. */
    public boolean isAttracted() {
        return attracted;
    }

    /** Sets the vector that is normally equal to the previous value of the velocity vector.
     *  The direction of this vector is used to rotate the model when drawing
     *      it if the velocity vector is equal to zero (which means undefined
     *      rotation angle). */
    public void setPreviousVelocity(float previousVelocityX, float previousVelocityY) {
        this.previousVelocityX = previousVelocityX;
        this.previousVelocityY = previousVelocityY;
    }

    /**
     *  Updates the velocity vector of the attracted moving object.
     *
     *  The new value of the velocity vector is computed according to the
     *      attractor's position represented by the attributes {@code attractorX}
     *      and {@code attractorY}.
     */
    public void updateVelocity(float deltaTime) {
        if (attracted) {
            if (velocityX != 0.0f || velocityY != 0.0f) {
                previousVelocityX = velocityX;
                previousVelocityY = velocityY;
            }
            //  Compute the acceleration vector according as the difference
            //      vector between the position of the attractor and the position
            //      of this object.
            //  The acceleration vector is consequently modified (multiplied)
            //      by {@code accelerationModifier}.
            float accelerationX = attractorX - positionX;
            float accelerationY = attractorY - positionY;
            accelerationX *= accelerationModifier;
            accelerationY *= accelerationModifier;
            //  Add the acceleration vector to the velocity vector.
            velocityX += deltaTime * accelerationX;
            velocityY += deltaTime * accelerationY;
            //  Modify the velocity vector by the resistance modifier.
            velocityX *= Math.pow(resistanceModifier, deltaTime);
            velocityY *= Math.pow(resistanceModifier, deltaTime);
        }
    }

    /** Moves the attracted moving object. */
    public void move(float deltaTime) {
        updateVelocity(deltaTime);
        positionX += deltaTime * velocityX * speedModifier;
        positionY += deltaTime * velocityY * speedModifier;
    }

    /** Performs unit testing. */
    public static void main(String[] args) {
        UnitTestAttractedMovingObject unitTest = new UnitTestAttractedMovingObject();
        unitTest.createBuffer();
        unitTest.run();
    }
}

// ====================
//     UNIT TESTING
// ====================

/**
 *  A non-abstract version of the tested class to be able to perfrom the unit testing.
 */
class AttractedMovingObjectNonAbstract extends AttractedMovingObject {
    AttractedMovingObjectNonAbstract(
            float positionX,
            float positionY,
            float velocityX,
            float velocityY,
            float collisionRadius,
            float speedModifier,
            float accelerationModifier,
            float resistanceModifier,
            Random rand) {
        super(positionX, positionY, velocityX, velocityY, 
                collisionRadius, 
                speedModifier, accelerationModifier, resistanceModifier,
                rand);
    }
}

/**
 *  Implemets the unit testing.
 */
class UnitTestAttractedMovingObject {
    /** Geometry of the window */
    private Frame testFrame;
    private int testFrameWidth = 800;
    private int testFrameHeight = 600;

    /** Geometry of the canvas */
    private TestCanvas testCanvas;
    private int testCanvasWidth = 500;
    private int testCanvasHeight = 500;

    /** Instance of random number generator which all the classes share */
    private Random rand;    

    /** Instances of the tested class. */
    private AttractedMovingObjectNonAbstract movingObject;
    private MovingObjectNonAbstract staticObject;
    private MovingObjectNonAbstract objectMovedByMouse;
    private AttractedMovingObjectNonAbstract objectAttractedByMouse;

    /** Some of the objects will start to move after the mouse is clicked. */
    private boolean waitingForTheFirstClick;

    /** Initializes a simple frame with a canvas and creates an instance of {@code Background}. */
    UnitTestAttractedMovingObject() {
        rand = new Random();

        testFrame = new Frame();
        testFrame.setSize(testFrameWidth, testFrameHeight);
        testFrame.setLocation(100, 100);
        testFrame.addWindowListener(new FrameWA());
        testFrame.setVisible(true);
        testFrame.setResizable(true);

        Panel panel = new Panel();
        panel.setLayout(new FlowLayout());
        panel.setBackground(new Color(50, 0, 50));
        testFrame.add(panel);

        testCanvas = new TestCanvas();
        testCanvas.setSize(testCanvasWidth, testCanvasHeight);
        testCanvas.addMouseMotionListener(new MouseMovement());
        testCanvas.addMouseListener(new MouseClicking());
        panel.add(testCanvas);

        staticObject = new MovingObjectNonAbstract(200.0f, 200.0f, 0.0f, 0.0f, 2.0f, rand);
        movingObject = new AttractedMovingObjectNonAbstract(200.0f, 50.0f, 190.0f, 0.0f, 10.0f, 
                1.0f, 1.04f, 0.9f, // ... this seems to be suitable for the alien movement
                rand);
        movingObject.setAttractorPosition(200.0f, 200.0f);
        movingObject.setAttracted(true);

        objectMovedByMouse = new MovingObjectNonAbstract(2.0f, 2.0f, 0.0f, 0.0f, 10.0f, rand);
        objectAttractedByMouse = new AttractedMovingObjectNonAbstract(300.0f, 200.0f, 0.0f, 0.0f, 10.0f, 
                4.0f, 4.0f, 0.5f, // ... this seems to be suitable for the shuttle movement
                rand);
        objectAttractedByMouse.setAttracted(false);

        waitingForTheFirstClick = true;
    }

    /** The main loop. */
    public void run() {
        float deltaTime = 0.0f;
        long nanoDeltaTime = 0;
        long previousTime = 0;
        long currentTime = System.nanoTime();

        while(testCanvas.isVisible()) {
            previousTime = currentTime;
            currentTime = System.nanoTime();
            nanoDeltaTime = currentTime - previousTime;

            // keep the "frames per second" rate under 50
            if (nanoDeltaTime < 20000000L) {
                try {
                    Thread.sleep((20000000L - nanoDeltaTime) / 1000000L);
                } catch(InterruptedException e) {}
                currentTime = System.nanoTime();
                nanoDeltaTime = currentTime - previousTime;
            }

            deltaTime = (float) ((double) nanoDeltaTime / 1000000000.0);

            movingObject.move(deltaTime);
            staticObject.move(deltaTime);

            objectAttractedByMouse.move(deltaTime);

            testCanvas.draw();
        }
    }

    /** Creates a buffer for the double-buffered graphics. */
    public void createBuffer() {
        testCanvas.createBufferStrategy(2);
        testCanvas.strategy = testCanvas.getBufferStrategy();
    }

    /** Allows the frame to be closed. */
    private class FrameWA extends WindowAdapter {
        public void windowClosing(WindowEvent e) {
            System.exit(1);
        }
    }

    /** Displays the graphics. */
    class TestCanvas extends Canvas {
        public BufferStrategy strategy;
        public void draw() {
            Graphics g = strategy.getDrawGraphics();

            g.setColor(new Color(0, 0, 50));
            g.fillRect(0, 0, testCanvasWidth, testCanvasHeight);

            movingObject.draw(g);
            staticObject.draw(g);
            objectMovedByMouse.draw(g);
            objectAttractedByMouse.draw(g);

            g.dispose();
            strategy.show();
        }
    }

    /**
     *  Handles the movement of the mouse cursor.
     */
    class MouseMovement extends MouseMotionAdapter {
        public void mouseMoved(MouseEvent e) {
            objectMovedByMouse.setPosition((float) e.getX(), (float) e.getY());
            objectAttractedByMouse.setAttractorPosition((float) e.getX(), (float) e.getY());
        }
        public void mouseDragged(MouseEvent e) {
        }
    }

    /**
     *  Handles the mouse clicking.
     */
    class MouseClicking extends MouseAdapter {
        public void mousePressed(MouseEvent e) {
            if(waitingForTheFirstClick) {
                waitingForTheFirstClick = false;
                objectAttractedByMouse.setAttracted(true);
            }
        }
        public void mouseReleased(MouseEvent e) {
        }
    }
}
