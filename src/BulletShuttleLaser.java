import java.awt.*;
import java.util.*;

// needed for unit test
import java.awt.event.*;
import java.awt.image.*;

/**
 * Represents the laser beam fired by the shuttle.
 */
public class BulletShuttleLaser extends Bullet {

    /**
     *  Creates a new laser beam given position and velocity vector.
     *
     *  The velocity vector determines the direction of the beam.
     *
     *  @param  positionX   x-coordinate of the position
     *  @param  positionY   y-coordinate of the position
     *  @param  velocityX   x-coordinate of the velocity vector
     *  @param  velocityY   y-coordinate of the velocity vector
     *  @param  rand        reference to a random number generator shared by all classes of the program
     */
    public BulletShuttleLaser(float positionX, float positionY, float velocityX, float velocityY, Random rand) {
        super(positionX, positionY, velocityX, velocityY, 1.0f, rand);
    }

    /** Tests whether there is a collision between the laser beam and other
     *      moving object.
     *  This tests differs from the standard test defined in
     *      {@code MovingObject}.
     *  A general moving object is approximated by a point a by a circle for
     *      the purpose of the collision detection.  
     *  A laser beam is approximated by a line. 
     *  Hence the method computes the distance of the center of the object from
     *      the beam line and if it is less than the collision radius of the
     *      object, {@code true} is returned. */
    public boolean isCollision(MovingObject object) {
        boolean inFrontOfMuzzle = 
            Utils.sgnDet(velocityY, -velocityX, object.getX() - positionX, object.getY() - positionY) > 0;
        if (inFrontOfMuzzle) {
            float distanceFromBeam = Utils.computeDistanceFromLine(object.getX(), object.getY(),
                    positionX, positionY, velocityX, velocityY);
            if (distanceFromBeam < object.getCollisionRadius()) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /** Moves the laser beam.
     *  Actually the beam exists only in one step of the main program loop.
     *  Therefore this method does nothing but destroys the laser beam. */
    public void move(float deltaTime) {
        die();
    }

    /** Draws the laser beam. */
    public void draw(Graphics g) {
        if (! isDying()) {
            float howFar = Constants.GAME_CANVAS_WIDTH + Constants.GAME_CANVAS_HEIGHT;
            float toX = positionX + howFar * velocityX;
            float toY = positionY + howFar * velocityY;
            g.setColor(Color.RED);
            g.drawLine(Math.round(positionX), Math.round(positionY), Math.round(toX), Math.round(toY));
        }
    }

    /** Performs unit testing. */
    public static void main(String[] args) {
        UnitTestBulletShuttleLaser unitTest = new UnitTestBulletShuttleLaser();
        unitTest.createBuffer();
        unitTest.run();
    }
}

// ====================
//     UNIT TESTING
// ====================

/**
 *  Implemets a unit test if the class {@code Background}.
 *
 *  Opens a frame with a canvas with an instance of {@code Background}.
 */
class UnitTestBulletShuttleLaser {
    /** Geometry of the window */
    private Frame testFrame;
    private int testFrameWidth = 800;
    private int testFrameHeight = 600;

    /** Geometry of the canvas */
    private TestCanvas testCanvas;
    private int testCanvasWidth = testFrameWidth - 100;
    private int testCanvasHeight = testFrameHeight - 100;

    /** instance of random number generator which all the classes share */
    private Random rand;    

    private BulletShuttleLaser bullet;

    /** Initializes a simple frame with a canvas and creates an instance of {@code Background}. */
    UnitTestBulletShuttleLaser() {
        rand = new Random();

        testFrame = new Frame();
        testFrame.setSize(testFrameWidth, testFrameHeight);
        testFrame.setLocation(100, 100);
        testFrame.addWindowListener(new FrameWA());
        testFrame.setVisible(true);
        testFrame.setResizable(true);

        Panel panel = new Panel();
        panel.setLayout(new FlowLayout());
        panel.setBackground(new Color(50, 0, 50));
        testFrame.add(panel);

        testCanvas = new TestCanvas();
        testCanvas.setSize(testCanvasWidth, testCanvasHeight);
        panel.add(testCanvas);

        bullet = new BulletShuttleLaser(0.1f * testCanvasWidth, 0.1f * testCanvasHeight, 
                0.1f * testCanvasWidth, 0.1f * testCanvasHeight, rand);
    }

    /** The main loop. */
    public void run() {
        float deltaTime = 0.0f;
        long nanoDeltaTime = 0;
        long previousTime = 0;
        long currentTime = System.nanoTime();

        while(testCanvas.isVisible()) {
            previousTime = currentTime;
            currentTime = System.nanoTime();
            nanoDeltaTime = currentTime - previousTime;

            // keep frames per second rate at maximum of 50
            if (nanoDeltaTime < 20000000L) {
                try {
                    Thread.sleep((20000000L - nanoDeltaTime) / 1000000L);
                } catch(InterruptedException e) {}
                currentTime = System.nanoTime();
                nanoDeltaTime = currentTime - previousTime;
            }
            deltaTime = (float) ((double) nanoDeltaTime / 1000000000.0);

            testCanvas.draw();
        }
    }

    /** Creates a buffer for the double-buffered graphics. */
    public void createBuffer() {
        testCanvas.createBufferStrategy(2);
        testCanvas.strategy = testCanvas.getBufferStrategy();
    }

    /** Allows the frame to be closed. */
    private class FrameWA extends WindowAdapter {
        public void windowClosing(WindowEvent e) {
            System.exit(1);
        }
    }

    /** Displays the graphics. */
    class TestCanvas extends Canvas {
        public BufferStrategy strategy;
        public void draw() {
            Graphics g = strategy.getDrawGraphics();

            g.setColor(new Color(0, 0, 50));
            g.fillRect(0, 0, testCanvasWidth, testCanvasHeight);

            bullet.draw(g);

            g.dispose();
            strategy.show();
        }
    }
}
