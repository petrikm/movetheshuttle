import java.awt.*;
import java.util.*;

// needed for unit test
import java.awt.event.*;
import java.awt.image.*;

/**
 * Represents the basic bullet fired by the shuttle.
 */
public class BulletShuttleGun extends Bullet {
   
    /** Position of the bullet in the previous loop step.
     *  Needed to compute the collisions with the aliens. */
    private float previousPositionX;
    private float previousPositionY;

    /**
     *  Creates a new basic bullet shot by the shuttle.
     *
     *  @param  positionX   x-coordinate of the position
     *  @param  positionY   y-coordinate of the position
     *  @param  velocityX   x-coordinate of the velocity vector
     *  @param  velocityY   y-coordinate of the velocity vector
     *  @param  rand        reference to a random number generator shared by all classes of the program
     */
    public BulletShuttleGun(float positionX, float positionY, float velocityX, float velocityY, Random rand) {
        super(positionX, positionY, velocityX, velocityY, 2.0f, rand);
        previousPositionX = positionX;
        previousPositionY = positionY;
    }

    /**
     *  Checks whether there is a collision with another moving object. 
     */
    public boolean isCollision(MovingObject object) {
        float objectX = object.getX();
        float objectY = object.getY();
        int behinHead = Utils.sgnDet(objectX - positionX, objectY - positionY, velocityY, -velocityX);
        int inFrontOfTail = Utils.sgnDet(objectX - previousPositionX, objectY - previousPositionY, velocityY, -velocityX);
        float distanceFromBullet = Utils.computeDistanceFromLine(objectX, objectY, positionX, positionY, velocityX, velocityY);
        return (behinHead != inFrontOfTail) && (distanceFromBullet <= object.getCollisionRadius());
    }

    /** Moves the bullet. */
    public void move(float deltaTime) {
        previousPositionX = positionX;
        previousPositionY = positionY;
        super.move(deltaTime);
    }
    /** Draws the bullet. 
     *  The bullet is drawn as a line segment. */
    public void draw(Graphics g) {
        float length = 50.0f; // length of the line segment that represents the flying bullet
        int numSegments = 10; // number of segments - to interpolate the color of the bullet from white to black
        float absoluteVelocity = getAbsoluteVelocity();
        float normedVelocityX = velocityX / absoluteVelocity;
        float normedVelocityY = velocityY / absoluteVelocity;
        float tailX = positionX - length * normedVelocityX;
        float tailY = positionY - length * normedVelocityY;
        float dx = tailX - positionX;
        float dy = tailY - positionY;
        int x1 = Math.round(positionX);
        int y1 = Math.round(positionY);
        int x2;
        int y2;
        int col;
        for (int i = 0; i < numSegments; ++i) {
            x2 = Math.round(positionX + (dx * (float) (i + 1)) / (float) numSegments);
            y2 = Math.round(positionY + (dy * (float) (i + 1)) / (float) numSegments);
            col = (numSegments - i) * 255 / numSegments;
            g.setColor(new Color(col, col, col));
            g.drawLine(x1, y1, x2, y2);
            x1 = x2;
            y1 = y2;
        }
    }

    /** Performs unit testing. */
    public static void main(String[] args) {
        UnitTestBulletShuttleGun unitTest = new UnitTestBulletShuttleGun();
        unitTest.createBuffer();
        unitTest.run();
    }
}

// ====================
//     UNIT TESTING
// ====================

/**
 *  Implemets a unit test if the class {@code Background}.
 *
 *  Opens a frame with a canvas with an instance of {@code Background}.
 */
class UnitTestBulletShuttleGun {
    /** Geometry of the window */
    private Frame testFrame;
    private int testFrameWidth = 800;
    private int testFrameHeight = 600;

    /** Geometry of the canvas */
    private TestCanvas testCanvas;
    private int testCanvasWidth = testFrameWidth;
    private int testCanvasHeight = testFrameHeight - 100;

    /** instance of random number generator which all the classes share */
    private Random rand;    

    private BulletShuttleGun bullet;

    /** Initializes a simple frame with a canvas and creates an instance of {@code Background}. */
    UnitTestBulletShuttleGun() {
        rand = new Random();

        testFrame = new Frame();
        testFrame.setSize(testFrameWidth, testFrameHeight);
        testFrame.setLocation(100, 100);
        testFrame.addWindowListener(new FrameWA());
        testFrame.setVisible(true);
        testFrame.setResizable(true);

        Panel panel = new Panel();
        panel.setLayout(new FlowLayout());
        panel.setBackground(new Color(50, 0, 50));
        testFrame.add(panel);

        testCanvas = new TestCanvas();
        testCanvas.setSize(testCanvasWidth, testCanvasHeight);
        panel.add(testCanvas);

        bullet = new BulletShuttleGun(1.0f, 1.0f, 0.1f * testCanvasWidth, 0.1f * testCanvasHeight, rand);
    }

    /** The main loop. */
    public void run() {
        float deltaTime = 0.0f;
        long nanoDeltaTime = 0;
        long previousTime = 0;
        long currentTime = System.nanoTime();

        while(testCanvas.isVisible()) {
            previousTime = currentTime;
            currentTime = System.nanoTime();
            nanoDeltaTime = currentTime - previousTime;

            // keep frames per second rate at maximum of 50
            if (nanoDeltaTime < 20000000L) {
                try {
                    Thread.sleep((20000000L - nanoDeltaTime) / 1000000L);
                } catch(InterruptedException e) {}
                currentTime = System.nanoTime();
                nanoDeltaTime = currentTime - previousTime;
            }

            deltaTime = (float) ((double) nanoDeltaTime / 1000000000.0);

            System.out.println("deltaTime: " + deltaTime + " FPS: " + 1 / deltaTime);

            bullet.move(deltaTime);

            testCanvas.draw();
        }
    }

    /** Creates a buffer for the double-buffered graphics. */
    public void createBuffer() {
        testCanvas.createBufferStrategy(2);
        testCanvas.strategy = testCanvas.getBufferStrategy();
    }

    /** Allows the frame to be closed. */
    private class FrameWA extends WindowAdapter {
        public void windowClosing(WindowEvent e) {
            System.exit(1);
        }
    }

    /** Displays the graphics. */
    class TestCanvas extends Canvas {
        public BufferStrategy strategy;
        public void draw() {
            Graphics g = strategy.getDrawGraphics();

            g.setColor(new Color(0, 0, 50));
            g.fillRect(0, 0, testCanvasWidth, testCanvasHeight);

            bullet.draw(g);

            g.dispose();
            strategy.show();
        }
    }
}
