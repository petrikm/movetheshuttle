import java.io.*;

/** 
 *  Implements reading of a white-space-separated text data file.
 *
 *  The text data file is supposed to contain integer, float, and string values
 *      separated by white characters (' ', '\t', and '\n').
 *
 *  Integer is defined as a sequence of the characters from the set 
 *      '0', ..., '9'
 *      possibly preceded by '-'.
 *  Float is defined as a sequence of the characters from the set 
 *      '0', ..., '9'
 *      possibly preceded by '-' and containing exactly one '.'.
 *  String is defined as a sequence of the characters from the set 
 *      'a' ... 'z', 'A' ... 'Z', '0' ... '9'
 *      starting by a non-digit character (there are no quotes used).
 *
 *  The text data file may contain comments: everything that begins by '#' is
 *      ignored until the end of the line.
 */
public class TextDataFile {

    /** Types of data that can be parsed. */
    public enum ParsedType {
        EOF,
        UNKNOWN,
        INT,
        FLOAT,
        STRING 
    }

    /** The content of the file is loaded to an integer stream which is then
     *      used to parse the data. */
    private InputStream stream;

    /** Current number of the line where the file is being parsed.
     *  To report parse error messages. */
    private int lineNumber;

    /** Information about the currently parsed value. */
    ParsedType parsedType;
    int parsedInt;
    float parsedFloat;
    String parsedString;
    int parsedSignum;

    /** Opens the parsed file.
     *  @param  path    path to the file */
    TextDataFile(String path) throws FileNotFoundException {
        stream = getClass().getResourceAsStream(path);
        if (stream == null) {
            throw new FileNotFoundException("ERROR in TextDataFile: CANNOT FIND FILE " + path);
        }
        lineNumber = 0;
        parsedType = ParsedType.UNKNOWN;
        parsedInt = 0;
        parsedFloat = 0.0f;
        parsedString = "";
        parsedSignum = 1;
    }

    /** Returns the number of the line in the parsed '.fig' file.
     *  To report errors while parsing the '.fig' file. */
	public int getLineNumber() {
		return lineNumber;
	}

    /** Reads next character from the file while skipping comments that start by '#'. 
     *  Furthermore, increases {@code lineNumber} if the character is '\n'. */
    private int readNextChar() throws IOException {
        int value = stream.read();
        if (value == '#') {
            while (value > 0 && value != '\n') {
                value = stream.read();
            }
        }
        if (value == '\n') {
            ++ lineNumber;
        }
        return value;
    }

    /** Reads and stores the next value in the file (or stream, actually).
     *  The value can be of one of the types given by {@code ParsedType}.
     *  Adter running this method, the type and the value can be obtained by
     *      calling the corresponding methods.
     *  @see    getParsedType
     *  @see    getParsedInt
     *  @see    getParsedFloat
     *  @see    getParsedString */
    public void parseNextValue() throws IOException, TextDataFile.ParseError {
        int value = readNextChar();
        // skip white chars
        while (value == ' ' || value == '\t' || value == '\n') {
            value = readNextChar();
        }

        parsedType = ParsedType.UNKNOWN;
        parsedInt = 0;
        parsedFloat = 0.0f;
        parsedString = "";
        parsedSignum = 1;

        if (value < 0) {
            // end of the file reached
            parsedType = ParsedType.EOF;
            return;
        } else if (Character.isDigit(value) || value == '-') {
            // managing negative signum
            if (value == '-') {
                parsedSignum = -1;
                value = readNextChar();
            }
            // parsing an integer
            parsedType = ParsedType.INT;
            while (Character.isDigit(value)) {
                parsedInt = 10 * parsedInt + Integer.valueOf("" + (char) value).intValue();
                value = readNextChar();
            }
            // change integer to float
            if (value == '.') {
                parsedType = ParsedType.FLOAT;
                parsedFloat = (float) parsedInt;
                value = readNextChar();
                float base = 0.1f;
                while (Character.isDigit(value)) {
                    parsedInt = 10 * parsedInt + Integer.valueOf("" + (char) value).intValue();
					parsedFloat += base * Integer.valueOf("" + (char) value).floatValue();
					base /= 10.0f;
                    value = readNextChar();
                }
            }
            if (parsedSignum < 0) {
                parsedInt *= -1;
                parsedFloat *= -1.0f;
            }
        } else if (Character.isLetter(value)) {
            // parsing a string
            parsedType = ParsedType.STRING;
            parsedString = "";
            while (Character.isLetterOrDigit(value)) {
                parsedString += (char) value;
                value = readNextChar();
            }
        }

        // all the text elements should be separated by white chars
        if (value >= 0 && value != ' ' && value != '\t' && value != '\n') {
            throw new ParseError("NON-WHITE CHARACTER " + (char) value + " (" + value + ")");
        }
    }

    /** Returns the type of the last parsed value.
     *  @see    ParsedType */
    public ParsedType getParsedType() {
        return parsedType;
    }

    /** Returns the last parsed value if it is INT.
     *  @see    ParsedType */
    public int getParsedInt() {
        return parsedInt;
    }

    /** Returns the last parsed value if it is FLOAT.
     *  @see    ParsedType */
    public float getParsedFloat() {
        return parsedFloat;
    }

    /** Returns the last parsed value if it is STRING.
     *  @see    ParsedType */
    public String getParsedString() {
        return parsedString;
    }

    /** Returns a string that describes the last parsed value. 
     *  To report parse error messages. */
    public String reportParsedValue() {
        switch (getParsedType()) {
            case EOF:
                return "EOF";
            case INT:
                return "INT (" + getParsedInt() + ")";
            case FLOAT:
                return "FLOAT (" + getParsedInt() + ")";
            case STRING:
                return "STRING (" + getParsedInt() + ")";
            default:
                return "UNKNOWN TYPE";
        }
    }

    /** Reads and returns the next integer value from the stream.
     *  If the value is not {@code INT}, {@code ParseError} is thrown.
     *  @see    getParsedType */
    public int parseNextInteger() throws IOException, TextDataFile.ParseError, TextDataFile.EndOfFile {
        parseNextValue();
        switch (getParsedType()) {
            case INT:
                return getParsedInt();
            case EOF:
                throw new EndOfFile();
            default:
                throw new ParseError("NON-INTEGER VALUE: " + reportParsedValue());
        }
    }

    /** Reads and returns the next float value from the stream.
     *  If the value is not {@code FLOAT}, {@code ParseError} is thrown.
     *  @see    getParsedType */
    public int parseNextFloat() throws IOException, TextDataFile.ParseError, TextDataFile.EndOfFile {
        parseNextValue();
        if (getParsedType() == ParsedType.FLOAT) {
            return getParsedInt();
        } else {
            throw new ParseError("NON-FLOAT VALUE: " + reportParsedValue());
        }
    }

    /** Reads and returns the next string value from the stream.
     *  If the value is not {@code STRING}, {@code ParseError} is thrown.
     *  @see    getParsedType */
    public int parseNextString() throws IOException, TextDataFile.ParseError, TextDataFile.EndOfFile {
        parseNextValue();
        if (getParsedType() == ParsedType.STRING) {
            return getParsedInt();
        } else {
            throw new ParseError("NON-STRING VALUE: " + reportParsedValue());
        }
    }

    // ==================
    //     EXCEPTIONS
    // ==================

    /**
     *  Implements en exceprion which handles the errors that may occur when
     *      parsing the text data file.
     */
    public class ParseError extends Exception {
        ParseError(String msg) {
            super("PARSE ERROR ON LINE " + lineNumber + ": " + msg);
        }
    }

    /**
     *  Thrown out when the end of the file has been reached.
     */
    public class EndOfFile extends Exception {
        EndOfFile() {
            super();
        }
    }

    // =================
    //     UNIT TEST
    // =================

    /** Performs unit testing. */
    public static void main(String[] args) 
            throws FileNotFoundException, IOException, TextDataFile.ParseError {
        TextDataFile inputFile = new TextDataFile(Constants.DIRECTORY_WITH_MODELS + "bonus_shotgun.fig");
        boolean finished = false;
        while (! finished) {
            inputFile.parseNextValue();
            switch (inputFile.getParsedType()) {
                case EOF:
                    System.out.println("EOF");
                    finished = true;
                    break;
                case INT:
                    System.out.println("INT: " + inputFile.getParsedInt());
                    break;
                case FLOAT:
                    System.out.println("FLOAT: " + inputFile.getParsedFloat());
                    break;
                case STRING:
                    System.out.println("STRING: " + inputFile.getParsedString());
                    break;
                default:
                    System.out.println("UNKNOWN TYPE: " + inputFile.getParsedType());
                    finished = true;
            }
        }
    }
}
