/**
 *  Represents an atom in a list of (x,y)-coordinates.
 *
 *  This class is used by {@code VectorImage} when creating a vector image from a '.fig' file.
 *
 *  @see    VectorImage
 *  @see    FileFIG
 */
public class AtomCoordinate {
    private int x;
    private int y;

    /** Reference to the successor of this atom.
     *  It is equal to {@code null} if this is the last atom of the list. */
    private AtomCoordinate next;

    /** Constructor.
     *  @param  x       x-coordinate
     *  @param  y       y-coordinate
     *  @param  next    the successor of this atom; set it to {@code null} if this is the last atom of the list
     */
    AtomCoordinate(int x, int y, AtomCoordinate next) {
        this.x = x;
        this.y = y;
        this.next = next;
    }

//    /**
//     * Sets the x-coordinate.
//     *
//     * @param x      x-coordinate
//     */
//    public void setX(int x) {
//        this.x = x;
//    }
//
//    /**
//     * Sets the y-coordinate.
//     *
//     * @param y      y-coordinate
//     */
//    public void setY(int y) {
//        this.y = y;
//    }
//
    /** Sets the successor of this atom. */
    public void setNext(AtomCoordinate next) {
        this.next = next;
    }

    /** Returns the x-coordinate. */
    public int getX() {
        return x;
    }

    /** Returns the y-coordinate. */
    public int getY() {
        return y;
    }

    /** Returns the reference to the successor of this atom. */
    public AtomCoordinate getNext() {
        return next;
    }

    /** Returns a string containing the (x,y)-coordinates. */
    public String toString() {
        return "(" + x + "," + y + ")";
    }
}
