import java.awt.*;
import java.util.*;

/**
 *  An abstract class representing a bonus item which, when caught by
 *      the shutlle, adds the player some advantages.
 */
public class Bonus extends RotatingMovingObject {

    /** Image of the bonus item. */
	protected VectorImage img;

    /** Collision radius shared by all the bonus items. */
    public static final float COLLISION_RADIUS = 10.0f;

    /** Speed of the bonus item is chosen randomly according to the following range. */
    public static final float MINIMAL_SPEED = Constants.GAME_CANVAS_DIAGONAL / 20.0f;
    public static final float MAXIMAL_SPEED = Constants.GAME_CANVAS_DIAGONAL / 5.0f;

    /** Creates a new bonus item with given position and velocity and with
     *      random rotation speed.
     *
     *  @param  positionX           x-coordinate of the position
     *  @param  positionY           y-coordinate of the position
     *  @param  velocityX           x-coordinate of the velocity vector
     *  @param  velocityY           y-coordinate of the velocity vector
     *  @param  rand                reference to a random number generator shared by all classes of the program
     */
    Bonus(float positionX, float positionY,
            float velocityX, float velocityY,
            Random rand) {
        super(positionX, positionY, velocityX, velocityY, COLLISION_RADIUS, rand);
        img = null;
	}

    /** Creates a new bonus item with random position and velocity outside the board.
     *  @param rand reference to a random number generator shared by all classes of the program
     */
    Bonus(Random rand) {
        super(0.0f, 0.0f, 0.0f, 0.0f, COLLISION_RADIUS, rand);
        // randomly choose position of the new bonus item behind the border of the game canvas
		switch (rand.nextInt(4)) {
			case 0: // upper border
                this.positionX = (float) rand.nextInt(Constants.GAME_CANVAS_WIDTH);
				//positionY = - 2.0f * COLLISION_RADIUS;
				this.positionY = - COLLISION_RADIUS;
				break;
			case 1: // right border
                //positionX = (float) Constants.GAME_CANVAS_WIDTH + 2.0f * COLLISION_RADIUS;
                this.positionX = (float) Constants.GAME_CANVAS_WIDTH + COLLISION_RADIUS;
				this.positionY = (float) rand.nextInt(Constants.GAME_CANVAS_HEIGHT);
				break;
			case 2: // bottom border
                this.positionX = (float) rand.nextInt(Constants.GAME_CANVAS_WIDTH);
				//positionY = (float) Constants.GAME_CANVAS_HEIGHT + 2.0f * COLLISION_RADIUS;
				this.positionY = (float) Constants.GAME_CANVAS_HEIGHT + COLLISION_RADIUS;
				break;
			case 3: // left border
                //positionX = - 2.0f * COLLISION_RADIUS;
                this.positionX = - COLLISION_RADIUS;
				this.positionY = (float) rand.nextInt(Constants.GAME_CANVAS_HEIGHT);
				break;
			default:
                this.positionX = (float) Constants.GAME_CANVAS_WIDTH / 2.0f;
				this.positionY = (float) Constants.GAME_CANVAS_HEIGHT / 2.0f;
		}
        // direct the velocity vector to the center of the game canvas
        this.velocityX = Constants.GAME_CANVAS_WIDTH / 2.0f - positionX;
        this.velocityY = Constants.GAME_CANVAS_HEIGHT / 2.0f - positionY;
        // norm the velocity vector
        //float length = (float) Math.sqrt(velocityX * velocityX + velocityY * velocityY);
        float absoluteVelocity = getAbsoluteVelocity();
        velocityX /= absoluteVelocity;
        velocityY /= absoluteVelocity;
        // choose random speed
        float speed = MINIMAL_SPEED + (MAXIMAL_SPEED - MINIMAL_SPEED) * rand.nextFloat();
        velocityX *= speed;
        velocityY *= speed;
	}

    /** Manages collision between the bonus item and the shuttle.
     *  If there is such a collision, the bonus item is destroyed and an
     *      advatage is given to the player.
     *  The implementation of the advatage has to be done in the child classes
     *      (BonusLaser, BonusShield, ...). */
	public void handleCollisionWithShuttle(Shuttle shuttle) {
		if (isCollision(shuttle)) {
            die();
        }
    }

    /** Moves the bonus item reflecting its velocity vector, rotation speed,
     *      and the given time interval.
     *  @param  deltaTime   time interval according to which the new position of the moving object is computed */
    public void move(float deltaTime) {
        super.move(deltaTime);
        if (isOutOfGameCanvas()) die();
    }

    /** Draws the bonus item.
     *  The method draws an empty circle which encircles all the bonus items.
     *  Drawing of the images has to be implemented in the child classes */
	public void draw(Graphics g) {
        g.setColor(Color.GRAY);
        VectorImage.drawCenteredOval(Math.round(positionX), Math.round(positionY), diameter, diameter, g);
        if (img != null) {
            img.rotateTranslateAndScale(positionX, positionY, 
                    directionX, directionY, Constants.FIG_IMAGE_SCALE);
            img.draw(g);
        }
	}

}
