import java.awt.*;
import java.util.*;

/**
 *  Implements a list of aliens.
 *
 *  Handles collisions between the aliens and the shuttle, between the aliens
 *      and the shuttle bullets, between aliens and other aliens.
 *  Implements a method which sets the position of the attractor of the aliens.
 *
 *  @see    ListOfMovingObjects
 */
public class ListOfAliens extends ListOfMovingObjects {
    /** Externally created lists of particles. */
    private ListOfMovingObjects listOfParticles;

    /** Externally created list of alien bullets.
     *  When the alien decides to make a shot, it simply adds a bullet to this
     *      list. */
	private ListOfMovingObjects listOfAlienBullets;

    /** Externally created list of bullets shot by the shuttle. */
	private ListOfMovingObjects listOfShuttleBullets;

    /** At which value of the player's score the aliens of the type 
     *      {@code AlienShooter} will start to spawn. */
    public final static int MILESTONE_SHOOTER = 50;

    /** At which value of the player's score the aliens of the type 
     *      {@code AlienCloner} will start to spawn. */
    public final static int MILESTONE_CLONER = 100;

    private float timeToSpawnNextAlien;

    private int maxNumberOfAliens;

    /** Reference to a random number generator shared by all classes of the program */
    protected Random rand;

    /** Creates an empty list. 
     *
     *  @param  listOfParticles     externally created list of particles
     *  @param  listOfAlienBullets  externally created list of alien bullets
     *  @param  rand                reference to a random number generator shared by all classes of the program */
    ListOfAliens(ListOfMovingObjects listOfParticles,
            ListOfMovingObjects listOfAlienBullets,
            ListOfMovingObjects listOfShuttleBullets,
            Random rand) {
        super();
        this.listOfParticles = listOfParticles;
        this.listOfAlienBullets = listOfAlienBullets;
        this.listOfShuttleBullets = listOfShuttleBullets;
        this.rand = rand;
        clear();
    }

    /** Clears the list making it an empty list. */
    public void clear() {
        super.clear();
        timeToSpawnNextAlien = 1.0f;
        maxNumberOfAliens = 50;
    }

    /** Sets the position of the attractor of all the aliens in the list. */
    public void setAttractorPosition(float newAttractorX, float newAttractorY) {
        for (Alien iter = (Alien) getFirst(); iter != null; iter = (Alien) iter.getNext()) {
            iter.setAttractorPosition(newAttractorX, newAttractorY);
        }
    }

    /** Creates a new alien of random type and position. 
     *  A random value {@code ticket} is taken from the interval [0, 1].
     *  If {@code 0.0f <= ticket <= a} then an instance of {@code Alien} is created.
     *  If {@code a < ticket <= b} then an instance of {@code AlienShooter} is created.
     *  If {@code b < ticket <= 1.0f} then an instance of {@code AlienCloner} is created.
     *  @param  score   current player's score; determines how tough the newly created alien shall be
     */
	public void spawnAnAlien(float a, float b, int score) {
        float alienX;
        float alienY;
        // randomly choose position of the new alien behind the border of the game canvas
		switch (rand.nextInt(4)) {
			case 0: // upper border
                alienX = (float) rand.nextInt(Constants.GAME_CANVAS_WIDTH);
				alienY = - 2.0f * Alien.COLLISION_RADIUS;
				break;
			case 1: // right border
                alienX = (float) Constants.GAME_CANVAS_WIDTH + 2.0f * Alien.COLLISION_RADIUS;
				alienY = (float) rand.nextInt(Constants.GAME_CANVAS_HEIGHT);
				break;
			case 2: // bottom border
                alienX = (float) rand.nextInt(Constants.GAME_CANVAS_WIDTH);
				alienY = (float) Constants.GAME_CANVAS_HEIGHT + 2.0f * Alien.COLLISION_RADIUS;
				break;
			case 3: // left border
                alienX = - 2.0f * Alien.COLLISION_RADIUS;
				alienY = (float) rand.nextInt(Constants.GAME_CANVAS_HEIGHT);
				break;
			default:
                alienX = (float) Constants.GAME_CANVAS_WIDTH / 2.0f;
				alienY = (float) Constants.GAME_CANVAS_HEIGHT / 2.0f;
		}
        float ticket = rand.nextFloat();
        if (ticket <= a) {
            add(new Alien(alienX, alienY, listOfParticles, score, rand));
        } else if (ticket <= b) {
            add((Alien) new AlienShooter(alienX, alienY, listOfParticles, listOfAlienBullets, score, rand));
        } else {
            add(new AlienCloner(alienX, alienY, listOfParticles, this, score, rand));
        }
	}

    /** Creates new aliens according to the current value of the player's score.
     *  The idea is that the difficulty of the game increases with the
     *      increasing score of the player.
     *  Hence, with the increasing score the time between the spawns of the
     *      aliens decreases.
     *  When the score exceeds {@code MILESTONE_SHOOTER} then the aliens of the
     *      type {@code AlienShooter} start to spawn with an increasing
     *      probability.
     *  When the score exceeds {@code MILESTONE_CLONER} then the aliens of the
     *      type {@code AlienCloner} start to spawn with an increasing
     *      probability.
     */
	public void spawn(float deltaTime, int score) {
        timeToSpawnNextAlien -= deltaTime;
        if (timeToSpawnNextAlien <= 0.0f) {
            if (getNumber() < maxNumberOfAliens) {
                if (score < MILESTONE_SHOOTER) {
                    spawnAnAlien(1.0f, 1.0f, score);
                } else if (score < MILESTONE_CLONER) {
                    float a = Utils.getInterpolation(score, MILESTONE_SHOOTER, MILESTONE_CLONER, 1.0f, 0.5f);
                    // a is the probability that the basic alien (green one) will spawn
                    // 1-a is the probability that the alien shooter (red one) will spawn
                    spawnAnAlien(a, 1.0f, score);
                } else {
                    float a = Utils.getAsymptoticInterpolation(score, MILESTONE_CLONER, 2.0f * MILESTONE_CLONER, 0.5f, 0.25f);
                    float b = Utils.getAsymptoticInterpolation(score, MILESTONE_CLONER, 2.0f * MILESTONE_CLONER, 1.0f, 0.75f);
                    // a is the probability that the basic alien (green one) will spawn
                    // b-a is the probability that the alien shooter (red one) will spawn
                    // 1-b is the probability that the alien cloner (blue one) will spawn
                    spawnAnAlien(a, b, score);
                }
            }
            //  Time to spawn the next alien is chosen randomly, however, the
            //      borders of the interval are given by the current player's
            //      score.
            float minTime = Utils.getAsymptoticInterpolation(score, 0.0f, 2.0f * MILESTONE_CLONER, 1.0f, 0.5f);
            float maxTime = Utils.getAsymptoticInterpolation(score, 0.0f, 2.0f * MILESTONE_CLONER, 1.5f, 1.0f);
            timeToSpawnNextAlien = minTime + (maxTime - minTime) * rand.nextFloat();
        }
	}

    /** Causes all the aliens to stop being attracted by the shuttle's position.
     *  Used when shuttle dies.
     *  If this method is not called after the shuttle dies then the alien
     *      still will be attracted by the last position of the shuttle. */ 
	public void stopFollowingTheShuttle() {
        for (Alien iter = (Alien) getFirst(); iter != null; iter = (Alien) iter.getNext()) {
			iter.setAttracted(false);
        }
	}

    /** Manages collisions between the aliens and the bullets shot by the shuttle.
     *  If there is such a collision, both the alien and the bullet are set to
     *      the "dying" state. 
     *  @return how many aliens have been killed */
    public int handleCollisions(ListOfMovingObjects listOfShuttleBullets) {
        int howManyKilled = 0;
        for (Alien alien = (Alien) getFirst(); alien != null; alien = (Alien) alien.getNext()) {
            if (! alien.isDying()) {
                for (Bullet bullet = (Bullet) listOfShuttleBullets.getFirst(); bullet != null; bullet = (Bullet) bullet.getNext()) {
                    if (! bullet.isDying() && ! alien.isDying() && bullet.isCollision(alien)) {
                        alien.die();
                        bullet.die();
                        ++ howManyKilled;
                    }
                }
            }
        }
        return howManyKilled;
    }

    /** Moves all the moving objects in the list. */
    public void move(float deltaTime) {
        Alien iter = (Alien) first;
        Alien prev = null;

        while(iter != null) {
            if(iter.isDying()) {
                // if the alien is dying then remove the alien from the list
                // (which will cause the destruction of the instance) and
                // initiate an explosion
                iter.explode();
                if(prev == null) {
                    first = (Alien) first.getNext();
                    iter = (Alien) first;
                } else {
                    prev.setNext(iter.getNext());
                    iter = (Alien) iter.getNext();
                }
                -- numberOfAtoms;
            } else {
                //if (listOfShuttleBullets.isCollision(iter)) {
                //    iter.die();
                //} else {
                    for (Alien iter2 = (Alien) iter.getNext();
                            iter2 != null; 
                            iter2 = (Alien) iter2.getNext()) {
                        iter.repulseOtherAlien(iter2, deltaTime);
                    }
                    iter.move(deltaTime);
                //}

                prev = iter;
                iter = (Alien) iter.getNext();
            }
        }
    }

    /** Draws all the aliens in the list. */
    public void draw(Graphics g) {
        super.draw(g);
    }

}
